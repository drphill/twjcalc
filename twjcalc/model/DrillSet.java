package twjcalc.model;

import java.util.ArrayList;
import java.util.StringTokenizer;

import twjcalc.gui.ChangeListener;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * DrillSet.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Class defines a set of drill sizes.
 */
public final class DrillSet {

	private static final String SEPARATOR = ", ";

	public static void main(final String[] args) {
		final DrillSet drillSet = new DrillSet();
		final String str1 = drillSet.unparse();
		drillSet.parse(str1);
		final String str2 = drillSet.unparse();

		if (str1.equals(str2)) {
			System.out.println("correct");
			System.out.println(str1);
		} else {
			System.out.println(str1);
			System.out.println(str2);
		}
	}
	private final ChangeListener listener;
	private String name = "drills";
	private String filename = null;

	private final ArrayList sizes = new ArrayList();

	public DrillSet() {
		listener = null;
	}

	/**
	 * @param listener
	 */
	public DrillSet(final ChangeListener listener) {
		super();
		this.listener = listener;
		for (float f = 3; f < 13.4; f += 0.5) {
			sizes.add(Float.toString(f));
		}
		for (float f = 14; f < 21.4; f += 1.0) {
			sizes.add(Float.toString(f));
		}
		for (float f = 22; f < 30.4; f += 2.0) {
			sizes.add(Float.toString(f));
		}
	}

	public DrillSet(final DrillSet drillSet) {
		this();
		sizes.addAll(drillSet.sizes);
		name = drillSet.name;
	}

	public final String getFilename() {
		return filename;
	}

	public final String getName() {
		return name;
	}

	public final ArrayList getSizes() {
		return sizes;
	}

	public final void parse(final String parseString) {
		sizes.clear();
		final StringTokenizer tokeniser = new StringTokenizer(parseString, SEPARATOR);
		while (tokeniser.hasMoreTokens()) {
			final String token = tokeniser.nextToken();
			try {
				Double.parseDouble(token);
				// only add valid strings
				sizes.add(token);
			} catch (final NumberFormatException e) {
				e.printStackTrace();
			}
		}
		if (null != listener) {
			listener.changed(this);
		}
	}

	public final void setFilename(final String filename) {
		this.filename = filename;
	}

	public final void setName(final String name) {
		this.name = name;
	}

	public final String unparse() {
		return unparse(SEPARATOR);
	}

	public String unparse(final String separator) {
		final StringBuffer buf = new StringBuffer();
		if (sizes.size() > 0) {
			buf.append(sizes.get(0).toString());
		}
		for (int i = 1; i < sizes.size(); i++) {
			buf.append(separator);
			buf.append(sizes.get(i).toString());
		}
		return buf.toString();
	}

}
