package twjcalc.model.whistle;

import twjcalc.model.calculate.WhistleCalculator;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * Whistle.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 *
 */
public class Whistle {
	public final Embouchure embouchure;
	public Hole[] hole = new Hole[0];
	public double bore, wallThickness;
	public double speedOfSound = WhistleCalculator.getSpeedOfSound();

	private Whistle(final Whistle whistle) {
		bore = whistle.bore;
		wallThickness = whistle.wallThickness;
		hole = new Hole[whistle.hole.length];
		for (int i = 0; i < hole.length; i++) {
			hole[i] = whistle.hole[i].duplicate(this);
		}
		embouchure = whistle.embouchure.duplicate(this);
	}

	public Whistle() {
		super();
		embouchure = new Embouchure(this);
		bore = 12.7;
		wallThickness = 0.5;
		setHoleCount(7);
	}

	public final void copyCalculatedValuesFrom(final Whistle whistle) {
		for (int i = 0; i < hole.length; i++) {
			hole[i].copyCalculatedResultsFrom(whistle.hole[i]);
		}
		embouchure.copyCalculatedResultsFrom(whistle.embouchure);
	}

	public final Whistle duplicate() {
		return new Whistle(this);
	}

	public final void setHoleCount(final int holeCount) {
		final Hole[] newHoles = new Hole[holeCount];
		int copy = holeCount;
		if (hole.length < holeCount) {
			copy = hole.length;
		}
		for (int i = 0; i < copy; i++) {
			newHoles[i] = hole[i];
		}
		for (int i = copy; i < newHoles.length; i++) {
			newHoles[i] = new Hole(this, i);
		}
		hole = newHoles;
	}

}
