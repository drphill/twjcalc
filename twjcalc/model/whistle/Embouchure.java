package twjcalc.model.whistle;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * Embouchure.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 *
 */
public class Embouchure {
	public double correction;
	public final Whistle whistle;
	public double width = 6.35, length = 6.35;
	public double height = 0.5;

	private Embouchure(final Whistle whistle, final Embouchure embouchure) {
		this.whistle = whistle;
		width = embouchure.width;
		length = embouchure.length;
		correction = embouchure.correction;
	}

	public Embouchure(final Whistle whistle) {
		super();
		this.whistle = whistle;
	}

	public void copyCalculatedResultsFrom(final Embouchure embouchure) {
		correction = embouchure.correction;
	}

	public Embouchure duplicate(final Whistle owningWhistle) {
		return new Embouchure(owningWhistle, this);
	}

}
