package twjcalc.model.whistle;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * Hole.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 *
 */
public class Hole {
	public double size, frequency, position, relativeCutoff;
	public final int holeNumber;
	public boolean isThumbHole = false;
	public int offset;
	public final Whistle whistle;

	private Hole(final Whistle whistle, final Hole hole) {
		this.whistle = whistle;
		size = hole.size;
		frequency = hole.frequency;
		position = hole.position;
		relativeCutoff = hole.relativeCutoff;
		holeNumber = hole.holeNumber;
		isThumbHole = hole.isThumbHole;
		offset = hole.offset;
	}

	public Hole(final Whistle whistle, final int index) {
		super();
		this.whistle = whistle;
		holeNumber = index;
		size = 5;
	}

	public void copyCalculatedResultsFrom(final Hole hole) {
		position = hole.position;
		relativeCutoff = hole.relativeCutoff;
	}

	public final Hole duplicate(final Whistle owningWhistle) {
		return new Hole(owningWhistle, this);
	}
}
