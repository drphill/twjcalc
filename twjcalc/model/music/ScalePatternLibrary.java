package twjcalc.model.music;

import java.util.ArrayList;

import twjcalc.gui.ChangeListener;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * ScalePatternLibrary.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * A scale pattern is a set of intervals between notes of a scale.
 * <p>
 * A scale pattern has a name, a
 * </p>
 * <p>
 * Major harmonic scale in ET and JI
 * <ul>
 * <li>0S,2S,4S,5S,7S,9S,11S</li>
 * <li>1/1B, 9/8B, 5/4B, 4/3B, 3/2B, 5/3B, 15/8B</li>
 * <li>1.0B, 1.125B, 1.2B, 1.34B, 1.5B, 1.667B, 1.875B</li>
 * </ul>
 * </p>
 * <p>
 * </p>
 */
public class ScalePatternLibrary {

	// @formatter:off
	private static final String[][] _scales = new String[][] {
		{"Major ET", "0S,2S,4S,5S,7S,9S,11S", "The standard Major ET scale used by most whistles."}, 
		{"Major +b7 ET", "0S,2S,4S,5S,7S,9S,10S*,11S", "The standard Major ET scale used by most whistles, with a thumbhole for a strong flattened seventh."}, 
		{"Major BLF ET", "-2S,0S,2S,4S,5S,7S,9S,11S", "The standard Major ET scale used by most whistles, with a 'bottom little finger hole' for a low flattened seventh."}, 
		{"Major BLF +b7 ET", "-2S,0S,2S,4S,5S,7S,9S,10S*,11S", "The standard Major ET scale used by most whistles, with a 'bottom little finger hole' for a low flattened seventh, and a thumbhole for a strong flattened seventh."}, 
		{"Harmonic Minor ET", "0S,2S,3S,5S,7S,8S,11S", ""},
		{"Natural Minor ET", "0S,2S,3S,5S,7S,8S,10S", ""}, 
		{"Melodic Minor ET", "0S,2S,3S,5S,7S,9S,11S", ""},
		{"Major JI", "1/1B, 9/8B, 5/4B, 4/3B, 3/2B, 5/3B, 15/8B", "A Major JI scale."},
		{"Major +b7 JI", "1/1B, 9/8B, 5/4B, 4/3B, 3/2B, 5/3B, 16/9B*, 15/8B", "A Major JI scale, with a thumbhole for a strong flattened seventh."},
		{"Major BLF JI", "8/9B, 1/1B, 9/8B, 5/4B, 4/3B, 3/2B, 5/3B, 15/8B", "A Major JI scale, with a 'bottom little finger hole' for a low flattened seventh."},
		{"Major BLF + b7 JI", "8/9B, 1/1B, 9/8B, 5/4B, 4/3B, 3/2B, 5/3B, 16/9B*, 15/8B", "A Major JI scale, with a 'bottom little finger hole' for a low flattened seventh, and a thumbhole for a strong flattened seventh."}
		};
	// @formatter:on

	private static final ArrayList AllScalePatterns;
	private static final ArrayList listeners;

	static {
		AllScalePatterns = new ArrayList();
		listeners = new ArrayList();

		for (int i = 0; i < _scales.length; i++) {
			final ScalePattern pattern = new ScalePattern(_scales[i][0], _scales[i][1], true);
			pattern.setDescription(_scales[i][2]);
			AllScalePatterns.add(pattern);
		}
	}

	public static final void add(final ScalePattern scalePattern) {
		final ScalePattern found = get(scalePattern.getName());
		if (null != found) {
			AllScalePatterns.remove(found);
		}
		AllScalePatterns.add(scalePattern);
		for (int i = 0; i < listeners.size(); i++) {
			final ChangeListener listener = (ChangeListener) listeners.get(i);
			listener.changed(ScalePatternLibrary.class);
		}
	}

	public static final void addListener(final ChangeListener listener) {
		listeners.add(listener);
	}

	public final static ScalePattern get(final int i) {
		final ScalePattern found = (ScalePattern) AllScalePatterns.get(i);
		return found;
	}

	public final static ScalePattern get(String scalePatternName) {
		ScalePattern found = null;
		if (scalePatternName.startsWith("MajorScale")) {
			scalePatternName = "Major ET";
		}
		for (int i = 0; (null == found) && (i < AllScalePatterns.size()); i++) {
			final ScalePattern pattern = (ScalePattern) AllScalePatterns.get(i);
			if (scalePatternName.equals(pattern.getName())) {
				found = pattern;
			}
		}
		return found;
	}

	public static final ArrayList getAllScalePatterns() {
		return AllScalePatterns;
	}

}
