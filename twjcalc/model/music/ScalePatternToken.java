package twjcalc.model.music;

public class ScalePatternToken {

	public static final boolean isValid(final String pattern) {
		boolean valid = true;
		try {
			valid = new ScalePatternToken(pattern).isValid();
		} catch (final NumberFormatException e) {
			valid = false;
		}
		return valid;
	}

	public static void main(final String[] args) {
		String[] test = new String[] {
			"0S", "1S", "2S", "1.0B", "1.125B", "1.2B", "1/1B", "9/8B", "5/4B"
		};
		for (int i = 0; i < test.length; i++) {
			final String str = test[i];
			final ScalePatternToken tok = new ScalePatternToken(str);

			System.out.println(str + " ---> " + tok.getFrequency(1));
		}

		test = new String[] {
			"S", "B", "2", "1.125", "1/1", "9/8"
		};
		for (int i = 0; i < test.length; i++) {
			final String str = test[i];
			System.out.println(str + " ---> " + ScalePatternToken.isValid(str));
		}

	}

	private double factor = Double.NaN;
	private final boolean isThumbhole;
	private final String pattern;

	/**
	 * @param pattern
	 */
	ScalePatternToken(String pattern) {
		super();
		pattern = pattern.trim();
		this.pattern = pattern;

		if (pattern.length() < 1) {
			isThumbhole = false;
			factor = Double.NaN;
		} else {
			int index = pattern.indexOf('*');
			isThumbhole = (-1 != index);

			index = pattern.indexOf('S');
			if (-1 != index) {

				// we have a semi-tone multiplier. Everything before this should
				// be
				// a
				// double
				final String str = pattern.substring(0, index);
				try {
					final double multiplier = Double.parseDouble(str);
					factor = Math.pow(ET12.getSemitoneRatio(), multiplier);
				} catch (final NumberFormatException e) {
					// e.printStackTrace();
				}
			} else {
				index = pattern.indexOf('B');
				if (-1 != index) {
					// we have a base note multiplier, everything before the 'B'
					// is
					// a
					// number or a fraction
					final String str = pattern.substring(0, index);
					index = pattern.indexOf('/');
					if (-1 == index) {
						// this is a number
						factor = Double.parseDouble(str);
					} else {
						// this is a fraction
						final String quotientStr = str.substring(0, index);
						final String divisorStr = str.substring(index + 1);
						try {
							final double quotient = Double.parseDouble(quotientStr);
							final double divisor = Double.parseDouble(divisorStr);
							factor = quotient / divisor;
						} catch (final NumberFormatException e) {
							// e.printStackTrace();
						}
					}
				}
			}
		}

	}

	public final double getFrequency(final double baseFrequency) {
		return baseFrequency * factor;
	}

	public final String getPattern() {
		return pattern;
	}

	public final boolean isThumbhole() {
		return isThumbhole;
	}

	public final boolean isValid() {
		return !Double.isNaN(factor);
	}

}
