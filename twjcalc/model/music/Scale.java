package twjcalc.model.music;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * Scale.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * A scale is a collection of Notes.
 * <p>
 * A Scale is defined by a base note and a ScalePattern.
 * </p>
 * <p>
 * The scale notes get their frequency from the scale pattern.
 * </p>
 */
public class Scale {

	private final BaseNote baseNote;
	private final ScaleNote[] note;
	private final ScalePattern scalePattern;

	public Scale(final BaseNote baseNote, final ScalePattern scalePattern) {
		this.scalePattern = scalePattern;
		note = new ScaleNote[scalePattern.getNoteCount()];
		this.baseNote = baseNote;
		for (int i = 0; i < note.length; i++) {
			note[i] = new ScaleNote(i, this);
		}
	}

	public double getFrequency(final int index) {
		return scalePattern.getFrequency(index, baseNote.getFrequency());
	}

	public final ScaleNote getNote(final int i) {
		return note[i];
	}

	public final int getNoteCount() {
		return note.length;
	}

	public boolean isThumbed(final int index) {
		return scalePattern.isThumbhole(index);
	}

	@Override
	public String toString() {
		final StringBuffer buf = new StringBuffer();
		if (null != note) {
			for (int i = 0; i < note.length; i++) {
				buf.append(note[i].getName());
				buf.append(' ');
			}
		}
		return buf.toString();
	}
}
