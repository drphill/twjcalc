package twjcalc.model.music;

import java.util.StringTokenizer;

import twjcalc.application.xml.XmlConstants;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * ScalePattern.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * A scale pattern is a set of intervals between notes of a scale.
 * <p>
 * A scale pattern has a name, a pattern a commnet.
 * </p>
 * <p>
 * Major harmonic scale in ET and JI
 * <ul>
 * <li>0S,2S,4S,5S,7S,9S,11S</li>
 * <li>1/1B, 9/8B, 5/4B, 4/3B, 3/2B, 5/3B, 15/8B</li>
 * <li>1.0B, 1.125B, 1.2B, 1.34B, 1.5B, 1.667B, 1.875B</li>
 * </ul>
 * </p>
 * <p>
 * </p>
 */
public class ScalePattern {

	/**
	 * @param pattern
	 * @return null if the pattern is valid, or an error description if the pattern is invalid
	 */
	public static final String errorString(final String pattern) {
		String error = null;
		final StringTokenizer tokeniser = new StringTokenizer(pattern, " ,()");
		String tok = tokeniser.nextToken();
		if (null == tok) {
			error = "There are no valid tokens.";
		} else {
			ScalePatternToken token = new ScalePatternToken(tok);
			if (!token.isValid()) {
				error = "The first token(" + token.getPattern() + ") is invalid.";
			} else {
				double ratio = token.getFrequency(1);
				int index = 1;
				while ((null == error) && tokeniser.hasMoreElements()) {
					tok = tokeniser.nextToken();
					final ScalePatternToken newToken = new ScalePatternToken(tok);
					if (!token.isValid()) {
						error = "Token " + index + " (" + token.getPattern() + ") is invalid.";
					} else {
						final double newRatio = newToken.getFrequency(1);
						if (newRatio <= ratio) {
							error =
								"Token " + index + " (" + newToken.getPattern() + ") <= Token " + (index - 1) + " ("
									+ token.getPattern() + ").";
						}
						ratio = newRatio;
					}
					token = newToken;
					index++;
				}
			}
		}
		return error;
	}

	public static final boolean isValid(final String pattern) {
		boolean valid = true;
		final StringTokenizer tokeniser = new StringTokenizer(pattern, " ,()");
		while (valid && tokeniser.hasMoreElements()) {
			valid = new ScalePatternToken(tokeniser.nextToken()).isValid();
		}
		return valid;
	}

	private final boolean _builtIn;
	private String name = "";
	private String description = "";
	private final ScalePatternToken[] tokens;

	public ScalePattern(final String name, final String pattern, final boolean b) {
		this(name, pattern, b, XmlConstants.VERSION_CURRENT);
	}

	public ScalePattern(final String name, final String pattern, final boolean b, final String version) {
		this.name = name;
		_builtIn = b;
		if (XmlConstants.VERSION_2_00.equals(version)) {
			final StringTokenizer tokeniser = new StringTokenizer(pattern, " ,()");
			tokens = new ScalePatternToken[tokeniser.countTokens()];
			for (int i = 0; i < tokens.length; i++) {
				tokens[i] = new ScalePatternToken(tokeniser.nextToken());
			}
		} else if (null == version) {
			// old style intervals
			final StringTokenizer tokeniser = new StringTokenizer(pattern, " ,()");
			tokens = new ScalePatternToken[tokeniser.countTokens() + 1];
			tokens[0] = new ScalePatternToken("0S");
			int totalSemitones = 0;
			for (int i = 1; i < tokens.length; i++) {
				final String tok = tokeniser.nextToken();
				final int delta = Integer.parseInt(tok);
				totalSemitones += delta;
				final String str = totalSemitones + "S";
				tokens[i] = new ScalePatternToken(str);
			}
		} else {
			tokens = null;
		}
	}

	final double getFrequency(final int index, final double baseFrequency) {
		return tokens[index].getFrequency(baseFrequency);
	}

	public final String getDescription() {
		return description;
	}

	public final String getName() {
		return name;
	}

	public final int getNoteCount() {
		return tokens.length;
	}

	public final String getPattern() {
		return getPattern(",");
	}

	public final String getPattern(final String separator) {
		final StringBuffer buf = new StringBuffer();
		buf.append(tokens[0].getPattern());
		for (int i = 1; i < tokens.length; i++) {
			buf.append(separator);
			buf.append(tokens[i].getPattern());
		}
		return buf.toString();
	}

	public final boolean isBuiltIn() {
		return _builtIn;
	}

	public final boolean isThumbhole(final int i) {
		return tokens[i].isThumbhole();
	}

	public final void setDescription(final String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		final StringBuffer buf = new StringBuffer(name);
		buf.append(" [");
		buf.append(getPattern());
		buf.append(']');
		return buf.toString();
	}

}
