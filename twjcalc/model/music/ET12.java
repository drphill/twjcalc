package twjcalc.model.music;

import twjcalc.gui.ChangeListener;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * ET12.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 *
 */
public class ET12 {

	private static final ET12 et12;

	/**
	 * this array contains the highest frequency ratio that we will allocate to each note. so if a
	 * given note's frequency ratio is between n and n+1 the note is n.
	 */
	private static final double factorBoundary[];

	/**
	 * an array of the factors by which we multiply the reference pitch to get the pitch of a note.
	 */
	private static final double factors[];

	private static final int NOT_FOUND = -1;

	// @formatter:off
	/*The String names of the notes.  We use the 'flat' term for semitones as
	 * Java does not like a hash (#) in a constant name.
	 */
	public static final String Ab = "G#";
	public static final String A = "A";
	public static final String Bb = "Bb";
	public static final String B = "B";
	public static final String C = "C";
	public static final String Db = "C#";
	public static final String D = "D";
	public static final String Eb = "Eb";
	public static final String E = "E";
	public static final String F = "F";
	public static final String Gb = "F#";
	public static final String G = "G";
	// @formatter:on

	//@formatter:off
	/**
	 * An array of the permissible note names. 
	 * Each element in the outer array is an array of synonyms. 
	 */
	private static final String[][] noteNames = new String[][] {
		{C}, {Db, "Db"}, {D}, {Eb, "D#"}, 
		{E}, {F}, {Gb, "Gb"}, {G}, {Ab,  "Ab"},
		{A}, {Bb, "A#"}, {B}
	};
	//@formatter:on

	private static final double semitoneRatio = Math.pow(2, 1.0 / 12.0);

	static {
		factors = new double[noteNames.length];
		factorBoundary = new double[noteNames.length];
		factorBoundary[0] = Math.pow(2, 1.0 / 24.0);
		;
		factors[0] = 1;
		for (int i = 1; i < factors.length; i++) {
			factors[i] = semitoneRatio * factors[i - 1];
			factorBoundary[i] = semitoneRatio * factorBoundary[i - 1];
		}
		et12 = new ET12("A4", 440);
	}

	public static String baseName(final String noteName) {
		return et12._baseName(noteName);
	}

	/**
	 * @param noteName
	 *            : name of the note. If no octave is specified then DEFAULT_OCTAVE is assumed.
	 * @return the frequency of the named note. if there is an error then -1 is returned.
	 */
	public static final double getFrequency(final String noteName) {
		return et12._getFrequency(noteName);
	}

	public static final String getLastBaseNoteName() {
		return et12._getLastBaseNoteName();
	}

	public static final String[][] getNotenames() {
		return noteNames;
	}

	public static final double getSemitoneRatio() {
		return semitoneRatio;
	}

	public static void main(final String[] args) {
		final String[] test = new String[] {
			"A4", "A3", "A5", "A3", "A#3", "Bb3", "B3", "C4", "C#4", "Db", "D", "D4", "D9"
		};
		for (int i = 0; i < test.length; i++) {
			System.out.println(test[i] + " = " + ET12.getFrequency(test[i]));
		}

		final double[] test2 = new double[] {
			246, 250, 255, 260, 265, 270, 275, 280, 285, 290, 2900
		};

		for (int i = 0; i < test2.length; i++) {
			System.out.println("" + test2[i] + " = " + ET12.noteName(test2[i]));
		}

	}

	/**
	 * @param frequency
	 * @return the name of the note closest to the given frequency
	 */
	public static final String noteName(final double frequency) {
		return et12._noteName(frequency);
	}

	/**
	 * @param noteName
	 * @return the octave specified in the note name or DEFAULT_OCTAVE if none specified.
	 */
	public static final int octave(final String noteName) {
		return et12._octave(noteName);
	}

	/**
	 * @param noteName
	 *            name of note whose frequency is specified
	 * @param frequency
	 *            of note specified.
	 */
	public static final void setBaseFrequency(final String noteName, final double frequency) {
		et12._setBaseFrequency(noteName, frequency);
	}

	public static final void setChangeListener(final ChangeListener aListener) {
		et12._setChangeListener(aListener);
	}

	private double baseFrequency = 440;

	private final int DEFAULT_OCTAVE = 4;

	private String lastBaseNoteName;

	private ChangeListener listener = null;

	private final int REFERENCE_OCTAVE = 0;

	private ET12(final String noteName, final double frequency) {
		_setBaseFrequency(noteName, frequency);
	}

	private String _baseName(String noteName) {
		int firstDigitPosition = NOT_FOUND;
		final char[] charArray = noteName.toCharArray();
		for (int i = 0; (firstDigitPosition == NOT_FOUND) && (i < charArray.length); i++) {
			if (Character.isDigit(charArray[i])) {
				firstDigitPosition = i;
			}
		}
		if (NOT_FOUND != firstDigitPosition) {
			noteName = noteName.substring(0, firstDigitPosition);
		}
		return noteName;
	}

	private final double _getFrequency(final String noteName) {
		double frequency = NOT_FOUND;
		final int note = _noteIndex(noteName);
		if (NOT_FOUND != note) {
			int octave = octave(noteName);

			// System.out.println("note: " + note);
			// System.out.println("octave: " + octave);
			frequency = baseFrequency * factors[note];
			while (octave > REFERENCE_OCTAVE) {
				frequency = frequency * 2;
				octave--;
			}
		}
		return frequency;
	}

	private final String _getLastBaseNoteName() {
		return lastBaseNoteName;
	}

	/**
	 * return the number of semi tones above C that the named note is.
	 * 
	 * @param noteName
	 * @return index of note name (place in octave)
	 */
	private final int _noteIndex(String noteName) {
		int index = NOT_FOUND;

		noteName = _baseName(noteName);

		// search the list of note names (and all synonyms) to fina a match
		for (int note = 0; (index == NOT_FOUND) && (note < noteNames.length); note++) {
			final String[] synonyms = noteNames[note];
			for (int i = 0; i < synonyms.length; i++) {
				final String synonym = synonyms[i];
				if (noteName.equals(synonym)) {
					index = note;
				}
			}
		}
		return index;
	}

	private final String _noteName(double frequency) {
		int octave = 0;
		// calculate highest frequency deemed to be in first octave
		final int lastNote = factorBoundary.length - 1;
		final double maximumOctaveFrequency = baseFrequency * factorBoundary[lastNote];
		while (frequency > maximumOctaveFrequency) {
			frequency /= 2;
			octave++;
		}

		final double ratio = frequency / baseFrequency;

		int note = 0;
		while ((note < factorBoundary.length) && (factorBoundary[note] < ratio)) {
			note++;
		}
		if (note >= noteNames.length) {
			note -= noteNames.length;
		}
		String noteName = noteNames[note][0];

		noteName = noteName + octave;
		return noteName;
	}

	private final int _octave(final String noteName) {
		int octave = DEFAULT_OCTAVE;
		final char[] charArray = noteName.toCharArray();
		for (int i = 0; (octave == DEFAULT_OCTAVE) && (i < charArray.length); i++) {
			if (Character.isDigit(charArray[i])) {
				octave = Character.digit(charArray[i], 10);
			}
		}
		return octave;
	}

	private final void _setBaseFrequency(final String noteName, double frequency) {
		lastBaseNoteName = noteName;
		final int note = _noteIndex(noteName);
		int octave = _octave(noteName);
		// we need to rebase to CZero for internal consistency
		frequency = frequency / factors[note];

		while (octave > REFERENCE_OCTAVE) {
			frequency = frequency / 2;
			octave--;
		}
		baseFrequency = frequency;
		if (null != listener) {
			listener.changed(getClass());
		}
	}

	private final void _setChangeListener(final ChangeListener aListener) {
		listener = aListener;
	}

}
