package twjcalc.model.music;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * BaseNote.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * A base note is defined by its name. Its frequency is always derived from the ET12
 * 
 */
public final class BaseNote {

	private final String name;

	/**
	 * @param name
	 */
	public BaseNote(final String name) {
		super();
		this.name = name;
	}

	public final double getFrequency() {
		return ET12.getFrequency(name);
	}

	public String getName() {
		return name;
	}

}
