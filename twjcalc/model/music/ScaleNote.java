package twjcalc.model.music;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * ScaleNote.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * A note in a scale is defined by its position in a scale.
 */
public final class ScaleNote {

	private final int index;
	private final Scale scale;

	/**
	 * @param index
	 * @param scale
	 */
	ScaleNote(final int index, final Scale scale) {
		super();
		this.index = index;
		this.scale = scale;
	}

	public final double getFrequency() {
		return scale.getFrequency(index);
	}

	public String getName() {
		return ET12.noteName(getFrequency());
	}

	public final boolean isThumbed() {
		return scale.isThumbed(index);
	}

}
