package twjcalc.model.calculate;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * CalculatorEntry.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 *
 */
public class CalculatorEntry {

	private WhistleCalculator calculator = null;
	private final String className;
	private final String name;

	/**
	 * @param name
	 * @param className
	 */
	CalculatorEntry(final String name, final String className) {
		super();
		this.name = name;
		this.className = className;
	}

	/**
	 * @param name
	 * @param className
	 */
	CalculatorEntry(final String name, final WhistleCalculator calculator) {
		super();
		this.name = name;
		className = calculator.getClass().getName();
		this.calculator = calculator;
	}

	public final WhistleCalculator getCalculator() {
		if (null == calculator) {
			try {
				calculator = (WhistleCalculator) getClass().getClassLoader().loadClass(className).newInstance();
			} catch (final InstantiationException e) {
				e.printStackTrace();
			} catch (final IllegalAccessException e) {
				e.printStackTrace();
			} catch (final ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return calculator;
	}

	public String getName() {
		return name;
	}

}
