package twjcalc.model.calculate;

import twjcalc.model.music.BaseNote;
import twjcalc.model.music.Scale;
import twjcalc.model.music.ScaleNote;
import twjcalc.model.music.ScalePattern;
import twjcalc.model.music.ScalePatternLibrary;
import twjcalc.model.whistle.Hole;
import twjcalc.model.whistle.Whistle;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * Specification.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * This class is a wrapper for a Whistle. It allows the GUI to interact with the whistle.
 */
public class Specification {

	private BaseNote _baseNote = null;
	private String _baseNoteName = "D5";
	private Scale _scale;
	private ScalePattern _scalePattern = null;
	private String _scalePatternName = "MajorScale";
	private final Whistle whistle;
	private String whistleComment = "Default whistle comment.";
	private String whistleName = "Unnamed Whistle";

	public Specification() {
		super();
		whistle = new Whistle();
	}

	public final void calculate(final WhistleCalculator calculator) {
		// check we have all data structures needed
		calculateScale();
		final Whistle duplicate = whistle.duplicate();
		calculator.calculate(duplicate);
		whistle.copyCalculatedValuesFrom(duplicate);
	}

	public final void calculateScale() {
		final ScalePattern scalePattern = getScalePattern();
		_scale = new Scale(getBaseNote(), scalePattern);
		whistle.setHoleCount(_scale.getNoteCount());
		for (int i = 0; i < _scale.getNoteCount(); i++) {
			final ScaleNote note = _scale.getNote(i);
			final Hole hole = whistle.hole[i];
			hole.isThumbHole = note.isThumbed();
			if (hole.isThumbHole) {
				hole.offset += 180;
				if (hole.offset > 360) {
					hole.offset -= 360;
				}
			}
			hole.frequency = note.getFrequency();
		}
	}

	public final BaseNote getBaseNote() {
		if (null == _baseNote) {
			_baseNote = new BaseNote(_baseNoteName);
		}
		return _baseNote;
	}

	public final String getBaseNoteName() {
		return _baseNoteName;
	}

	public final double getEmbouchureHeight() {
		return whistle.embouchure.height;
	}

	public final double getEmbouchureLength() {
		return whistle.embouchure.length;
	}

	public final double getEmbouchureWidth() {
		return whistle.embouchure.width;
	}

	public final double getPipeID() {
		return whistle.bore;
	}

	public final double getPipeWallThickness() {
		return whistle.wallThickness;
	}

	public final Scale getScale() {
		if (null == _scale) {
			if ((null != _baseNote) && (null != _scalePattern)) {
				_scale = new Scale(_baseNote, _scalePattern);
			} else {
				throw new Error();
			}
		}
		return _scale;
	}

	public final ScalePattern getScalePattern() {
		if (null == _scalePattern) {
			_scalePattern = ScalePatternLibrary.get(_scalePatternName);
		}
		return _scalePattern;
	}

	public final String getScalePatternName() {
		return _scalePatternName;
	}

	public final Whistle getWhistle() {
		return whistle;
	}

	public final String getWhistleComment() {
		return whistleComment;
	}

	public double getWhistleLength() {
		return whistle.hole[0].position;
	}

	public final String getWhistleName() {
		return whistleName;
	}

	public final void setBaseNote(final BaseNote baseNote) {
		final BaseNote oldValue = _baseNote;
		_baseNote = baseNote;
		_baseNoteName = baseNote.getName();

		if (_baseNote != oldValue) {
			// if the scale is changeing, recalculate
			calculateScale();
			// if this is not the first set value, print
			// System.out.println(this);
		}
	}

	public final void setEmbouchure(final double length, final double width, double height) {
		whistle.embouchure.length = length;
		whistle.embouchure.width = width;
		if (height == 0) {
			height = whistle.wallThickness;
		}
		whistle.embouchure.height = height;
	}

	public final void setEmbouchure(final String length, final String width, String height) {
		if ((null == height) || (height.length() < 1)) {
			height = "0";
		}
		setEmbouchure(Double.parseDouble(length), Double.parseDouble(width), Double.parseDouble(height));
	}

	public final void setHole(final int holeNum, final String sizeStr, final String offStr) {
		final Hole hole = whistle.hole[holeNum];
		hole.size = Double.parseDouble(sizeStr);
		hole.offset = Integer.parseInt(offStr);
	}

	public final void setHoleCount(final int holeCount) {
		whistle.setHoleCount(holeCount);
	}

	public final void setMusicNames(final String scalePatternName, final String baseNoteName) {
		_scalePatternName = scalePatternName;
		_baseNoteName = baseNoteName;
		getBaseNoteName();
		getScalePattern();
		calculateScale();
	}

	public final void setPipe(final double d, final double e) {
		whistle.bore = d;
		whistle.wallThickness = e;
	}

	public final void setPipe(final String pipeID, final String thickness) {
		setPipe(Double.parseDouble(pipeID), Double.parseDouble(thickness));
	}

	public final void setScalePattern(final ScalePattern scalePattern) {
		_scalePattern = scalePattern;
		_scalePatternName = _scalePattern.getName();
		calculateScale();
	}

	public final void setWhistleComment(final String whistleComment) {
		this.whistleComment = whistleComment;
	}

	public final void setWhistleName(final String whistleName) {
		this.whistleName = whistleName;
	}

	@Override
	public String toString() {
		final StringBuffer buf = new StringBuffer();
		if (null != _baseNote) {
			buf.append(_baseNote);
			buf.append(" [");
			buf.append(_baseNote.getFrequency());
			buf.append("]\n");
		}
		if (null != _scalePattern) {
			buf.append(_scalePattern.toString());
			buf.append("\n");
		}
		return buf.toString();
	}

}
