package twjcalc.model.calculate;

import java.util.ArrayList;

import twjcalc.gui.ChangeListener;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * CalculatorRegister.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 *
 */
public class CalculatorRegister {

	private static final CalculatorRegister register = new CalculatorRegister();

	public final static void addListener(final ChangeListener listener) {
		register._addListener(listener);
	}

	public static final WhistleCalculator getCalculator(final String name) {
		return register._getCalculator(name);
	}

	public static final WhistleCalculator getCurrentCalculator() {
		return register._getCurrentCalculator();
	}

	public static final ArrayList getNames() {
		return register._getNames();
	}

	/**
	 * @param name
	 * @param className
	 */
	public static final void register(final String name, final String className) {
		register._register(name, className);
	}

	public static final void register(final WhistleCalculator calculator) {
		register._register(calculator);
		register.notifyChanged();
	}

	public static final void setCurrentCalculatorName(final String currentCalculatorName) {
		register._setCurrentCalculatorName(currentCalculatorName);
	}
	private final ArrayList calculators = new ArrayList();

	private final ArrayList listeners = new ArrayList();

	private String currentCalculatorName = "Corrected Flutomat";

	public CalculatorRegister() {
		_register(new WhistleCalculator());
		// final UnifiedCalculator calc = new UnifiedCalculator();
		// calc.setIterative(true);
		// _register(calc);
		// _register(new Flutomat());
		// _register(new Flutomat3());
		// _register(new HBFlutomat());
		_register(new HBracker());
		// _register(new TWCalculator2());
		_register(new TWCalc());
	}

	private final void _addListener(final ChangeListener listener) {
		listeners.add(listener);
	}

	private final WhistleCalculator _getCalculator(final String name) {
		WhistleCalculator calculator = null;
		final CalculatorEntry found = find(name);
		if (null != found) {
			calculator = found.getCalculator();
		}
		return calculator;
	}

	private final ArrayList _getNames() {
		final ArrayList names = new ArrayList();
		for (int i = 0; i < calculators.size(); i++) {
			final CalculatorEntry entry = (CalculatorEntry) calculators.get(i);
			names.add(entry.getName());
		}
		return names;
	}

	private final void _register(final String name, final String className) {
		// make sure no duplicate keys
		remove(name);
		calculators.add(new CalculatorEntry(name, className));
	}

	private final void _register(final WhistleCalculator calculator) {
		final String name = calculator.getName();
		// make sure no duplicate keys
		remove(name);
		calculators.add(new CalculatorEntry(name, calculator));
	}

	private final CalculatorEntry find(final String name) {
		CalculatorEntry found = null;
		for (int i = 0; (null == found) && (i < calculators.size()); i++) {
			final CalculatorEntry entry = (CalculatorEntry) calculators.get(i);
			if (entry.getName().equals(name)) {
				found = entry;
			}
		}
		return found;
	}

	private void notifyChanged() {
		for (int i = 0; i < listeners.size(); i++) {
			final ChangeListener l = (ChangeListener) listeners.get(i);
			l.changed(getClass());
		}
	}

	private final void remove(final String name) {
		final CalculatorEntry found = find(name);
		if (null != found) {
			calculators.remove(found);
		}
	}

	public final WhistleCalculator _getCurrentCalculator() {
		return getCalculator(currentCalculatorName);
	}

	public final void _setCurrentCalculatorName(final String currentCalculatorName) {
		final CalculatorEntry found = find(currentCalculatorName);
		if (null != found) {
			this.currentCalculatorName = currentCalculatorName;
			notifyChanged();
		}
	}

}
