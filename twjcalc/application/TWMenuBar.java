package twjcalc.application;

import java.awt.CheckboxMenuItem;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * TWMenuBar.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
public class TWMenuBar extends MenuBar implements ActionListener, ItemListener {

	private static final String HELP_ABOUT = "About";
	private static final String HELP_HELP = "Help";
	private static final String FILE_LOAD_WHISTLE = "Load Whistle";
	private static final String FILE_LOAD_WHISTLE_REPLACE = "Load Whistle (replace current)";
	private static final String FILE_NEW_WHISTLE = "New Whistle";
	private static final String FILE_NEW_WHISTLE_REPLACE = "New Whistle (replace current)";
	private static final String FILE_SAVE_WHISTLE = "Save Whistle";
	private static final String FILE_UNLOAD_WHISTLE = "Unload Whistle";
	private static final String FILE_EXIT = "Exit";

	private static final String EDIT_WHISTLE_NAME = "Edit Whistle Name";
	private static final String EDIT_SPEED_OF_SOUND = "Edit Speed of Sound";
	private static final String EDIT_WHISTLE_COMMENT = "Edit Whistle Comment";
	private static final String EDIT_DRILL_SET = "Edit Drill Set";
	private static final String EDIT_SCALE_PATTERNS = "Edit Scale Patterns";
	private static final String EDIT_BASE_FREQUENCY = "Edit Base Frequency";
	private static final String EDIT_JAVASCRIPT = "Edit JavaScript";

	private static final String VIEW_GRAPH = "Graph";
	private static final String VIEW_CUTOFF_RATIO = "Cutoff Ratio";

	private static final String HELP_DUMP_LOG = "Dump Log";
	private static final String HElP_USER_MANUAL = "User Manual";

	private static final long serialVersionUID = 1L;
	private final TWFrame frame;
	private final Menu whistleMenu;
	private final CheckboxMenuItem showGraphMenuItem;
	private final CheckboxMenuItem showCutoffMenuItem;

	public TWMenuBar(final TWFrame frame) {
		super();
		this.frame = frame;
		MenuItem menuItem;
		Menu menu = new Menu("File");

		menuItem = new MenuItem(FILE_LOAD_WHISTLE);
		menuItem.addActionListener(this);
		menu.add(menuItem);
		menuItem = new MenuItem(FILE_LOAD_WHISTLE_REPLACE);
		menuItem.addActionListener(this);
		menu.add(menuItem);
		menuItem = new MenuItem(FILE_SAVE_WHISTLE);
		menuItem.addActionListener(this);
		menu.add(menuItem);
		menuItem = new MenuItem(FILE_NEW_WHISTLE);
		menuItem.addActionListener(this);
		menu.add(menuItem);
		menuItem = new MenuItem(FILE_NEW_WHISTLE_REPLACE);
		menuItem.addActionListener(this);
		menu.add(menuItem);
		menuItem = new MenuItem(FILE_UNLOAD_WHISTLE);
		menuItem.addActionListener(this);
		menu.add(menuItem);
		menuItem = new MenuItem(FILE_EXIT);
		menuItem.addActionListener(this);
		menu.add(menuItem);

		add(menu);

		menu = new Menu("Edit");
		menuItem = new MenuItem(EDIT_WHISTLE_NAME);
		menuItem.addActionListener(this);
		menu.add(menuItem);
		menuItem = new MenuItem(EDIT_WHISTLE_COMMENT);
		menuItem.addActionListener(this);
		menu.add(menuItem);
		menuItem = new MenuItem(EDIT_SCALE_PATTERNS);
		menuItem.addActionListener(this);
		menu.add(menuItem);
		menuItem = new MenuItem(EDIT_SPEED_OF_SOUND);
		menuItem.addActionListener(this);
		menu.add(menuItem);
		menuItem = new MenuItem(EDIT_DRILL_SET);
		menuItem.addActionListener(this);
		menu.add(menuItem);
		menuItem = new MenuItem(EDIT_BASE_FREQUENCY);
		menuItem.addActionListener(this);
		menu.add(menuItem);
		menuItem = new MenuItem(EDIT_JAVASCRIPT);
		menuItem.addActionListener(this);
		menu.add(menuItem);
		add(menu);

		whistleMenu = new Menu("Whistles");
		add(whistleMenu);

		menu = new Menu("View");
		showGraphMenuItem = new CheckboxMenuItem(VIEW_GRAPH, true);
		showGraphMenuItem.addItemListener(this);
		menu.add(showGraphMenuItem);
		showCutoffMenuItem = new CheckboxMenuItem(VIEW_CUTOFF_RATIO, true);
		showCutoffMenuItem.addItemListener(this);
		menu.add(showCutoffMenuItem);

		add(menu);

		menu = new Menu("Help");
		menuItem = new MenuItem(HELP_DUMP_LOG);
		menuItem.addActionListener(this);
		menu.add(menuItem);
		menuItem = new MenuItem(HELP_ABOUT);
		menuItem.addActionListener(this);
		menu.add(menuItem);
		menuItem = new MenuItem(HELP_HELP);
		menuItem.addActionListener(this);
		menu.add(menuItem);
		menuItem = new MenuItem(HElP_USER_MANUAL);
		menuItem.addActionListener(this);
		menu.add(menuItem);
		add(menu);
	}

	@Override
	public void actionPerformed(final ActionEvent e) {
		final String cmd = e.getActionCommand();
		if (cmd.equals(HELP_ABOUT)) {
			frame.about();
		} else if (cmd.equals(FILE_LOAD_WHISTLE)) {
			frame.loadWhistle();
		} else if (cmd.equals(FILE_LOAD_WHISTLE_REPLACE)) {
			frame.loadWhistleReplace();
		} else if (cmd.equals(FILE_NEW_WHISTLE)) {
			frame.newWhistle();
		} else if (cmd.equals(FILE_NEW_WHISTLE_REPLACE)) {
			frame.newWhistleReplace();
		} else if (cmd.equals(FILE_SAVE_WHISTLE)) {
			frame.saveWhistle();
		} else if (cmd.equals(FILE_UNLOAD_WHISTLE)) {
			frame.unloadWhistle();
		} else if (cmd.equals(FILE_EXIT)) {
			frame.exit();
		} else if (cmd.equals(EDIT_DRILL_SET)) {
			frame.editDrillSet();
		} else if (cmd.equals(EDIT_SCALE_PATTERNS)) {
			frame.editScalePattern();
		} else if (cmd.equals(EDIT_WHISTLE_COMMENT)) {
			frame.editWhistleComment();
		} else if (cmd.equals(EDIT_WHISTLE_NAME)) {
			frame.editWhistleName();
		} else if (cmd.equals(EDIT_BASE_FREQUENCY)) {
			frame.setBaseFrequency();
		} else if (cmd.equals(EDIT_JAVASCRIPT)) {
			frame.editJavaScript();
		} else if (cmd.equals(EDIT_SPEED_OF_SOUND)) {
			frame.editSpeedOfSound();
		} else if (cmd.equals(HELP_DUMP_LOG)) {
			frame.dumpLog();
		} else if (cmd.equals(HELP_HELP)) {
			frame.help();
		} else if (cmd.equals(HElP_USER_MANUAL)) {
			frame.userManual();
		}
	}

	public void add(final LoadedSpecification loadedSpecification) {
		whistleMenu.add(loadedSpecification);
	}

	public boolean isShowCutoff() {
		return showCutoffMenuItem.getState();
	}

	public boolean isShowGraph() {
		return showGraphMenuItem.getState();
	}

	@Override
	public void itemStateChanged(final ItemEvent e) {
		final Object src = e.getSource();
		if (src == showGraphMenuItem) {
			frame.showGraph(showGraphMenuItem.getState());
		} else if (src == showCutoffMenuItem) {
			frame.showCutoffRatio(showCutoffMenuItem.getState());
		}

	}

	public void remove(final LoadedSpecification loadedSpecification) {
		whistleMenu.remove(loadedSpecification);
	}

	public void setShowCutoff(final boolean state) {
		showCutoffMenuItem.setState(state);
	}

	public void setShowGraph(final boolean show) {
		showGraphMenuItem.setState(show);
	}

}
