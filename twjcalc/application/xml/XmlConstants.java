package twjcalc.application.xml;

public interface XmlConstants {

	public static final String XPOS = "YPos";
	public static final String YPOS = "YPos";
	public static final String HEIGHT = "Height";
	public static final String BOUNDS = "Bounds";
	public static final String SHOW_GRAPH = "ShowGraph";
	public static final String TWCALC_INI = "twcalc.ini";
	public static final String BASENOTENAME = "BaseNoteName";
	public static final String BORE = "Bore";
	public static final String DESCRIPTION = "Description";
	public static final String DRILL_SET = "DrillSet";
	public static final String FREQUENCY = "Frequency";
	public static final String HOLE = "Hole";
	public static final String HOLECOUNT = "HoleCount";
	public static final String HOLES = "Holes";
	public static final String IMPERIAL = "Imperial";
	public static final String LENGTH = "Length";
	public static final String METRIC = "Metric";
	public static final String NAME = "Name";
	public static final String NOTES = "Notes";
	public static final String OFFSET = "Offset";
	public static final String PATTERN = "Pattern";
	public static final String SCALE = "Scale";
	public static final String SCALEPATTERN = "ScalePattern";
	public static final String SCALEPATTERNNAME = "ScalePatternName";
	public static final String THICKNESS = "Thickness";
	public static final String TUBE = "Tube";
	public static final String TWJCALC = "TWJCalc";
	public static final String UNITS = "Units";
	public static final String VERSION = "Version";
	public static final String VERSION_2_00 = "2.00";
	public static final String VERSION_CURRENT = VERSION_2_00;
	public static final String WHISTLE_CALCULATOR = "WhistleCalculator";
	public static final String WHISTLE_COMMENT = "WhistleComment";
	public static final String WHISTLE_NAME = "WhistleName";
	public static final String WHISTLES = "Whistles";
	public static final String WHISTLEFILE = "WhistleFile";
	public static final String DRILLFILE = "DrillFile";
	public static final String TRANSLUCENT = "TranslucentWhistle";
	public static final String LINE_DRAW_WHISTLE = "LineDrawWhistle";

	public static final String SHOWOFFSETS = "ShowOffsets";
	public static final String SHOWCUTOFF = "ShowCutoff";
	public static final String CALCULATORNAME = "CalculatorName";

	public static final String WHISTLE_SPECIFICATION = "WhistleSpecification";
	public static final String WIDTH = "Width";
	public static final String WINDOW = "Window";
	public static final String FUNCTION_SET = "FunctionSet";

}