package twjcalc.application.xml;

import java.awt.Rectangle;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import twjcalc.application.LoadedSpecification;
import twjcalc.application.TWFrame;
import twjcalc.application.javascript.FunctionSet;
import twjcalc.gui.NumberRender;
import twjcalc.model.DrillSet;
import twjcalc.model.calculate.CalculatorRegister;
import twjcalc.model.calculate.Specification;
import twjcalc.model.music.ScalePattern;
import twjcalc.model.whistle.Hole;
import twjcalc.model.whistle.Whistle;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * Xml.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Temporary holder for the Xml parse/unparse routines
 */
public class XmlUnparse2 implements XmlConstants {
	/**
	 * @param parent
	 * @param name
	 * @return child element called 'name' appended to 'parent'
	 */
	private static final Element createChild(final Element parent, final String name) {
		final Element child = parent.getOwnerDocument().createElement(name);
		parent.appendChild(child);
		return child;
	}

	/**
	 * @param parent
	 * @param name
	 * @param txt
	 * @return child element called 'name' appended to 'parent' with text content as 'txt'
	 */
	private static final Element createChild(final Element parent, final String name, final String txt) {
		final Element child = createChild(parent, name);
		child.setTextContent(txt);
		return child;
	}

	private static final Element createChildWithCData(final Element parent, final String name, final String txt) {
		if (null != txt) {
			final Element child = createChild(parent, name);
			final CDATASection cdata = parent.getOwnerDocument().createCDATASection(txt);
			child.appendChild(cdata);
			return child;
		}
		return null;
	}

	private static final Document createNewDocument() {
		Document document = null;
		try {
			final DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			final DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
			document = docBuilder.newDocument();
			final Element documentElement = document.createElement(WHISTLE_CALCULATOR);
			document.appendChild(documentElement);
			documentElement.setAttribute(VERSION, VERSION_CURRENT);
		} catch (final ParserConfigurationException e) {
			e.printStackTrace();
		}
		return document;
	}

	private static final void unparse(final FunctionSet functionSet, final Element documentElement) {
		final Element patternElement = createChild(documentElement, XmlConstants.FUNCTION_SET);
		documentElement.appendChild(patternElement);
		final String[] fnames = functionSet.getFunctionNames();
		for (int i = 0; i < fnames.length; i++) {
			final String fname = fnames[i];
			final String func = functionSet.getFunction(fname);
			createChildWithCData(patternElement, fname, func);
		}
	}

	private static final void unparse(final ScalePattern scalePattern, final Element documentElement) {
		final Element patternElement = createChild(documentElement, SCALEPATTERN);
		documentElement.appendChild(patternElement);
		createChildWithCData(patternElement, NAME, scalePattern.getName());
		createChildWithCData(patternElement, DESCRIPTION, scalePattern.getDescription());
		createChild(patternElement, PATTERN, scalePattern.getPattern());
	}

	/**
	 * @param drillSet
	 * @param document
	 */
	private static final void unparse2(final DrillSet drillSet, final Element documentElement) {

		final Element drillElement = createChild(documentElement, DRILL_SET);
		createChildWithCData(drillElement, NAME, drillSet.getName());
		final String pattern = drillSet.unparse(",");
		createChildWithCData(drillElement, PATTERN, pattern);
	}

	private static final void unparse2(final TWFrame frame, final Element documentElement) {
		boolean show = frame.getShowCutoff();
		createChild(documentElement, SHOWCUTOFF, Boolean.toString(show));
		show = frame.getShowGraph();
		createChild(documentElement, SHOW_GRAPH, Boolean.toString(show));

		final Rectangle bounds = frame.getBounds();
		Element element = createChild(documentElement, BOUNDS);
		createChild(element, XPOS, Integer.toString(bounds.x));
		createChild(element, YPOS, Integer.toString(bounds.y));
		createChild(element, WIDTH, Integer.toString(bounds.width));
		createChild(element, HEIGHT, Integer.toString(bounds.height));

		element = createChild(documentElement, WHISTLES);

		final ArrayList loaded = frame.getLoadedSpecs();
		for (int i = 0; i < loaded.size(); i++) {
			final LoadedSpecification loadedSpec = (LoadedSpecification) loaded.get(i);
			final String name = loadedSpec.getFilename();
			createChildWithCData(element, WHISTLEFILE, name);
		}
		final String drillFile = frame.getDrillSetFilename();
		createChildWithCData(documentElement, DRILLFILE, drillFile);

		final String calculator = CalculatorRegister.getCurrentCalculator().getName();
		createChildWithCData(documentElement, CALCULATORNAME, calculator);
	}

	private static final void unparseToFile(final Document doc, final String filename) {
		try {
			// set up a transformer
			final TransformerFactory transfac = TransformerFactory.newInstance();
			final Transformer trans = transfac.newTransformer();
			// trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			trans.setOutputProperty(OutputKeys.METHOD, "xml");
			trans.setOutputProperty(OutputKeys.STANDALONE, "yes");
			trans.setOutputProperty(OutputKeys.INDENT, "yes");
			final FileWriter fw = new FileWriter(new File(filename));
			final StreamResult result = new StreamResult(fw);
			final DOMSource source = new DOMSource(doc);
			trans.transform(source, result);
		} catch (final IOException e) {
			e.printStackTrace();
		} catch (final TransformerException e) {
			e.printStackTrace();
		}

	}

	public static final void saveConfig(final TWFrame frame) {
		final Document document = createNewDocument();
		final Element documentElement = document.getDocumentElement();
		unparse2(frame, documentElement);
		unparseToFile(document, TWCALC_INI);
	}

	/**
	 * @param documentElement
	 * @param specification
	 */
	public static final void unparse(final Element documentElement, final Specification specification) {
		final Element specElement = createChild(documentElement, WHISTLE_SPECIFICATION);
		final ScalePattern scalePattern = specification.getScalePattern();
		// if the scale pattern is not a built in one save it as well
		if (!scalePattern.isBuiltIn()) {
			unparse(scalePattern, documentElement);
		}

		final Element scaleElement = createChild(specElement, SCALE);
		createChild(scaleElement, SCALEPATTERNNAME, scalePattern.getName());
		createChild(scaleElement, BASENOTENAME, specification.getBaseNoteName());
		// create CData elements for whistleName and whistleComment

		if (true) {
			createChildWithCData(specElement, WHISTLE_NAME, specification.getWhistleName());
			createChildWithCData(specElement, WHISTLE_COMMENT, specification.getWhistleComment());
		}

		final Element tubeElement = createChild(specElement, TUBE);
		double f = specification.getPipeID();
		String str = NumberRender.stringWithDecimalPlaces(f, 3);
		createChild(tubeElement, BORE, str);
		f = specification.getPipeWallThickness();
		str = NumberRender.stringWithDecimalPlaces(f, 3);
		createChild(tubeElement, THICKNESS, str);

		final Element windowElement = createChild(specElement, WINDOW);
		f = specification.getEmbouchureWidth();
		str = NumberRender.stringWithDecimalPlaces(f, 3);
		createChild(windowElement, WIDTH, str);
		f = specification.getEmbouchureLength();
		str = NumberRender.stringWithDecimalPlaces(f, 3);
		createChild(windowElement, LENGTH, str);
		f = specification.getEmbouchureHeight();
		str = NumberRender.stringWithDecimalPlaces(f, 3);
		createChild(windowElement, HEIGHT, str);

		final Element holesElement = createChild(specElement, HOLES);
		final Whistle whistle = specification.getWhistle();
		holesElement.setAttribute(HOLECOUNT, Integer.toString(whistle.hole.length));
		for (int i = 1; i < whistle.hole.length; i++) {
			final Element el = createChild(holesElement, HOLE + Integer.toString(i));
			final Hole hole = whistle.hole[i];
			final double d = hole.size;
			str = NumberRender.stringWithDecimalPlaces(d, 3);
			createChild(el, WIDTH, str);
			final int off = hole.offset;
			str = Integer.toString(off);
			createChild(el, OFFSET, str);
		}
	}

	public static final void unparseToFile(final DrillSet drillSet, final String filename) {
		final Document document = createNewDocument();
		final Element documentElement = document.getDocumentElement();
		unparse2(drillSet, documentElement);
		unparseToFile(document, filename);
	}

	public static final void unparseToFile(final FunctionSet functionSet, final String filename) {
		final Document document = createNewDocument();
		final Element documentElement = document.getDocumentElement();
		unparse(functionSet, documentElement);
		unparseToFile(document, filename);
	}

	public static final void unparseToFile(final ScalePattern scalePattern, final String filename) {
		final Document document = createNewDocument();
		final Element documentElement = document.getDocumentElement();
		unparse(scalePattern, documentElement);
		unparseToFile(document, filename);
	}

	public static final void unparseToFile(final Specification specification, final String filename) {
		final Document document = createNewDocument();
		final Element documentElement = document.getDocumentElement();
		unparse(documentElement, specification);
		unparseToFile(document, filename);
	}

}
