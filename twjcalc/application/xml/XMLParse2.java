package twjcalc.application.xml;

import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import twjcalc.application.TWFrame;
import twjcalc.application.javascript.FunctionSet;
import twjcalc.model.DrillSet;
import twjcalc.model.calculate.CalculatorRegister;
import twjcalc.model.calculate.Specification;
import twjcalc.model.music.ScalePattern;
import twjcalc.model.music.ScalePatternLibrary;
import twjcalc.model.whistle.Hole;
import twjcalc.model.whistle.Whistle;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * Xml.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 */
public class XMLParse2 implements XmlConstants {
	private static final String EMPTYSTRING = "";
	private static final Logger logger;
	static {
		logger = Logger.getLogger("twjcalc.application.xml");
	}

	private static final void drillSet(final DrillSet drillSet, final Element documentElement) {
		logger.entering("XMLParse2", "drillSet");
		String pattern = null;
		String name = null;
		final Element drillSetElement = findChild(documentElement, DRILL_SET);
		if (null == drillSetElement) {
			logger.warning("No drill set element");
		} else {
			Element element = findChild(drillSetElement, NAME);
			if (null == element) {
				// old format
				logger.info("no name element - old format drill set");
				name = drillSetElement.getAttribute(NAME);
				pattern = drillSetElement.getAttribute(PATTERN);
			} else {
				name = element.getTextContent();
				element = findChild(drillSetElement, PATTERN);
				if (null == element) {
					logger.warning("New format, but no drill set pattern");
				} else {
					pattern = element.getTextContent();
				}
			}
			if (null != pattern) {
				drillSet.parse(pattern);
			}
			if (null != name) {
				drillSet.setName(name);
			}
		}
		logger.exiting("XMLParse2", "drillSet", pattern);
	}

	private static final Element findChild(final Element element, final String name) {
		Element found = null;
		final NodeList nodes_i = element.getChildNodes();
		for (int i = 0; i < nodes_i.getLength(); i++) {
			final Node node_i = nodes_i.item(i);
			if (node_i.getNodeType() == Node.ELEMENT_NODE) {
				final Element child = (Element) node_i;
				if (child.getTagName().equals(name)) {
					found = child;
				}
			}
		}
		return found;
	}

	/**
	 * @param functionSet
	 * @param docElement
	 */
	private static void functionSet(final FunctionSet functionSet, final Element docElement) {
		logger.entering("XMLParse2", "functionSet");
		final ScalePattern scalePattern = null;
		final Element patternElement = findChild(docElement, FUNCTION_SET);
		if (null == patternElement) {
			logger.warning("No function set element");
		} else {
			final NodeList nodeListj = patternElement.getChildNodes();
			for (int j = 0; j < nodeListj.getLength(); j++) {
				final Node node_j = nodeListj.item(j);
				if (node_j.getNodeType() == Node.ELEMENT_NODE) {
					final Element element = (Element) node_j;
					final String fname = element.getTagName();
					final String func = element.getTextContent();
					functionSet.setFunction(fname, func);
				}
			}
		}
		logger.exiting("XMLParse2", "functionSet", scalePattern);
	}

	/**
	 * We are swapping from using attributes to children to store values. This utility method
	 * searches for a child with the given name. If there is a child, return its text. Otherwise
	 * return the attribute value
	 * 
	 * @param element
	 * @param childName
	 * @return
	 */
	private static final String getChildOrAttributeText(final Element element, final String childName) {
		String childText = getChildText(element, childName);
		if (null == childText) {
			childText = element.getAttribute(childName);
		}
		return childText;
	}

	/**
	 * @param element
	 * @param childName
	 * @return Text from named child of given element, or null if failure
	 */
	private static final String getChildText(final Element element, final String childName) {
		String childText = null;
		final Element child = findChild(element, childName);
		if (null != child) {
			childText = child.getTextContent();
		}
		return childText;
	}

	private static final Specification parseSpecification1(final Element docElement) {
		logger.entering("XMLParse2", "parseSpecification1");
		Specification spec = null;
		final ScalePattern scalePattern = scalePattern(docElement);
		if (null != scalePattern) {
			ScalePatternLibrary.add(scalePattern);
		}

		final Element specElement = findChild(docElement, WHISTLE_SPECIFICATION);
		if (null == specElement) {
			logger.warning("No specification element");
		} else {
			spec = new Specification();

			final String units = specElement.getAttribute(UNITS);
			final boolean isMetric = ((null == units) || units.equals(METRIC));

			Element element = findChild(specElement, SCALE);
			if (null == element) {
				logger.warning("No scale element");
			} else {
				final String scalePatternName = element.getAttribute(SCALEPATTERNNAME);
				final String baseNoteName = element.getAttribute(BASENOTENAME);
				spec.setMusicNames(scalePatternName, baseNoteName);
				logger.info("Scale: " + baseNoteName + " " + scalePatternName);
			}

			element = findChild(specElement, TUBE);
			if (null == element) {
				logger.warning("No tube element");
			} else {
				final String strThickness = element.getAttribute(THICKNESS);
				final String strBore = element.getAttribute(BORE);
				spec.setPipe(strBore, strThickness);
				logger.info("Tube: " + strBore + " " + strThickness);
			}

			element = findChild(specElement, WINDOW);
			if (null == element) {
				logger.warning("No window element");
			} else {
				final String strWidth = element.getAttribute(WIDTH);
				final String strLength = element.getAttribute(LENGTH);
				spec.setEmbouchure(strLength, strWidth, "");
				logger.info("Window: " + strWidth + " x " + strLength);
			}

			element = findChild(specElement, HOLES);
			if (null == element) {
				logger.warning("No holes element");
			} else {
				final Whistle whistle = spec.getWhistle();
				final int holeCount = Integer.parseInt(element.getAttribute(HOLECOUNT));
				logger.info("Hole count = " + holeCount);
				whistle.setHoleCount(holeCount);
				for (int i = 1; i < holeCount; i++) {
					String str = "h" + Integer.toString(i);
					final Element el = findChild(element, str);
					if (null == el) {
						logger.warning("No hole element: " + str);
					} else {
						final Hole hole = whistle.hole[i];
						str = el.getAttribute(WIDTH);
						double diameter = Double.parseDouble(str);
						if (!isMetric) {
							// conversion to metric
							diameter = (diameter * 32.0) / 25.4;
						}
						hole.size = diameter;

						final int off = Integer.parseInt(el.getAttribute(OFFSET));
						hole.offset = off;
					}
				}
			}
		}
		logger.exiting("XMLParse2", "parseSpecification1", spec);
		return spec;
	}

	private static final Specification parseSpecification2(final Element docElement) {
		logger.entering("XMLParse2", "parseSpecification2");
		Specification spec = null;
		final ScalePattern scalePattern = scalePattern(docElement);
		if (null != scalePattern) {
			ScalePatternLibrary.add(scalePattern);
		}

		final Element specElement = findChild(docElement, WHISTLE_SPECIFICATION);
		if (null == specElement) {
			System.out.println("No specification element");
		} else {
			spec = new Specification();

			final NodeList nodeList = specElement.getChildNodes();
			for (int i = 0; i < nodeList.getLength(); i++) {
				final Node node_i = nodeList.item(i);
				if (node_i.getNodeType() == Node.ELEMENT_NODE) {
					final Element element = (Element) node_i;
					if (element.getTagName().equals(SCALE)) {
						final String scalePatternName = getChildText(element, SCALEPATTERNNAME);
						final String baseNoteName = getChildText(element, BASENOTENAME);
						spec.setMusicNames(scalePatternName, baseNoteName);
					} else if (element.getTagName().equals(TUBE)) {
						final String strThickness = getChildText(element, THICKNESS);
						final String strBore = getChildText(element, BORE);
						spec.setPipe(strBore, strThickness);
					} else if (element.getTagName().equals(WHISTLE_COMMENT)) {
						final String comment = element.getTextContent();
						spec.setWhistleComment(comment);
					} else if (element.getTagName().equals(WHISTLE_NAME)) {
						final String name = element.getTextContent();
						spec.setWhistleName(name);
					} else if (element.getTagName().equals(WINDOW)) {
						final String strWidth = getChildText(element, WIDTH);
						final String strLength = getChildText(element, LENGTH);
						final String strHeight = getChildText(element, HEIGHT);
						spec.setEmbouchure(strLength, strWidth, strHeight);
					} else if (element.getTagName().equals(HOLES)) {
						final int holeCount = Integer.parseInt(element.getAttribute(HOLECOUNT));
						spec.setHoleCount(holeCount);
						for (int hh = 1; hh < holeCount; hh++) {
							final String str = HOLE + Integer.toString(hh);
							final Element el = findChild(element, str);
							final String diamStr = getChildText(el, WIDTH);
							final String offString = getChildText(el, OFFSET);
							spec.setHole(hh, diamStr, offString);
						}
					}
				}
			}
		}
		logger.exiting("XMLParse2", "parseSpecification2", spec);
		return spec;
	}

	private static final ScalePattern scalePattern(final Element documentElement) {
		logger.entering("XMLParse2", "scalePattern");
		ScalePattern scalePattern = null;
		final Element patternElement = findChild(documentElement, SCALEPATTERN);
		if (null == patternElement) {
			logger.warning("No pattern element");
		} else {
			final String name = getChildOrAttributeText(patternElement, NAME);
			final String pattern = getChildOrAttributeText(patternElement, PATTERN);
			scalePattern = new ScalePattern(name, pattern, false, null);
		}
		logger.exiting("XMLParse2", "scalePattern", scalePattern);
		return scalePattern;
	}

	private static final Element validDocumentElement(final String filename) {
		logger.entering("XMLParse2", "validDocumentElement", filename);
		Element docElement = null;
		Document document = null;
		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(false);
		try {
			DocumentBuilder builder;
			builder = factory.newDocumentBuilder();
			document = builder.parse(filename);
			docElement = document.getDocumentElement();
			if (docElement.getNodeType() == Node.ELEMENT_NODE) {
				final String tagName = docElement.getTagName();
				if (tagName.equals(TWJCALC)) {
					logger.info("Version 1 format");
				} else if (tagName.equals(WHISTLE_CALCULATOR)) {
					logger.info("Version 2 format");
				} else {
					logger.warning("Unnrecognised tag: " + tagName);
				}
			} else {
				logger.warning("No document element");
				docElement = null;
			}
		} catch (final Exception e) {
			logger.warning(e.toString());
		}
		logger.exiting("XMLParse2", "validDocumentElement", docElement);
		return docElement;
	}

	public static final void drillSetFromFile(final DrillSet drillSet, final String filename) {
		logger.entering("XMLParse2", "drillSetFromFile", filename);
		final Element docElement = validDocumentElement(filename);
		if (null != docElement) {
			drillSet(drillSet, docElement);
		}
		logger.exiting("XMLParse2", "drillSetFromFile");
	}

	public static final void functionSetFromFile(final FunctionSet functionSet, final String filename) {
		logger.entering("XMLParse2", "functionSetFromFile", filename);
		final ScalePattern scalePattern = null;
		final Element docElement = validDocumentElement(filename);
		if (null != docElement) {
			functionSet(functionSet, docElement);
		}
		logger.exiting("XMLParse2", "functionSetFromFile", scalePattern);
	}

	public static final Specification readConfig(final TWFrame frame) {
		logger.entering("XMLParse2", "readConfig");
		final Specification spec = null;
		final Element docElement = validDocumentElement(TWCALC_INI);
		if (null != docElement) {
			boolean show = true;
			// some values used to be stored as attributes - look for them as
			// attributes first

			show = false;
			String str = docElement.getAttribute(TRANSLUCENT);
			if (null != str) {
				show = Boolean.parseBoolean(str);
				logger.info("translucent: " + str);
				frame.translucentWhistle(show);
			}

			show = true;
			str = docElement.getAttribute(SHOW_GRAPH);
			if ((null != str) && !"".equals(str)) {
				show = Boolean.parseBoolean(str);
				logger.info("Show Graph: " + str);
				frame.showGraph(show);
			}

			show = true;
			str = docElement.getAttribute(SHOWCUTOFF);
			if ((null != str) && !"".equals(str)) {
				show = Boolean.parseBoolean(str);
				logger.info("Show Cutoff: " + str);
				frame.showCutoffRatio(show);
			}

			final NodeList nodeListj = docElement.getChildNodes();
			for (int j = 0; j < nodeListj.getLength(); j++) {
				final Node node_j = nodeListj.item(j);
				if (node_j.getNodeType() == Node.ELEMENT_NODE) {
					final Element element = (Element) node_j;
					final String tagName = element.getTagName();
					if (tagName.equals(BOUNDS)) {
						str = getChildText(element, XPOS);
						final int x = Integer.parseInt(str);
						str = getChildText(element, YPOS);
						final int y = Integer.parseInt(str);
						str = getChildText(element, WIDTH);
						final int width = Integer.parseInt(str);
						str = getChildText(element, HEIGHT);
						final int height = Integer.parseInt(str);
						frame.setBounds(x, y, width, height);
					} else if (tagName.equals(WHISTLES)) {
						logger.info("Whistles.....");
						final NodeList nodeListw = docElement.getElementsByTagName(WHISTLEFILE);
						for (int w = 0; w < nodeListw.getLength(); w++) {
							final Element elementw = (Element) nodeListw.item(w);
							final String filename = elementw.getTextContent();
							logger.info("Whistle " + w + ": " + filename);
							if (w == 0) {
								frame.loadReplace(filename);
							} else {
								frame.loadWhistle(filename);
							}
						}
					} else if (tagName.equals(DRILLFILE)) {
						final String filename = element.getTextContent();
						logger.info("Drill file: " + filename);
						;
						frame.loadDrillSet(filename);
					} else if (tagName.equals(CALCULATORNAME)) {
						final String calculatorName = element.getTextContent();
						logger.info("Calculator: " + calculatorName);
						CalculatorRegister.setCurrentCalculatorName(calculatorName);
					} else if (tagName.equals(TRANSLUCENT)) {
						str = element.getTextContent();
						show = Boolean.parseBoolean(str);
						logger.info("translucent: " + str);
						frame.translucentWhistle(show);
					} else if (tagName.equals(SHOW_GRAPH)) {
						str = element.getTextContent();
						show = Boolean.parseBoolean(str);
						logger.info("Show Graph: " + str);
						frame.showGraph(show);
					} else if (tagName.equals(SHOWCUTOFF)) {
						str = element.getTextContent();
						show = Boolean.parseBoolean(str);
						logger.info("Show Cutoff: " + str);
						frame.showCutoffRatio(show);
					} else if (tagName.equals(LINE_DRAW_WHISTLE)) {
						str = element.getTextContent();
						show = Boolean.parseBoolean(str);
						logger.info("Line Draw: " + str);
						frame.lineDrawWhistle(show);
					}
				}
			}
		}
		logger.exiting("XMLParse2", "readConfig");
		return spec;
	}

	public static final ScalePattern scalePatternFromFile(final String filename) {
		logger.entering("XMLParse2", "scalePatternFromFile", filename);
		ScalePattern scalePattern = null;
		final Element docElement = validDocumentElement(filename);
		if (null != docElement) {
			scalePattern = scalePattern(docElement);
		}
		logger.exiting("XMLParse2", "scalePatternFromFile", scalePattern);
		return scalePattern;
	}

	public static final Specification specificationFromFile(final String filename) {
		logger.entering("XMLParse2", "specificationFromFile", filename);
		Specification spec = null;
		final Element docElement = validDocumentElement(filename);
		if (null != docElement) {
			final String version = docElement.getAttribute(VERSION);
			if ((null == version) || EMPTYSTRING.equals(version)) {
				logger.info("Old version");
				spec = parseSpecification1(docElement);
			} else if (VERSION_2_00.equals(version)) {
				logger.info("New version: " + version);
				spec = parseSpecification2(docElement);
			}
		}
		logger.exiting("XMLParse2", "specificationFromFile", spec);
		return spec;
	}

}
