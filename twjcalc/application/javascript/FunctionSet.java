package twjcalc.application.javascript;

/*
 * Copyright 2011 Phill van Leersum
 * 
 * FunctionSet.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 *
 */
public class FunctionSet implements ScriptConstants {
	private final String[][] functions = FlutomatScripts.DefaultFunctions;
	private String combinedScript = null;

	/**
	 * 
	 */
	public FunctionSet() {
		super();
	}

	public final String combinedScript() {
		// combinedScript = null;
		if (null == combinedScript) {
			final StringBuffer buf = new StringBuffer();
			for (int i = 0; i < functions.length; i++) {
				buf.append(functions[i][1]);
				buf.append("\n\n");
			}
			combinedScript = buf.toString();
		}
		return combinedScript;
	}

	public final String getFunction(final String name) {
		String found = null;
		for (int i = 0; (null == found) && (i < functions.length); i++) {
			if (functions[i][0].equals(name)) {
				found = functions[i][1];
			}

		}
		return found;
	}

	public final String[] getFunctionNames() {
		return FunctionNames;
	}

	public final void setBracker() {
		setFunctions(BrackerScripts.DefaultFunctions);
	}

	public final void setFlutomat() {
		setFunctions(FlutomatScripts.DefaultFunctions);
	}

	public final boolean setFunction(final String name, final String code) {
		boolean found = false;
		for (int i = 0; !found && (i < functions.length); i++) {
			if (functions[i][0].equals(name)) {
				functions[i][1] = code;
				combinedScript = null;
				found = true;
			}

		}
		return found;
	}

	public final void setFunctions(final String[][] func) {
		for (int i = 0; i < func.length; i++) {
			setFunction(func[i][0], func[i][1]);
		}
	}

	public final void setTWCalc() {
		setFunctions(TWCalcScripts.DefaultFunctions);
	}

}
