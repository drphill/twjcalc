package twjcalc.application.javascript;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * FlutomatScripts.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Contains the default JavaScript for the functions in the Flutomat style calculations.
 */
public class FlutomatScripts implements ScriptConstants {

	//@formatter:off
	static final String DefaultClosedCorrectionStr =
		"// This the default (flutomat) function.\n" + 
		" function closedCorrection(hole) {\n"+
		"    ratio = hole.size / hole.whistle.bore;\n"+
		"    ratio = ratio * ratio;\n"+
		"    return 0.25 * hole.whistle.wallThickness * ratio;\n"+
		"}";
	
	static final String DefaultCufoffFrequencyStr =
		"// This the default (flutomat) function.\n" + 
		"function cutoffFrequency(hole) {\n"+
		"    whistle = hole.whistle;\n"+
		"    holeNum = hole.holeNumber;\n"+
		"    dist = whistle.hole[holeNum - 1].position - whistle.hole[holeNum].position;\n"+
		"    sqrtTerm = Math.sqrt(effectiveThickness(whistle.hole[holeNum]) * dist);\n"+
		"    ratio = whistle.speedOfSound / (2 * Math.PI);\n"+
		"    ratio = ratio * (whistle.hole[holeNum].size / whistle.bore);\n"+
		"    ratio = ratio / sqrtTerm;\n"+
		"    return ratio;\n"+
		"}\n\n"+
		"// We need this here for the momment.... FIX later\n"+
		"function effectiveThickness(hole) {\n"+
		"    return hole.whistle.wallThickness + (0.75 * hole.size);\n" + 
		"}";
	
	static final String DefaultEndCorrectionStr =
		"// This the default (flutomat) function.\n" + 
		"function endCorrection(whistle) {\n"+
		"    return 0.6133 * whistle.bore / 2;\n" + 
		"}";

	static final String DefaultEmbouchureCorrectionStr =
		"// This the default (flutomat) function.\n" + 
		"function embouchureCorrection(whistle) {\n"+
		"    Demb = (whistle.embouchure.width + whistle.embouchure.length) / 2;\n"+
		"    r = (whistle.bore / Demb) * (whistle.bore / Demb);\n"+
		"    r = r * 10.84 * whistle.wallThickness * Demb;\n"+
		"    r = r / (whistle.bore + (2 * whistle.wallThickness)); \n" + "    return r;\n" + "}";

	static final String DefaultEffectiveThicknessStr =
		"// This the default (flutomat) function.\n" + 
		"function effectiveThickness(hole) {\n"+
		"    return hole.whistle.wallThickness + (0.75 * hole.size);\n" + 
		"}";

	static final String DefaultFirstHoleDistanceStr =
		"// This the default (flutomat) function.\n" + 
		"function firstHoleDistance(hole) {\n"+
		"    first = effectiveThickness(hole);\n"+
		"    second = hole.size / hole.whistle.bore;\n"+
		"    second = second * second;\n"+
		"    third = first / (hole.whistle.hole[0].position - hole.position);\n"+
		"    return first / (second + third);\n"+
	    "}\n";

	static final String DefaultSubsequentHoleDistanceStr =
		"// This the default (flutomat) function.\n" + 
		"function subsequentHoleDistance(hole) {\n"+
		"    prevHole = hole.whistle.hole[hole.holeNumber - 1];\n"+
		"    // if previous hole is a thumbHole we need to go back one more to find\n"+
		"    // an open tone hole.\n"+
		"    if (prevHole.isThumbHole) {\n"+
		"    	prevHole = hole.whistle.hole[hole.holeNumber - 2];\n"+
		"    }\n"+
		"    delta = prevHole.position - hole.position;\n"+
		"    first = delta / 2;\n"+
		"    second = effectiveThickness(hole) / delta;\n"+
		"    second = second * (hole.whistle.bore / hole.size) * (hole.whistle.bore / hole.size);\n"+
		"    second = second * 4;\n"+
		"    second = second + 1;\n"+
		"    second = Math.sqrt(second) - 1;\n"+
		"    return first * second;\n"+
		"}\n";

	static final String[][] DefaultFunctions = new String[][] {
		{EndCorrection, DefaultEndCorrectionStr}, 
		{ClosedCorrection, DefaultClosedCorrectionStr}, 
		{CutoffFrequency, DefaultCufoffFrequencyStr}, 
		{EmbouchureCorrection, DefaultEmbouchureCorrectionStr}, 
		{EffectiveThickness, DefaultEffectiveThicknessStr},
		{FirstHoleDistance, DefaultFirstHoleDistanceStr, }, 
		{SubsequentHoleDistance, DefaultSubsequentHoleDistanceStr},
	};
	
	//@formatter:on

	public static final String getFunction(final String name) {
		String found = null;
		for (int i = 0; (null == found) && (i < DefaultFunctions.length); i++) {
			if (DefaultFunctions[i][0].equals(name)) {
				found = DefaultFunctions[i][1];
			}

		}
		return found;
	}

	public static final String[] getFunctionNames() {
		final String[] names = new String[DefaultFunctions.length];
		for (int i = 0; i < names.length; i++) {
			names[i] = DefaultFunctions[i][0];
		}
		return names;
	}

}
