package twjcalc.application.javascript;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * ScriptConstants.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Contains constants common to the files in the JavaScript Calculator cluster
 */
public interface ScriptConstants {
	public static final String EffectiveThickness = "effectiveThickness";
	public static final String EndCorrection = "endCorrection";
	public static final String EmbouchureCorrection = "embouchureCorrection";
	public static final String ClosedCorrection = "closedCorrection";
	public static final String CutoffFrequency = "cutoffFrequency";
	public static final String FirstHoleDistance = "firstHoleDistance";
	public static final String SubsequentHoleDistance = "subsequentHoleDistance";

	//@formatter:off

	public static final String ClosedCorrectionComment =
		"Closed hole for tone hole n.  The length of the vibrating air " +
		"column is effectively increased by each closed tone hole which "+
		"exists above the first open tone hole.\n"+ 
		"This function is called C_c(n) in the Flutomat code.\n";
	
	public static final String CutoffFrequencyComment =
		"Calculates the cutoff frequency above which the open hole correction "+
		"is not valid.  Instrument should be designed so that all second register "+
		"notes are well below this frequency.\n"+
		"This function is called f_c(n) in the Flutomat code.\n";
	
	public static final String EndCorrectionComment =
		"Calculates the distance from physical open end of flute "+
		"to effective end of vibrating air column. The vibrating air "+
		"column ends beyond the end of the flute and C_end is always " + 
		"positive.\n"+
		"This function is called C_end() in the Flutomat code.\n";

	public static final String EmbouchureCorrectionComment =
		"Calculates the distance from theoretical start of air column to "+
		"the center of embouchure hole; the air column effectively extends "+
		"beyond the blow hole center by this distance.\n"+
		"This function is called C_emb() in the Flutomat code.\n";

	public static final String EffectiveThicknessComment =
		"Effective wall thickness, i.e. height of air column at open "+
		"finger holes - the air column extends out past the end of the hole by some proportion of " + 
		"the hole diameter\n"+
		"This function is called te(n) in the Flutomat code.\n" ;

	public static final String FirstHoleDistanceComment =
		"Calculates the effective distance from the first (\"single\") tone hole to "+
		"the end of the vibrating air column when only that hole is open.\n"+
		"This function is called C_s() in Flutomat code. It is only used in iterative mode ";

	public static final String SubsequentHoleDistanceComment =
		"Calculates the effective distance from the second and subsequent tone holes "+
		"to the end of the vibrating air column when all holes below are open. "+
		"NOTE: the value of this correction is invalid if the frequency of the note "+
		"played is above the cutoff frequency.\n"+
		"This function is called C_o(n) in Flutomat code. It is only used in iterative mode\n";

	public static final String[] FunctionNames = new String[] {
		EndCorrection, ClosedCorrection, CutoffFrequency, 
		EmbouchureCorrection, EffectiveThickness,
		FirstHoleDistance, SubsequentHoleDistance,
	};

	public static final String[] Comments = new String[] {
		EndCorrectionComment, ClosedCorrectionComment, CutoffFrequencyComment, 
		EmbouchureCorrectionComment, EffectiveThicknessComment,
		FirstHoleDistanceComment, SubsequentHoleDistanceComment,
	};

}
