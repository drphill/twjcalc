package twjcalc.application.javascript;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import twjcalc.model.calculate.WhistleCalculator;
import twjcalc.model.whistle.Hole;
import twjcalc.model.whistle.Whistle;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * JavaScriptCalculator.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Calculator instance that dynamically interprets JavaScript to provide the implementation of the
 * protected functions in the UnifiedCalculator.
 */
public class JavaScriptCalculator extends WhistleCalculator implements ScriptConstants {

	private static final String JAVASCRIPT = "JavaScript";
	private final FunctionSet functions;
	private ScriptEngine scriptEngine = null;

	public JavaScriptCalculator() {
		functions = new FunctionSet();
		scriptChanged();
	}

	private Double invoke(final String functionName, final Object parameter) {
		Double value = null;
		if (null != scriptEngine) {
			try {
				final Invocable invocable = (Invocable) scriptEngine;
				final Object obj = invocable.invokeFunction(functionName, parameter);
				if (obj instanceof Double) {
					value = (Double) obj;
				}
			} catch (final ScriptException ex) {
				ex.printStackTrace();
			} catch (final NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		return value;
	}

	@Override
	protected double closedCorrection(final Hole hole) {
		final Double value = invoke(ClosedCorrection, hole);
		if (null != value) {
			return value.doubleValue();
		}
		System.out.println("ClosedCorrection: failed to get return value from JScript");
		return super.closedCorrection(hole);
	}

	@Override
	protected double cutoffFrequency(final Hole hole) {
		final Double value = invoke(CutoffFrequency, hole);
		if (null != value) {
			return value.doubleValue();
		}
		System.out.println("CutoffFrequency: failed to get return value from JScript");
		return super.cutoffFrequency(hole);
	}

	@Override
	protected double effectiveThickness(final Hole hole) {
		final Double value = invoke(EffectiveThickness, hole);
		if (null != value) {
			return value.doubleValue();
		}
		System.out.println("EffectiveThickness: failed to get return value from JScript");
		return super.effectiveThickness(hole);
	}

	@Override
	protected double embouchureCorrection(final Whistle whistle) {
		final Double value = invoke(EmbouchureCorrection, whistle);
		if (null != value) {
			return value.doubleValue();
		}
		System.out.println("EmbouchureCorrection: failed to get return value from JScript");
		return super.embouchureCorrection(whistle);
	}

	@Override
	protected double endCorrection(final Whistle whistle) {
		final Double value = invoke(EndCorrection, whistle);
		if (null != value) {
			return value.doubleValue();
		}
		System.out.println("EndCorrection:  failed to get return value from JScript");
		return super.endCorrection(whistle);
	}

	@Override
	protected double firstHoleDistance(final Hole hole) {
		final Double value = invoke(FirstHoleDistance, hole);
		if (null != value) {
			return value.doubleValue();
		}
		System.out.println("FirstHoleDistance: failed to get return value from JScript");
		return super.firstHoleDistance(hole);
	}

	@Override
	protected double subsequentHoleDistance(final Hole hole) {
		final Double value = invoke(SubsequentHoleDistance, hole);
		if (null != value) {
			return value.doubleValue();
		}
		System.out.println("SubsequentHoleDistance: failed to get return value from JScript");
		return super.subsequentHoleDistance(hole);
	}

	public final FunctionSet getFunctionSet() {
		return functions;
	}

	@Override
	public String getName() {
		if (isIterative()) {
			return "Script Calculator Iterative)";
		}
		return "Script Calculator";
	}

	public final String scriptChanged() {
		final String combinedScript = functions.combinedScript();
		if (null == scriptEngine) {
			final ScriptEngineManager mgr = new ScriptEngineManager();
			scriptEngine = mgr.getEngineByName(JAVASCRIPT);
		}
		if (null != scriptEngine) {
			try {
				scriptEngine.eval(combinedScript);
			} catch (final ScriptException ex) {
				ex.printStackTrace();
			}
		}
		return combinedScript;
	}
}
