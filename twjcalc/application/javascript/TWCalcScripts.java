package twjcalc.application.javascript;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * FlutomatScripts.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Contains the default JavaScript for the functions in the Flutomat style calculations.
 */
public class TWCalcScripts implements ScriptConstants {

	//@formatter:off
	private static final String DefaultClosedCorrectionStr =
		"// This the TWCalc function.\n" + 
		"function closedCorrection(hole) {\n"+
		"    bore = hole.whistle.bore;\n"+
		"    holeSize = hole.size;\n"+
		"    p = (holeSize / bore) * (holeSize / bore);\n"+
		"    cls = (hole.whistle.wallThickness * p) / 4;\n"+
		"    return cls;\n"+
		"}";
	
	private static final String DefaultEmbouchureCorrectionStr =
		"// This the TWCalc function.\n" + 
		"function embouchureCorrection(whistle) {\n"+
		"    emb = whistle.embouchure;\n" + 
		"    embArea = (emb.width * emb.length * 2 * Math.PI);\n" + 
		"    embRadius = Math.sqrt(embArea / Math.PI);\n" + 
		"    bore = whistle.bore;\n" + 
		"    embDist = ((Math.PI * bore * bore * 0.25) / embArea);\n" + 
		"    ccc = ((whistle.wallThickness + 1.5 * embRadius));\n" + 
		"    embDist = embDist * ccc;\n" + 
		"    return embDist;\n" + 
		"    return r;\n" + 
		"}";

	private static final String DefaultEffectiveThicknessStr =
		"// This the TWCalc function.\n" + 
		"function effectiveThickness(hole) {\n"+
		"    return hole.whistle.wallThickness + (0.6133 * hole.size);\n" + 
		"}";

	private static final String DefaultFirstHoleDistanceStr =
		"// This the TWCalc function.\n" + 
		"function firstHoleDistance(hole) {\n"+
		"    bore = hole.whistle.bore;\n"+
		"    holeSize = hole.size;\n"+
		"    pow = (holeSize / bore) * (holeSize / bore);\n"+
	    "    previousHole = hole.whistle.hole[hole.holeNumber - 1];\n"+
	    "    holeSpacing = previousHole.position - hole.position;\n"+
		"    chimney = effectiveThickness(hole);\n"+
		"    q = chimney * (1 / holeSpacing);\n"+
		"    r = pow + q;\n"+
		"    openCorrection = chimney / r;\n"+
		"    return openCorrection;\n"+
	    "}";

	private static final String DefaultSubsequentHoleDistanceStr =
		"// This the TWCalc function.\n" + 
		"function subsequentHoleDistance(hole) {\n"+
		"    prevHole = hole.whistle.hole[hole.holeNumber - 1];\n"+
		"    // if previous hole is a thumbHole we need to go back one more to find\n"+
		"    // an open tone hole.\n"+
		"    if (prevHole.isThumbHole) {\n"+
		"    	prevHole = hole.whistle.hole[hole.holeNumber - 2];\n"+
		"    }\n"+
		"    holeSpacing = prevHole.position - hole.position;\n"+
		"    bore = hole.whistle.bore;\n"+
		"    holeSize = hole.size;\n"+
		"    a = bore / holeSize;\n"+
		"    b = a * a;\n"+
		"    c = 4.0 * effectiveThickness(hole) / holeSpacing * b;\n"+
		"    d = Math.sqrt(1.0 + c);\n"+
		"    openCorrection = holeSpacing * 0.5 * (d - 1.0);\n"+
		"    return openCorrection;\n"+
		"}\n";

	static final String[][] DefaultFunctions = new String[][] {
		{EndCorrection, FlutomatScripts.DefaultEndCorrectionStr}, 
		{ClosedCorrection, DefaultClosedCorrectionStr}, 
		{CutoffFrequency, FlutomatScripts.DefaultCufoffFrequencyStr}, 
		{EmbouchureCorrection, DefaultEmbouchureCorrectionStr}, 
		{EffectiveThickness, DefaultEffectiveThicknessStr},
		{FirstHoleDistance, DefaultFirstHoleDistanceStr, }, 
		{SubsequentHoleDistance, DefaultSubsequentHoleDistanceStr},
	};
	
	//@formatter:on

	public static final String getFunction(final String name) {
		String found = null;
		for (int i = 0; (null == found) && (i < DefaultFunctions.length); i++) {
			if (DefaultFunctions[i][0].equals(name)) {
				found = DefaultFunctions[i][1];
			}

		}
		return found;
	}

	public static final String[] getFunctionNames() {
		final String[] names = new String[DefaultFunctions.length];
		for (int i = 0; i < names.length; i++) {
			names[i] = DefaultFunctions[i][0];
		}
		return names;
	}

}
