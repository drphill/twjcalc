package twjcalc.application.javascript;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * FlutomatScripts.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Contains the default JavaScript for the functions in the Flutomat style calculations.
 */
public class BrackerScripts implements ScriptConstants {

	//@formatter:off
	
	private static final String DefaultEndCorrectionStr =
		"// This the HBracker function.\n" + 
		"function endCorrection(whistle) {\n"+
		"    return 0.56 * whistle.bore;\n" + 
		"}";

	private static final String DefaultEmbouchureCorrectionStr =
		"// This the HBracker function.\n" + 
		"function embouchureCorrection(whistle) {\n"+
		"    embHeight = whistle.embouchure.height; \n"+
		"    bore = whistle.bore;\n"+
		"    embWidth = whistle.embouchure.width;\n"+
		"    embLength = whistle.embouchure.length;\n"+
		"    Bd = (bore * bore) / (embWidth * embLength);\n"+
		"    De = (embWidth + embLength) / 2;\n"+
		"    return Bd * (embHeight + 0.3 * De);\n"+
		"}";

	static final String[][] DefaultFunctions = new String[][] {
		{EndCorrection, DefaultEndCorrectionStr}, 
		{ClosedCorrection, FlutomatScripts.DefaultClosedCorrectionStr}, 
		{CutoffFrequency, FlutomatScripts.DefaultCufoffFrequencyStr}, 
		{EmbouchureCorrection, DefaultEmbouchureCorrectionStr}, 
		{EffectiveThickness, FlutomatScripts.DefaultEffectiveThicknessStr},
		{FirstHoleDistance, FlutomatScripts.DefaultFirstHoleDistanceStr, }, 
		{SubsequentHoleDistance, FlutomatScripts.DefaultSubsequentHoleDistanceStr},
	};
	
	//@formatter:on

	public static final String getFunction(final String name) {
		String found = null;
		for (int i = 0; (null == found) && (i < DefaultFunctions.length); i++) {
			if (DefaultFunctions[i][0].equals(name)) {
				found = DefaultFunctions[i][1];
			}

		}
		return found;
	}

	public static final String[] getFunctionNames() {
		final String[] names = new String[DefaultFunctions.length];
		for (int i = 0; i < names.length; i++) {
			names[i] = DefaultFunctions[i][0];
		}
		return names;
	}

}
