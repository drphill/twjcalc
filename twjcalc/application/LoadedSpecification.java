package twjcalc.application;

import java.awt.MenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import twjcalc.model.calculate.Specification;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * LoadedSpecification.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Class to keep track of loaded specifications. This is also a self-naming menuItem.
 */
public class LoadedSpecification extends MenuItem implements ActionListener {
	private static final long serialVersionUID = 1L;
	private String filename;
	private final Specification specification;
	private final TWFrame frame;
	private boolean current = false;

	public LoadedSpecification(final TWFrame frame, final Specification specification, final String filename) {
		super();
		this.frame = frame;
		this.specification = specification;
		setFilename(filename);
		addActionListener(this);
	}

	private final void nameMenuItem() {
		String name = getWhistleName();
		if (null != name) {
		} else if (null != filename) {
			name = filename;
		} else {
			name = "Unnamed";
		}
		if (current) {
			name = "* " + name;
		}
		setLabel(name);
	}

	@Override
	public void actionPerformed(final ActionEvent e) {
		frame.viewSpecification(this);
	}

	public final String getFilename() {
		return filename;
	}

	public final Specification getSpecification() {
		return specification;
	}

	public final String getWhistleComment() {
		return specification.getWhistleComment();
	}

	public final String getWhistleName() {
		return specification.getWhistleName();
	}

	public final boolean isCurrent() {
		return current;
	}

	public final void setCurrent(final boolean current) {
		this.current = current;
		nameMenuItem();
	}

	public final void setFilename(final String filename) {
		this.filename = filename;
		nameMenuItem();
	}

	public final void setWhistleComment(final String whistleComment) {
		specification.setWhistleComment(whistleComment);
	}

	public final void setWhistleName(final String whistleName) {
		specification.setWhistleName(whistleName);
	}

}
