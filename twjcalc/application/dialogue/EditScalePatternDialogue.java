package twjcalc.application.dialogue;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;

import twjcalc.JumpTo;
import twjcalc.application.TWFrame;
import twjcalc.application.xml.XMLParse2;
import twjcalc.application.xml.XmlUnparse2;
import twjcalc.gui.BorderPanel;
import twjcalc.gui.ChangeListener;
import twjcalc.gui.specification.musical.WhistleScaleChoice;
import twjcalc.model.music.ScalePattern;
import twjcalc.model.music.ScalePatternLibrary;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * EditScalePatternDialogue.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
public class EditScalePatternDialogue extends AbstractDialogue implements TextListener, ChangeListener {
	private static final String CANCEL = "Cancel";
	private static final String LOAD = "Load";
	private static final String OK = "Ok";
	private static final String SAVE = "Save";
	private static final String HELP = "Help";
	private static final long serialVersionUID = 1L;

	private final Label error;
	private final TextField name;
	private final Button saveButton, okButton;
	private final TextField value;
	private final TextArea description;
	private final WhistleScaleChoice scaleChoice;

	public EditScalePatternDialogue(final TWFrame frame, final ScalePattern scalePattern) {
		super(frame, "Edit Scale Pattern", true);
		final Panel mainPanel = new Panel(new BorderLayout());
		add(mainPanel);

		Panel p = new Panel(new BorderLayout());
		p.add(new Label("Scale:"), BorderLayout.WEST);
		scaleChoice = new WhistleScaleChoice(this);
		scaleChoice.select(scalePattern);
		p.add(scaleChoice, BorderLayout.CENTER);
		mainPanel.add(new BorderPanel(p), BorderLayout.NORTH);

		final Panel innerPanel = new Panel(new BorderLayout());

		Panel subPanel = new Panel(new GridLayout(0, 1));

		p = new Panel(new BorderLayout());
		p.add(new Label("Name:"), BorderLayout.WEST);
		name = new TextField(scalePattern.getName());
		p.add(name, BorderLayout.CENTER);
		subPanel.add(p);

		p = new Panel(new BorderLayout());
		p.add(new Label("Pattern:"), BorderLayout.WEST);
		value = new TextField(scalePattern.getPattern());
		value.addTextListener(this);
		p.add(value);
		subPanel.add(p);

		error = new Label();
		subPanel.add(error);

		innerPanel.add(subPanel, BorderLayout.NORTH);

		subPanel = new Panel(new GridLayout(0, 1));
		p = new Panel(new BorderLayout());
		p.add(new Label("Description:"), BorderLayout.WEST);
		description = new TextArea(scalePattern.getDescription());
		p.add(description);
		subPanel.add(p);
		innerPanel.add(subPanel, BorderLayout.CENTER);
		mainPanel.add(new BorderPanel(innerPanel), BorderLayout.CENTER);

		final Panel buttonPanel = new Panel();
		okButton = new Button(OK);
		okButton.addActionListener(this);
		buttonPanel.add(okButton);
		saveButton = new Button(SAVE);
		saveButton.addActionListener(this);
		buttonPanel.add(saveButton);
		Button b = new Button(LOAD);
		b.addActionListener(this);
		buttonPanel.add(b);
		b = new Button(CANCEL);
		b.addActionListener(this);
		buttonPanel.add(b);
		b = new Button(HELP);
		b.addActionListener(this);
		buttonPanel.add(b);
		mainPanel.add(new BorderPanel(buttonPanel), BorderLayout.SOUTH);
		pack();
	}

	@Override
	public void actionPerformed(final ActionEvent e) {
		final String cmd = e.getActionCommand();
		if (OK.equals(cmd)) {
			final String pattern = value.getText();
			final String name = this.name.getText();
			final ScalePattern scalePattern = new ScalePattern(name, pattern, false);
			scalePattern.setDescription(description.getText());
			ScalePatternLibrary.add(scalePattern);
			setVisible(false);
		} else if (CANCEL.equals(cmd)) {
			setVisible(false);
		} else if (SAVE.equals(cmd)) {
			final String filename = getSaveFilename("Save Scale Pattern", ".spn");
			if (null != filename) {
				final String pattern = value.getText();
				final String name = this.name.getText();
				final ScalePattern scalePattern = new ScalePattern(name, pattern, false);
				scalePattern.setDescription(description.getText());
				XmlUnparse2.unparseToFile(scalePattern, filename);
			}
		} else if (LOAD.equals(cmd)) {
			final String filename = getLoadFilename("Load Scale Pattern", ".spn");
			if (null != filename) {
				final ScalePattern pattern = XMLParse2.scalePatternFromFile(filename);
				ScalePatternLibrary.add(pattern);
				name.setText(pattern.getName());
				value.setText(pattern.getPattern(", "));
				description.setText(pattern.getDescription());
				checkValidity();
			}
		} else if (HELP.equals(cmd)) {
			JumpTo.url(JumpTo.HELP_EDIT_SCALEPATTERN_URL);
		}
	}

	@Override
	public void changed(final Object sender) {
		// sender is only scaleChoice at the moment
		final ScalePattern pattern = scaleChoice.getSelected();
		name.setText(pattern.getName());
		value.setText(pattern.getPattern(", "));
		description.setText(pattern.getDescription());
		checkValidity();

	}

	public void checkValidity() {
		final String str = value.getText();
		final String errorString = ScalePattern.errorString(str);
		final boolean valid = null == errorString;
		if (null == errorString) {
			error.setForeground(Color.GRAY);
			error.setText("Valid Pattern.");
		} else {
			error.setForeground(Color.RED);
			error.setText("Invalid Pattern: " + errorString);
		}
		// final boolean valid = ScalePattern.isValid(str);
		okButton.setEnabled(valid);
		saveButton.setEnabled(valid);
	}

	@Override
	public void textValueChanged(final TextEvent e) {
		if (e.getID() == TextEvent.TEXT_VALUE_CHANGED) {
			checkValidity();
		}
	}

}
