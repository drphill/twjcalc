package twjcalc.application.dialogue;

import java.awt.BorderLayout;
import java.awt.Choice;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import twjcalc.gui.ChangeListener;
import twjcalc.gui.TopPanel;
import twjcalc.model.music.ET12;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * NoteNameChooser.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Provides a single implementation for choosing a note/octave pair
 */
public class NoteNameChooser extends Panel implements ItemListener {
	private static final int BASE_OCTAVE = 3;
	private static final int NUM_OCTAVES = 3;
	private static final long serialVersionUID = 1L;

	private final ChangeListener listener;
	private final Choice noteNameChoice;

	private final Choice octaveChoice;

	public NoteNameChooser(final String noteName, final ChangeListener listener) {
		super(new GridLayout(1, 0));
		this.listener = listener;

		noteNameChoice = new Choice();
		final String[][] noteNames = ET12.getNotenames();
		for (int i = 0; i < noteNames.length; i++) {
			noteNameChoice.add(noteNames[i][0]);
		}
		noteNameChoice.addItemListener(this);
		noteNameChoice.setBackground(TopPanel.USER_EDIT_COLOUR);

		octaveChoice = new Choice();
		for (int i = 0; i < NUM_OCTAVES; i++) {
			final String str = Integer.toString(BASE_OCTAVE + i);
			octaveChoice.add(str);
		}
		octaveChoice.addItemListener(this);
		octaveChoice.setBackground(TopPanel.USER_EDIT_COLOUR);

		Panel p = new Panel(new BorderLayout());
		p.add(new Label("Base Note:", Label.RIGHT), BorderLayout.WEST);
		p.add(noteNameChoice, BorderLayout.CENTER);
		add(p);

		p = new Panel(new BorderLayout());
		p.add(new Label("Octave:", Label.RIGHT), BorderLayout.WEST);
		p.add(octaveChoice, BorderLayout.CENTER);
		add(p);

		setNoteName(noteName);
	}

	public final String getNoteName() {
		return noteNameChoice.getSelectedItem() + octaveChoice.getSelectedItem();
	}

	@Override
	public void itemStateChanged(final ItemEvent itemEvent) {
		if (null != listener) {
			listener.changed(this);
		}
	}

	public final void setNoteName(final String noteName) {
		final String baseName = ET12.baseName(noteName);
		noteNameChoice.select(baseName);

		final int octave = ET12.octave(noteName);
		octaveChoice.select(Integer.toString(octave));
	}

}
