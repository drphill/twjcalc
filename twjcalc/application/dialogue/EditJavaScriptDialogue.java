package twjcalc.application.dialogue;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Font;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.BoxLayout;

import twjcalc.JumpTo;
import twjcalc.application.TWFrame;
import twjcalc.application.javascript.BrackerScripts;
import twjcalc.application.javascript.FlutomatScripts;
import twjcalc.application.javascript.FunctionSet;
import twjcalc.application.javascript.JavaScriptCalculator;
import twjcalc.application.javascript.ScriptConstants;
import twjcalc.application.javascript.TWCalcScripts;
import twjcalc.application.xml.XMLParse2;
import twjcalc.application.xml.XmlUnparse2;
import twjcalc.gui.BorderPanel;
import twjcalc.gui.MultiLineLable;
import twjcalc.gui.TopPanel;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * EditJavaScriptDialogue.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
public class EditJavaScriptDialogue extends AbstractDialogue implements ScriptConstants, TextListener, ItemListener {
	/**
	 * 
	 */
	private static final String FUNCTIONSET_FILE_EXTENSION = ".fns";
	private static final String CLOSE = "Close";
	private static final String LOAD = "Load";
	private static final String HELP = "Help";
	private static final String SAVE = "Save";
	private static final String APPLY = "Apply";
	private static final String FLUTOMAT = "Flutomat";
	private static final String BRACKER = "Bracker";
	private static final String TWCALC = "TWCalc";

	private static final String FLUTOMAT_ALL = "All Flutomat";
	private static final String BRACKER_ALL = "All Bracker";
	private static final String TWCALC_ALL = "All TWCalc";

	private static final long serialVersionUID = 1L;

	private final JavaScriptCalculator calculator;
	private final Choice functionName;
	private final MultiLineLable commentText;
	private final TextField errorText;
	private final TextArea javaScriptText;
	private final Button applyButton;
	private final Button saveButton;
	private final FunctionSet functionSet;

	public EditJavaScriptDialogue(final TWFrame frame, final JavaScriptCalculator calc) {
		super(frame, "Edit Java Script", true);
		setBackground(TopPanel.USER_EDIT_COLOUR);
		calculator = calc;
		functionSet = calc.getFunctionSet();
		final Panel mainPanel = new Panel(new BorderLayout());
		add(mainPanel);

		Panel p = new Panel(new BorderLayout());
		p.add(new Label("Function Name:"), BorderLayout.WEST);
		functionName = new Choice();
		final String[] funcs = FunctionNames;
		for (int i = 0; i < funcs.length; i++) {
			functionName.add(funcs[i]);
		}
		functionName.addItemListener(this);
		p.add(functionName, BorderLayout.CENTER);
		mainPanel.add(new BorderPanel(p), BorderLayout.NORTH);

		final Panel centrePanel = new Panel(new BorderLayout());
		p = new Panel(new BorderLayout());
		commentText = new MultiLineLable();
		centrePanel.add(commentText, BorderLayout.NORTH);

		javaScriptText = new TextArea("", -1, -1, TextArea.SCROLLBARS_BOTH);
		javaScriptText.addTextListener(this);
		javaScriptText.setFont(new Font("Monospaced", Font.PLAIN, 16));
		p.add(javaScriptText, BorderLayout.CENTER);
		centrePanel.add(p, BorderLayout.CENTER);

		p = new Panel();
		p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
		Button b = new Button(APPLY);
		b.addActionListener(this);
		p.add(b);
		applyButton = b;
		b = new Button(FLUTOMAT);
		b.addActionListener(this);
		p.add(b);
		b = new Button(BRACKER);
		b.addActionListener(this);
		p.add(b);
		b = new Button(TWCALC);
		b.addActionListener(this);
		p.add(b);
		centrePanel.add(p, BorderLayout.EAST);

		errorText = new TextField();
		errorText.setForeground(Color.RED);
		centrePanel.add(errorText, BorderLayout.SOUTH);

		mainPanel.add(new BorderPanel(centrePanel), BorderLayout.CENTER);

		final Panel buttonPanel = new Panel();
		b = new Button(FLUTOMAT_ALL);
		b.addActionListener(this);
		buttonPanel.add(b);
		b = new Button(BRACKER_ALL);
		b.addActionListener(this);
		buttonPanel.add(b);
		b = new Button(TWCALC_ALL);
		b.addActionListener(this);
		buttonPanel.add(b);
		b = new Button(SAVE);
		b.addActionListener(this);
		buttonPanel.add(b);
		saveButton = b;
		b = new Button(LOAD);
		b.addActionListener(this);
		buttonPanel.add(b);
		b = new Button(CLOSE);
		b.addActionListener(this);
		buttonPanel.add(b);
		b = new Button(HELP);
		b.addActionListener(this);
		buttonPanel.add(b);
		mainPanel.add(new BorderPanel(buttonPanel), BorderLayout.SOUTH);

		functionName.select(0);
		final String str = functionName.getSelectedItem();
		select(str);
		pack();
	}

	private void select(final String selected) {
		final String func = functionSet.getFunction(selected);
		setScript(func);
		setComment(selected);
	}

	private void setComment(final String selected) {
		String comment = null;
		for (int i = 0; (null == comment) && (i < FunctionNames.length); i++) {
			if (FunctionNames[i].equals(selected)) {
				comment = Comments[i];
			}

		}
		if (null == comment) {
			comment = "Invalid function name";
		}
		commentText.setText(comment);
	}

	private void setScript(final String func) {
		saveButton.setEnabled(null != func);
		applyButton.setEnabled(null != func);
		javaScriptText.setEnabled(null != func);
		javaScriptText.setText(func);
	}

	@Override
	public void actionPerformed(final ActionEvent e) {
		final String cmd = e.getActionCommand();
		if (APPLY.equals(cmd)) {
			final String javaScript = javaScriptText.getText();
			final String funcName = functionName.getSelectedItem();
			functionSet.setFunction(funcName, javaScript);
			calculator.scriptChanged();
			getFrame().recalculate();
		} else if (FLUTOMAT.equals(cmd)) {
			final String funcName = functionName.getSelectedItem();
			final String javaScript = FlutomatScripts.getFunction(funcName);
			setComment(funcName);
			setScript(javaScript);
		} else if (BRACKER.equals(cmd)) {
			final String funcName = functionName.getSelectedItem();
			final String javaScript = BrackerScripts.getFunction(funcName);
			setComment(funcName);
			setScript(javaScript);
		} else if (TWCALC.equals(cmd)) {
			final String funcName = functionName.getSelectedItem();
			final String javaScript = TWCalcScripts.getFunction(funcName);
			setComment(funcName);
			setScript(javaScript);
		} else if (FLUTOMAT_ALL.equals(cmd)) {
			functionSet.setFlutomat();
			calculator.scriptChanged();
			final String funcName = functionName.getSelectedItem();
			final String javaScript = FlutomatScripts.getFunction(funcName);
			setScript(javaScript);
			calculator.scriptChanged();
			getFrame().recalculate();
		} else if (BRACKER_ALL.equals(cmd)) {
			functionSet.setBracker();
			calculator.scriptChanged();
			final String funcName = functionName.getSelectedItem();
			final String javaScript = BrackerScripts.getFunction(funcName);
			setScript(javaScript);
			calculator.scriptChanged();
			getFrame().recalculate();
		} else if (TWCALC_ALL.equals(cmd)) {
			functionSet.setTWCalc();
			calculator.scriptChanged();
			final String funcName = functionName.getSelectedItem();
			final String javaScript = TWCalcScripts.getFunction(funcName);
			setScript(javaScript);
			calculator.scriptChanged();
			getFrame().recalculate();
		} else if (HELP.equals(cmd)) {
			JumpTo.url(JumpTo.HELP_EDIT_JAVASCRIPT_URL);
		} else if (CLOSE.equals(cmd)) {
			setVisible(false);
		} else if (SAVE.equals(cmd)) {
			final String filename = getSaveFilename("Save Function Set", FUNCTIONSET_FILE_EXTENSION);
			if (null != filename) {
				XmlUnparse2.unparseToFile(functionSet, filename);
			}
		} else if (LOAD.equals(cmd)) {
			final String filename = getLoadFilename("Load Function Set", FUNCTIONSET_FILE_EXTENSION);
			if (null != filename) {
				XMLParse2.functionSetFromFile(functionSet, filename);
				final String funcName = functionName.getSelectedItem();
				final String javaScript = functionSet.getFunction(funcName);
				setComment(funcName);
				setScript(javaScript);
				calculator.scriptChanged();
				getFrame().recalculate();
			}
		}
	}

	@Override
	public void itemStateChanged(final ItemEvent e) {
		final String selected = functionName.getSelectedItem();
		select(selected);
	}

	@Override
	public void textValueChanged(final TextEvent e) {
		if (e.getID() == TextEvent.TEXT_VALUE_CHANGED) {
			final ScriptEngineManager mgr = new ScriptEngineManager();
			final ScriptEngine scriptEngine = mgr.getEngineByName("JavaScript");
			if (null != scriptEngine) {
				try {
					scriptEngine.eval(javaScriptText.getText());
					errorText.setText("");
					applyButton.setEnabled(true);
					saveButton.setEnabled(true);
				} catch (final ScriptException ex) {
					String str = ex.getMessage();
					final int i = str.indexOf(':');
					final int j = str.indexOf('(', i);
					str = str.substring(i + 1, j);
					str = str + " at line " + ex.getLineNumber();
					// final int lineNumber = ex.getLineNumber();
					errorText.setText(str);
					saveButton.setEnabled(false);
					applyButton.setEnabled(false);
				}
			}

		}
	}

}
