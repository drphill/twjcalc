package twjcalc.application.dialogue;

import java.awt.Dialog;
import java.awt.FileDialog;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import twjcalc.application.TWFrame;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * AbstractDialogue.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Provides general functionality, such as centering
 */
public class AbstractDialogue extends Dialog implements ActionListener, WindowListener {
	private static final long serialVersionUID = 1L;

	private final TWFrame frame;

	public AbstractDialogue(final TWFrame frame, final String title, final boolean modal) {
		super(frame, title, modal);
		this.frame = frame;
		addWindowListener(this);
	}

	protected final TWFrame getFrame() {
		return frame;
	}

	@Override
	public void actionPerformed(final ActionEvent arg0) {
	}

	public final String getLoadFilename(final String title, final String type) {
		final FileDialog fd = new FileDialog(this, title, FileDialog.LOAD);
		if (null != frame.getCurrentDirectory()) {
			fd.setDirectory(frame.getCurrentDirectory());
		}
		fd.setFile("*" + type);
		fd.setVisible(true);
		String filename = fd.getFile();
		final String directoryName = fd.getDirectory();
		if ((null != filename) && (null != directoryName)) {
			filename = directoryName + filename;
		} else {
			filename = null;
		}
		return filename;
	}

	public final String getSaveFilename(final String title, final String type) {
		final FileDialog fd = new FileDialog(this, title, FileDialog.SAVE);
		if (null != frame.getCurrentDirectory()) {
			fd.setDirectory(frame.getCurrentDirectory());
		}
		fd.setFile("*" + type);

		fd.setVisible(true);
		String filename = fd.getFile();
		final String directoryName = fd.getDirectory();
		if ((null != filename) && (null != directoryName)) {
			filename = directoryName + filename;
		} else {
			filename = null;
		}
		return filename;
	}

	@Override
	public void setVisible(final boolean show) {
		if (show) {
			pack();
			final Rectangle ownerRect = getParent().getBounds();
			final Rectangle myRect = getBounds();
			final int xoff = (ownerRect.width - myRect.width) / 2;
			final int yoff = (ownerRect.height - myRect.height) / 2;
			myRect.setLocation(xoff, yoff);
			setBounds(myRect);
		}
		super.setVisible(show);
	}

	@Override
	public void windowActivated(final WindowEvent arg0) {
	}

	@Override
	public void windowClosed(final WindowEvent arg0) {
	}

	@Override
	public void windowClosing(final WindowEvent arg0) {
		setVisible(false);
	}

	@Override
	public void windowDeactivated(final WindowEvent arg0) {
	}

	@Override
	public void windowDeiconified(final WindowEvent arg0) {
	}

	@Override
	public void windowIconified(final WindowEvent arg0) {
	}

	@Override
	public void windowOpened(final WindowEvent arg0) {
	}

}
