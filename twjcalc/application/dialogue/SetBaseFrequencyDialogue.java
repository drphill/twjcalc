package twjcalc.application.dialogue;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Panel;
import java.awt.event.ActionEvent;

import twjcalc.JumpTo;
import twjcalc.application.TWFrame;
import twjcalc.gui.BorderPanel;
import twjcalc.gui.ChangeListener;
import twjcalc.gui.LabelledDoubleField;
import twjcalc.gui.TopPanel;
import twjcalc.model.music.ET12;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * SetBaseFrequencyDialogue.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
public class SetBaseFrequencyDialogue extends AbstractDialogue implements ChangeListener {
	private static final String CANCEL = "Cancel";
	private static final String HELP = "Help";
	private static final String OK = "Ok";
	private static final long serialVersionUID = 1L;

	private final TWFrame frame;
	private final NoteNameChooser noteNameChooser;
	private final LabelledDoubleField value;

	public SetBaseFrequencyDialogue(final TWFrame frame) {
		super(frame, "Set Base Frequency", true);
		setBackground(TopPanel.USER_EDIT_COLOUR);
		this.frame = frame;
		final Panel mainPanel = new java.awt.Panel(new BorderLayout());
		add(mainPanel);

		final Panel panel = new Panel();
		final String current = ET12.getLastBaseNoteName();
		final double frequency = ET12.getFrequency(current);
		noteNameChooser = new NoteNameChooser(current, this);
		panel.add(noteNameChooser);

		value = new LabelledDoubleField("Frequency", frequency, null);
		panel.add(value);

		mainPanel.add(new BorderPanel(panel), BorderLayout.CENTER);

		final java.awt.Panel buttonPanel = new java.awt.Panel();
		Button b = new Button(OK);
		b.addActionListener(this);
		buttonPanel.add(b);
		b = new Button(CANCEL);
		b.addActionListener(this);
		buttonPanel.add(b);
		b = new Button(HELP);
		b.addActionListener(this);
		buttonPanel.add(b);
		mainPanel.add(new BorderPanel(buttonPanel), BorderLayout.SOUTH);
		pack();
	}

	@Override
	public void actionPerformed(final ActionEvent e) {
		final String cmd = e.getActionCommand();
		if (CANCEL.equals(cmd)) {
			setVisible(false);
		} else if (OK.equals(cmd)) {
			final String noteName = noteNameChooser.getNoteName();
			final double frequency = value.getValue();
			ET12.setBaseFrequency(noteName, frequency);
			setVisible(false);
		} else if (HELP.equals(cmd)) {
			JumpTo.url(JumpTo.HELP_EDIT_BASEFREQUENCY_URL);
		}
	}

	@Override
	public void changed(final Object sender) {
		final String noteName = noteNameChooser.getNoteName();
		final double frequency = ET12.getFrequency(noteName);
		value.setValue(frequency);
	}

	@Override
	public void setVisible(final boolean show) {
		if (show) {

		}
		super.setVisible(show);
		if (!show) {
			frame.recalculate();
		}
	}

}
