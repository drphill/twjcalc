package twjcalc.application.dialogue;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.event.ActionEvent;

import twjcalc.application.TWFrame;
import twjcalc.gui.BorderPanel;
import twjcalc.gui.TopPanel;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * EditDrillSizesDialogue.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
public class EditWhistleCommentDialogue extends AbstractDialogue {
	private static final String CANCEL = "Cancel";
	private static final String OK = "Ok";
	private static final long serialVersionUID = 1L;

	private String newComment = null;

	private final TextArea value;

	public EditWhistleCommentDialogue(final TWFrame frame, final String comment) {
		super(frame, "Edit Whistle Comment", true);
		setBackground(TopPanel.USER_EDIT_COLOUR);
		final Panel mainPanel = new Panel(new BorderLayout());
		add(mainPanel);
		value = new TextArea(comment, -1, -1, TextArea.SCROLLBARS_VERTICAL_ONLY);
		mainPanel.add(new BorderPanel(value), BorderLayout.CENTER);
		final Panel buttonPanel = new Panel();
		Button b = new Button(OK);
		b.addActionListener(this);
		buttonPanel.add(b);
		b = new Button(CANCEL);
		b.addActionListener(this);
		buttonPanel.add(b);
		mainPanel.add(new BorderPanel(buttonPanel), BorderLayout.SOUTH);
		pack();
	}

	@Override
	public void actionPerformed(final ActionEvent e) {
		final String cmd = e.getActionCommand();
		if (OK.equals(cmd)) {
			newComment = value.getText();
			setVisible(false);
		} else if (CANCEL.equals(cmd)) {
			newComment = null;
			setVisible(false);
		}
	}

	public final String getNewComment() {
		return newComment;
	}
}
