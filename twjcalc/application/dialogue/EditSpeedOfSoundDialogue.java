package twjcalc.application.dialogue;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Panel;
import java.awt.event.ActionEvent;

import twjcalc.application.TWFrame;
import twjcalc.gui.BorderPanel;
import twjcalc.gui.LabelledDoubleField;
import twjcalc.gui.TopPanel;
import twjcalc.model.calculate.WhistleCalculator;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * EditSpeedOfSoundDialogue.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
public class EditSpeedOfSoundDialogue extends AbstractDialogue {
	private static final String CANCEL = "Cancel";
	private static final String OK = "Ok";
	private static final long serialVersionUID = 1L;

	private final LabelledDoubleField value;

	public EditSpeedOfSoundDialogue(final TWFrame frame) {
		super(frame, "Edit Speed Of Sound", true);
		setBackground(TopPanel.USER_EDIT_COLOUR);
		final Panel mainPanel = new Panel(new BorderLayout());
		add(mainPanel);
		value = new LabelledDoubleField("Speed of Sound (mm/s)", WhistleCalculator.getSpeedOfSound());
		mainPanel.add(new BorderPanel(value), BorderLayout.CENTER);
		final Panel buttonPanel = new Panel();
		Button b = new Button(OK);
		b.addActionListener(this);
		buttonPanel.add(b);
		b = new Button(CANCEL);
		b.addActionListener(this);
		buttonPanel.add(b);
		mainPanel.add(new BorderPanel(buttonPanel), BorderLayout.SOUTH);
		validate();
	}

	@Override
	public void actionPerformed(final ActionEvent e) {
		final String cmd = e.getActionCommand();
		if (OK.equals(cmd)) {
			final double val = value.getValue();
			WhistleCalculator.setSpeedOfSound(val);
			getFrame().recalculate();
			setVisible(false);
		} else if (CANCEL.equals(cmd)) {
			setVisible(false);
		}
	}

}
