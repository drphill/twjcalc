package twjcalc.application.dialogue;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;

import twjcalc.JumpTo;
import twjcalc.application.TWFrame;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * AboutDialogue.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
public class AboutDialogue extends AbstractDialogue {
	private static final String CLOSE = "Close";
	private static final long serialVersionUID = 1L;

	public AboutDialogue(final TWFrame frame) {
		super(frame, "About Whistle Calculator", true);

		final java.awt.Panel mainPanel = new java.awt.Panel(new GridLayout(0, 1));
		add(mainPanel);

		Label label = new Label("Version " + JumpTo.VERSION + ";  Copyright 2010 Phill van Leersum");
		mainPanel.add(BorderLayout.CENTER, label);
		label = new Label("");
		mainPanel.add(BorderLayout.CENTER, label);

		label =
			new Label(
					"Whistle Calculator is free software: you can redistribute it and/or modify it under the terms of");
		mainPanel.add(BorderLayout.CENTER, label);
		label = new Label("the GNU General Public License as published by the Free Software Foundation,");
		mainPanel.add(BorderLayout.CENTER, label);
		label = new Label("either version 3 of the License, or (at your option) any later version.");
		mainPanel.add(BorderLayout.CENTER, label);
		label = new Label("");
		mainPanel.add(BorderLayout.CENTER, label);
		label = new Label("The Bracker algorithm for calculating hole placement was supplied by Hans Bracker.");
		mainPanel.add(BorderLayout.CENTER, label);
		label = new Label("The TWCalc algorithm for calculating hole placement was supplied by Daniel Bingamon.");
		mainPanel.add(BorderLayout.CENTER, label);
		label = new Label("The Flutomat algorithm for calculating hole placement was supplied by Pete Kosel.");
		mainPanel.add(BorderLayout.CENTER, label);

		final java.awt.Panel buttonPanel = new java.awt.Panel();
		final Button b = new Button(CLOSE);
		b.addActionListener(this);
		buttonPanel.add(b);
		mainPanel.add(buttonPanel);
		pack();
	}

	@Override
	public void actionPerformed(final ActionEvent arg0) {
		setVisible(false);
	}

}
