package twjcalc.application.dialogue;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;

import twjcalc.JumpTo;
import twjcalc.application.TWFrame;
import twjcalc.application.xml.XMLParse2;
import twjcalc.application.xml.XmlUnparse2;
import twjcalc.gui.BorderPanel;
import twjcalc.gui.StringClean;
import twjcalc.gui.TopPanel;
import twjcalc.model.DrillSet;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * EditDrillSizesDialogue.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
public class EditDrillSizesDialogue extends AbstractDialogue implements TextListener {
	private static final String CANCEL = "Cancel";
	private static final String LOAD = "Load";
	private static final String OK = "Ok";
	private static final String SAVE = "Save";
	private static final String HELP = "Help";
	private static final long serialVersionUID = 1L;

	private final DrillSet drillSet, tempDrillSet;
	private final TextField name;
	private final TextArea value;

	private String filename = null;

	public EditDrillSizesDialogue(final TWFrame frame, final DrillSet drillSet) {
		super(frame, "Edit Drill Sizes", true);
		setBackground(TopPanel.USER_EDIT_COLOUR);
		this.drillSet = drillSet;
		tempDrillSet = new DrillSet(drillSet);

		final Panel mainPanel = new Panel(new BorderLayout());
		add(mainPanel);

		name = new TextField(drillSet.getName());
		final Panel p = new Panel(new BorderLayout());
		p.add(new Label("Drill Set Name:"), BorderLayout.WEST);
		p.add(name, BorderLayout.CENTER);
		mainPanel.add(new BorderPanel(p), BorderLayout.NORTH);

		value = new TextArea(drillSet.unparse(), -1, -1, TextArea.SCROLLBARS_VERTICAL_ONLY);
		value.addTextListener(this);
		mainPanel.add(new BorderPanel(value), BorderLayout.CENTER);
		final Panel buttonPanel = new Panel();
		Button b = new Button(OK);
		b.addActionListener(this);
		buttonPanel.add(b);
		b = new Button(SAVE);
		b.addActionListener(this);
		buttonPanel.add(b);
		b = new Button(LOAD);
		b.addActionListener(this);
		buttonPanel.add(b);
		b = new Button(CANCEL);
		b.addActionListener(this);
		buttonPanel.add(b);
		b = new Button(HELP);
		b.addActionListener(this);
		buttonPanel.add(b);
		mainPanel.add(new BorderPanel(buttonPanel), BorderLayout.SOUTH);
		pack();
	}

	@Override
	public void actionPerformed(final ActionEvent e) {
		final String cmd = e.getActionCommand();
		if (OK.equals(cmd)) {
			final String values = value.getText();
			drillSet.setName(name.getText());
			drillSet.parse(values);
			if (null != filename) {
				drillSet.setFilename(filename);
			}
			setVisible(false);
		} else if (CANCEL.equals(cmd)) {
			setVisible(false);
		} else if (SAVE.equals(cmd)) {
			filename = getSaveFilename("Save Drill Set", ".drs");
			if (null != filename) {
				final String values = value.getText();
				tempDrillSet.setName(name.getText());
				tempDrillSet.parse(values);
				XmlUnparse2.unparseToFile(tempDrillSet, filename);
			}
		} else if (LOAD.equals(cmd)) {
			filename = getLoadFilename("Load Drill Set", ".drs");
			if (null != filename) {
				XMLParse2.drillSetFromFile(tempDrillSet, filename);
				value.setText(tempDrillSet.unparse());
				name.setText(tempDrillSet.getName());
			}
		} else if (HELP.equals(cmd)) {
			JumpTo.url(JumpTo.HELP_EDIT_DRILLSIZES_URL);
		}
	}

	@Override
	public void textValueChanged(final TextEvent e) {
		if (e.getID() == TextEvent.TEXT_VALUE_CHANGED) {
			final String str = value.getText();
			final String cleanStr = StringClean.clean(str, "1234567890,. ");
			if (!cleanStr.equals(str)) {
				int cpos = value.getCaretPosition() - 1;
				if (cpos < 0) {
					cpos = 0;
				}
				value.setText(cleanStr);
				value.setCaretPosition(cpos);
			}
		}

	}

}
