package twjcalc.application;

import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.logging.Logger;

import twjcalc.JumpTo;
import twjcalc.application.dialogue.AboutDialogue;
import twjcalc.application.dialogue.EditDrillSizesDialogue;
import twjcalc.application.dialogue.EditJavaScriptDialogue;
import twjcalc.application.dialogue.EditScalePatternDialogue;
import twjcalc.application.dialogue.EditSpeedOfSoundDialogue;
import twjcalc.application.dialogue.EditWhistleCommentDialogue;
import twjcalc.application.dialogue.EditWhistleNameDialogue;
import twjcalc.application.dialogue.SetBaseFrequencyDialogue;
import twjcalc.application.javascript.JavaScriptCalculator;
import twjcalc.application.xml.XMLParse2;
import twjcalc.application.xml.XmlUnparse2;
import twjcalc.gui.TopPanel;
import twjcalc.model.DrillSet;
import twjcalc.model.calculate.CalculatorRegister;
import twjcalc.model.calculate.Specification;
import twjcalc.model.music.ScalePattern;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * TWFrame.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Top level frame for running as an application
 */
public class TWFrame extends Frame {
	private static final long serialVersionUID = 1L;
	private static final String UNSAVED = "Unsaved";
	JavaScriptCalculator calc = null;

	private String _currentDirectory;

	private final TopPanel panel;
	private LoadedSpecification currentSpec;
	private final ArrayList loadedSpecs = new ArrayList();
	private final TWMenuBar menuBar;

	public TWFrame() {
		super(JumpTo.VERSION + ": Unnamed Specification");
		setBounds(0, 0, 1000, 750); // default size

		menuBar = new TWMenuBar(this);
		setMenuBar(menuBar);

		final Specification specification = new Specification();
		currentSpec = new LoadedSpecification(this, specification, null);
		currentSpec.setCurrent(true);

		menuBar.add(currentSpec);

		panel = new TopPanel(specification);
		add(panel);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent arg0) {
				exit();
			};
		});
		XMLParse2.readConfig(this);
		setTitle();

		initialiseJScript();
	}

	private final void setTitle() {
		String title = JumpTo.VERSION + ": ";
		String name = currentSpec.getWhistleName();
		if (null == name) {
			name = "Unnamed Whistle";
		}
		title += name;
		String filename = currentSpec.getFilename();
		if (null == filename) {
			filename = UNSAVED;
		}
		title += " (" + filename + ")";
		setTitle(title);
	}

	/**
	 * Internal version - do the bare minimum
	 * 
	 * @param specToUnload
	 */
	private void unloadSpecification(final LoadedSpecification specToUnload) {
		specToUnload.setCurrent(false);
		menuBar.remove(specToUnload);
		loadedSpecs.remove(specToUnload);
	}

	final void initialiseJScript() {
		calc = new JavaScriptCalculator();
		CalculatorRegister.register(calc);
	}

	public void about() {
		final AboutDialogue dlg = new AboutDialogue(this);
		dlg.setVisible(true);
	}

	public void dumpLog() {
		Logger.getLogger("twjcalc").severe("Dump Log");
	}

	public void editDrillSet() {
		final DrillSet drillSet = panel.getDrillSet();
		final EditDrillSizesDialogue dlg = new EditDrillSizesDialogue(this, drillSet);
		dlg.setVisible(true);
	}

	public void editJavaScript() {
		new EditJavaScriptDialogue(this, calc).setVisible(true);
	}

	public void editScalePattern() {
		final ScalePattern current = panel.getCurrentScalePattern();
		new EditScalePatternDialogue(this, current).setVisible(true);
	}

	public void editSpeedOfSound() {
		new EditSpeedOfSoundDialogue(this).setVisible(true);
	}

	public final void editWhistleComment() {
		String comment = currentSpec.getWhistleComment();
		final EditWhistleCommentDialogue dlg = new EditWhistleCommentDialogue(this, comment);
		dlg.setVisible(true);
		comment = dlg.getNewComment();
		if (null != comment) {
			currentSpec.setWhistleComment(comment);
			specHasChanged();
		}
	}

	public final void editWhistleName() {
		String name = currentSpec.getWhistleName();
		final EditWhistleNameDialogue dlg = new EditWhistleNameDialogue(this, name);
		dlg.setVisible(true);
		name = dlg.getNewName();
		if (null != name) {
			currentSpec.setWhistleName(name);
			setTitle();
			specHasChanged();
		}
	}

	public void exit() {
		XmlUnparse2.saveConfig(this);
		System.exit(0);
	}

	public String getCurrentDirectory() {
		return _currentDirectory;
	}

	public String getDrillSetFilename() {
		return panel.getDrillSet().getFilename();
	}

	public ArrayList getLoadedSpecs() {
		return loadedSpecs;
	}

	public final String getLoadFilename(final String title, final String type) {
		final FileDialog fd = new FileDialog(this, title, FileDialog.LOAD);
		if (null != _currentDirectory) {
			fd.setDirectory(_currentDirectory);
		}
		fd.setFile("*" + type);
		fd.setVisible(true);
		String filename = fd.getFile();
		final String directoryName = fd.getDirectory();
		if ((null != filename) && (null != directoryName)) {
			filename = directoryName + filename;
		} else {
			filename = null;
		}
		return filename;
	}

	public final String getSaveFilename(final String title, final String type) {
		final FileDialog fd = new FileDialog(this, title, FileDialog.SAVE);
		if (null != _currentDirectory) {
			fd.setDirectory(_currentDirectory);
		}
		fd.setFile("*" + type);

		fd.setVisible(true);
		String filename = fd.getFile();
		if (!filename.endsWith(type)) {
			filename = filename + type;
		}
		final String directoryName = fd.getDirectory();
		if ((null != filename) && (null != directoryName)) {
			filename = directoryName + filename;
		} else {
			filename = null;
		}
		return filename;
	}

	public boolean getShowCutoff() {
		return menuBar.isShowCutoff();
	}

	public boolean getShowGraph() {
		return menuBar.isShowGraph();
	}

	/**
	 * 
	 */
	public void help() {
		JumpTo.url(JumpTo.HELP_URL);
	}

	public void lineDrawWhistle(final boolean state) {

	}

	public void loadDrillSet(final String filename) {
		if (null != filename) {
			final DrillSet drillSet = panel.getDrillSet();
			XMLParse2.drillSetFromFile(drillSet, filename);
			drillSet.setFilename(filename);
		}

	}

	public void loadReplace(final String filename) {
		final Specification specification = XMLParse2.specificationFromFile(filename);
		if (null != specification) {
			final LoadedSpecification unloadThisOne = currentSpec;
			panel.setSpecification(specification);
			currentSpec = new LoadedSpecification(this, specification, filename);
			loadedSpecs.add(currentSpec);
			menuBar.add(currentSpec);
			unloadSpecification(unloadThisOne);
			setTitle();
		}
	}

	public void loadWhistle() {
		final String filename = getLoadFilename("Load Whistle", ".wdf");
		if (null != filename) {
			loadWhistle(filename);
		}
	}

	public void loadWhistle(final String filename) {
		final Specification specification = XMLParse2.specificationFromFile(filename);
		if (null != specification) {
			currentSpec.setCurrent(false);
			panel.setSpecification(specification);
			currentSpec = new LoadedSpecification(this, specification, filename);
			currentSpec.setCurrent(true);
			loadedSpecs.add(currentSpec);
			menuBar.add(currentSpec);
			setTitle();
		}
	}

	public void loadWhistleReplace() {
		final String filename = getLoadFilename("Load Whistle", ".wdf");
		if (null != filename) {
			loadReplace(filename);
		}
	}

	public void newWhistle() {
		final Specification specification = new Specification();
		currentSpec.setCurrent(false);
		currentSpec = new LoadedSpecification(this, specification, UNSAVED);
		currentSpec.setCurrent(true);
		loadedSpecs.add(currentSpec);
		menuBar.add(currentSpec);
		specHasChanged();
	}

	public void newWhistleReplace() {
		final LoadedSpecification toUnload = currentSpec;
		final Specification specification = new Specification();
		currentSpec = new LoadedSpecification(this, specification, UNSAVED);
		loadedSpecs.add(currentSpec);
		menuBar.add(currentSpec);
		unloadSpecification(toUnload);
		specHasChanged();
	}

	public void recalculate() {
		panel.recalculate();
	}

	public void saveWhistle() {
		final String filename = getSaveFilename("Save Whistle", ".wdf");
		if (null != filename) {
			final Specification spec = panel.getSpecification();
			XmlUnparse2.unparseToFile(spec, filename);
			currentSpec.setFilename(filename);
			setTitle();
		}
	}

	public void setBaseFrequency() {
		new SetBaseFrequencyDialogue(this).setVisible(true);
	}

	public void showCutoffRatio(final boolean state) {
		menuBar.setShowCutoff(state);
		panel.showCutoffRatio(state);
	}

	public void showGraph(final boolean show) {
		menuBar.setShowGraph(show);
		panel.showGraph(show);
	}

	public void specHasChanged() {
		panel.setSpecification(currentSpec.getSpecification());
	}

	public void translucentWhistle(final boolean translucent) {

	}

	public void translucentWhistleFromMenu(final boolean translucent) {

	}

	/**
	 * Externally called - we must ensure that there is always a valid spec.
	 */
	public void unloadWhistle() {
		unloadSpecification(currentSpec);
		if (loadedSpecs.size() > 0) {
			currentSpec = (LoadedSpecification) loadedSpecs.get(0);
			specHasChanged();
		} else {
			newWhistle();
		}
		setTitle();
		specHasChanged();
	}

	public void userManual() {
		JumpTo.url(JumpTo.USER_GUIDE_URL);
	}

	public void viewSpecification(final LoadedSpecification loadedSpec) {
		currentSpec.setCurrent(false);
		currentSpec = loadedSpec;
		currentSpec.setCurrent(true);
		setTitle();
		panel.setSpecification(loadedSpec.getSpecification());
	}
}
