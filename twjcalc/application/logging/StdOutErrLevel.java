package twjcalc.application.logging;

import java.io.InvalidObjectException;
import java.io.ObjectStreamException;
import java.util.logging.Level;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * StdOutErrLevel.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Class defining 2 new Logging levels, one for STDOUT, one for STDERR, used when multiplexing
 * STDOUT and STDERR into the same rolling log file via the Java Logging APIs.
 * <p>
 * This code was copied from:
 * http://blogs.sun.com/nickstephen/entry/java_redirecting_system_out_and.html
 */
public class StdOutErrLevel extends Level {

	private static final long serialVersionUID = 1L;

	/**
	 * Level for STDOUT activity.
	 */
	public static Level STDOUT = new StdOutErrLevel("STDOUT", Level.INFO.intValue() + 53);
	/**
	 * Level for STDERR activity
	 */
	public static Level STDERR = new StdOutErrLevel("STDERR", Level.INFO.intValue() + 54);

	/**
	 * Private constructor
	 */
	private StdOutErrLevel(final String name, final int value) {
		super(name, value);
	}

	/**
	 * Method to avoid creating duplicate instances when deserializing the object.
	 * 
	 * @return the singleton instance of this <code>Level</code> value in this classloader
	 * @throws ObjectStreamException
	 *             If unable to deserialize
	 */
	protected Object readResolve() throws ObjectStreamException {
		if (intValue() == STDOUT.intValue()) {
			return STDOUT;
		}
		if (intValue() == STDERR.intValue()) {
			return STDERR;
		}
		throw new InvalidObjectException("Unknown instance :" + this);
	}

}
