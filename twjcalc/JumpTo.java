/*
 * Copyright 2010 Phill van Leersum
 * 
 * Things.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 *
 *
 */
package twjcalc;

import java.io.IOException;
import java.net.URISyntaxException;

/*
 * Copyright 2011 Phill van Leersum
 * 
 * JumpTo.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 *
 */
public class JumpTo {
	// TODO: allow either web or file URLs

	public static final String BASE_URL = "http://twjcalc.sourceforge.net/2.10/";

	public static final String HELP_URL = "UserGuide/help/Help.html";
	public static final String HELP_EDIT_JAVASCRIPT_URL = "UserGuide/help/HelpEditJavaScript.html";
	public static final String HELP_EDIT_SCALEPATTERN_URL = "UserGuide/help/HelpEditScalePatterns.html";
	public static final String HELP_EDIT_DRILLSIZES_URL = "UserGuide/help/HelpEditDrillSet.html";
	public static final String HELP_EDIT_BASEFREQUENCY_URL = "UserGuide/help/HelpEditBaseFrequency.html";
	public static final String USER_GUIDE_URL = "UserGuide/UserGuide.html";
	public static final String VERSION = "Whistle Calculator, version 2.10 (test)";

	public static final void url(final String str) {
		if (java.awt.Desktop.isDesktopSupported()) {
			final java.awt.Desktop desktop = java.awt.Desktop.getDesktop();
			java.net.URI uri;
			try {
				final String strUrl = JumpTo.BASE_URL + str;
				uri = new java.net.URI(strUrl);
				desktop.browse(uri);
			} catch (final URISyntaxException e) {
				e.printStackTrace();
			} catch (final IOException e) {
				e.printStackTrace();
			}
		}
	}

}
