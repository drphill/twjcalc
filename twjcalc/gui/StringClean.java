package twjcalc.gui;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * StringClean.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Utility class to remove unwanted characters from Strings
 */
public class StringClean {

	public static final String clean(final String toClean, final String allowedChars) {
		final char[] character = toClean.toCharArray();
		final StringBuffer output = new StringBuffer();
		for (int i = 0; i < character.length; i++) {
			final char c = character[i];
			if (allowedChars.indexOf(c) > -1) {
				output.append(c);
			}
		}
		return output.toString();
	}

}
