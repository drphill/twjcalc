package twjcalc.gui;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * ChangeListener.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Defines the responsibility of an object that listens for change occuring in another object. This
 * decouples the classes in the GUI structure reducing the design entanglement.
 */
public interface ChangeListener {
	public abstract void changed(Object sender);
}
