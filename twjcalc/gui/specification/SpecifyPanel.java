package twjcalc.gui.specification;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Panel;

import twjcalc.gui.BorderPanel;
import twjcalc.gui.ChangeListener;
import twjcalc.gui.TopPanel;
import twjcalc.gui.specification.grid.GridPanel;
import twjcalc.gui.specification.musical.DescriptionPanel;
import twjcalc.gui.specification.musical.EmbouchureDescriptionPanel;
import twjcalc.gui.specification.musical.MusicalDefinitionPanel2;
import twjcalc.gui.specification.musical.TubeDescriptionPanel;
import twjcalc.model.DrillSet;
import twjcalc.model.calculate.CalculatorRegister;
import twjcalc.model.calculate.Specification;
import twjcalc.model.calculate.WhistleCalculator;
import twjcalc.model.music.ScalePattern;
import twjcalc.model.whistle.Whistle;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * SpecifyPanel.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Panel defining the user interface for specifying whistle. Provides a visual representation of a
 * Specification, and handles the calculation of derived values.
 */
public class SpecifyPanel extends Panel implements ChangeListener {

	private static final long serialVersionUID = 1L;
	private WhistleCalculator calculator;
	private final GridPanel gridPanel;
	private final ChangeListener _listener;
	private final MusicalDefinitionPanel2 musicDefPanel2;
	private final TubeDescriptionPanel tubePanel;
	private final EmbouchureDescriptionPanel embouchurePanel;
	private final DescriptionPanel description;

	private Specification _specification;

	public SpecifyPanel(final Specification specification, final ChangeListener listener) {
		super(new BorderLayout());
		_listener = listener;

		musicDefPanel2 = new MusicalDefinitionPanel2(this);
		embouchurePanel = new EmbouchureDescriptionPanel(this);
		tubePanel = new TubeDescriptionPanel(this);
		description = new DescriptionPanel();

		// _gridPanel = new GridPanel(7, this);
		gridPanel = new GridPanel(specification.getWhistle(), this);

		setBackground(TopPanel.USER_EDIT_COLOUR);

		final Panel panel = new Panel(new BorderLayout());

		final Panel p = new Panel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		p.add(new BorderPanel(musicDefPanel2));
		p.add(new BorderPanel(tubePanel));
		p.add(new BorderPanel(embouchurePanel));
		panel.add(p, BorderLayout.EAST);

		// p = new Panel(new BorderLayout());
		// p.add(new BorderPanel(description), BorderLayout.CENTER);
		// panel.add(p, BorderLayout.CENTER);
		panel.add(new BorderPanel(description), BorderLayout.CENTER);
		//
		add(panel, BorderLayout.NORTH);

		add(new BorderPanel(gridPanel), BorderLayout.CENTER);
		// add(new BorderPanel(embouchurePanel), BorderLayout.EAST);
	}

	public final void addGraphControl(final Panel panel) {
		add(panel, BorderLayout.EAST);
	}

	@Override
	public void changed(final Object sender) {
		// the specification has changed, so recalculate

		calculator = CalculatorRegister.getCurrentCalculator();
		_specification.calculate(calculator);

		if (gridPanel != sender) {
			gridPanel.update();
		}
		if (tubePanel != sender) {
			tubePanel.update();
		}
		if (embouchurePanel != sender) {
			embouchurePanel.update();
		}
		if (null != _listener) {
			_listener.changed(this);
		}
	}

	public ScalePattern getCurrentScalePattern() {
		return musicDefPanel2.getCurrentScalePattern();
	}

	public DrillSet getDrillSet() {
		return gridPanel.getDrillSet();
	}

	public final EmbouchureDescriptionPanel getEmbouchurePanel() {
		return embouchurePanel;
	}

	public int[] getLableX() {
		return gridPanel.getLableX();
	}

	public final Whistle getWhistle() {
		return _specification.getWhistle();
	}

	public void setSpecification(final Specification specification) {
		_specification = specification;
		musicDefPanel2.setSpecification(_specification);
		embouchurePanel.setSpecification(specification);
		tubePanel.setSpecification(_specification);
		gridPanel.setSpecification(_specification);
		description.setSpecification(_specification);

		changed(null);
	}

	public void showCutoffRatio(final boolean state) {
		gridPanel.showCutoff(state);
	}
}
