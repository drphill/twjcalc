package twjcalc.gui.specification.musical;

import java.awt.Choice;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import twjcalc.gui.ChangeListener;
import twjcalc.model.music.ScalePattern;
import twjcalc.model.music.ScalePatternLibrary;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * WhistleScales.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Presents a list of choices for the whistle scale, and sets the value into the specification
 * supplied.
 */
public class WhistleScaleChoice extends Choice implements ItemListener, ChangeListener {

	private static final long serialVersionUID = 1L;
	private final ChangeListener listener;

	public WhistleScaleChoice(final ChangeListener listener) {
		super();
		this.listener = listener;
		addItemListener(this);
		ScalePatternLibrary.addListener(this);
		final ArrayList scales = ScalePatternLibrary.getAllScalePatterns();
		for (int i = 0; i < scales.size(); i++) {
			final ScalePattern p = (ScalePattern) scales.get(i);
			add(p.getName());
		}
		select(0);
	}

	@Override
	public void changed(final Object sender) {
		removeAll();
		final ArrayList scales = ScalePatternLibrary.getAllScalePatterns();
		for (int i = 0; i < scales.size(); i++) {
			final ScalePattern p = (ScalePattern) scales.get(i);
			add(p.getName());
		}
	}

	public final ScalePattern getSelected() {
		return ScalePatternLibrary.get(getSelectedIndex());
	}

	@Override
	public void itemStateChanged(final ItemEvent arg0) {
		if (null != listener) {
			listener.changed(this);
		}
	}

	public final void select(final ScalePattern scalePattern) {
		// final int i = scalePattern.getIndex();
		select(scalePattern.getName());
	}

}
