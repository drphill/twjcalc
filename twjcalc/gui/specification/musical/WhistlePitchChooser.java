package twjcalc.gui.specification.musical;

import java.awt.BorderLayout;
import java.awt.Choice;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import twjcalc.gui.ChangeListener;
import twjcalc.gui.TopPanel;
import twjcalc.model.music.ET12;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * WhistlePitchChooser.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Maps between full name (e.g. Bb4) and partName (e.g. Bb) / register (e.g. Low)
 */
public class WhistlePitchChooser extends Panel implements ItemListener {

	private static final int FULLNAME = 2;
	private static final String HIGH = "High";
	private static final String LOW = "Low";
	public static final String BASS = "Bass";

	// @formatter:off
	private static final String[][] whistleNameMapping = new String[][] {
		{BASS, ET12.Bb, "Bb2"}, 
		{BASS, ET12.B, "B2"}, 
		{BASS, ET12.C, "C3"}, 
		{BASS, ET12.Db, "C#3"}, 
		{BASS, ET12.D, "D3"}, 
		{BASS, ET12.Eb, "Eb3"}, 
		{BASS, ET12.E, "E3"}, 
		{BASS, ET12.F, "F3"}, 
		{BASS, ET12.Gb, "F#3"}, 
		{BASS, ET12.G, "G3"}, 
		{BASS, ET12.Ab, "G#3"}, 
		{BASS, ET12.A, "A3"}, 
		{LOW, ET12.Bb, "Bb3"}, 
		{LOW, ET12.B, "B3"}, 
		{LOW, ET12.C, "C4"}, 
		{LOW, ET12.Db, "C#4"}, 
		{LOW, ET12.D, "D4"}, 
		{LOW, ET12.Eb, "Eb4"}, 
		{LOW, ET12.E, "E4"}, 
		{LOW, ET12.F, "F4"}, 
		{LOW, ET12.Gb, "F#4"}, 
		{LOW, ET12.G, "G4"}, 
		{LOW, ET12.Ab, "G#4"}, 
		{LOW, ET12.A, "A4"}, 
		{HIGH, ET12.Bb, "Bb4"}, 
		{HIGH, ET12.B, "B4"}, 
		{HIGH, ET12.C, "C5"}, 
		{HIGH, ET12.Db, "C#5"}, 
		{HIGH, ET12.D, "D5"}, 
		{HIGH, ET12.Eb, "Eb5"}, 
		{HIGH, ET12.E, "E5"}, 
		{HIGH, ET12.F, "F5"}, 
		{HIGH, ET12.Gb, "F#5"}, 
		{HIGH, ET12.G, "G5"}, 
		{HIGH, ET12.Ab, "G#5"}, 
		{HIGH, ET12.A, "A5"},
	};
	// @formatter:on

	private static final int PARTNAME = 1;
	private static final String[] partNames = new String[] {
		ET12.A, ET12.Ab, ET12.G, ET12.Gb, ET12.F, ET12.E, ET12.Eb, ET12.D, ET12.Db, ET12.C, ET12.B, ET12.Bb
	};
	private static final int REGISTER = 0;
	private static final String[] registers = new String[] {
		HIGH, LOW, BASS
	};
	private static final long serialVersionUID = 1L;

	public static final String findPartName(final String fullName) {
		String partName = null;
		for (int i = 0; i < whistleNameMapping.length; i++) {
			if (whistleNameMapping[i][FULLNAME].equalsIgnoreCase(fullName)) {
				partName = whistleNameMapping[i][PARTNAME];
			}
		}
		return partName;
	}

	public static final String findWhistleNote(final String register, final String partName) {
		String note = null;
		for (int i = 0; i < whistleNameMapping.length; i++) {
			if (whistleNameMapping[i][REGISTER].equalsIgnoreCase(register)) {
				if (whistleNameMapping[i][PARTNAME].equalsIgnoreCase(partName)) {
					note = whistleNameMapping[i][FULLNAME];
				}
			}
		}
		return note;
	}

	public static final String findWhistleRegister(final String fullName) {
		String register = null;
		for (int i = 0; i < whistleNameMapping.length; i++) {
			if (whistleNameMapping[i][FULLNAME].equalsIgnoreCase(fullName)) {
				register = whistleNameMapping[i][REGISTER];
			}
		}
		return register;
	}

	private final ChangeListener listener;
	private final Choice noteNameChoice;

	private final Choice registerChoice;

	public WhistlePitchChooser(final String noteName, final ChangeListener listener) {
		super(new BorderLayout());
		this.listener = listener;

		noteNameChoice = new Choice();
		for (int i = 0; i < partNames.length; i++) {
			noteNameChoice.add(partNames[i]);
		}
		noteNameChoice.addItemListener(this);
		noteNameChoice.setBackground(TopPanel.USER_EDIT_COLOUR);

		registerChoice = new Choice();
		for (int i = 0; i < registers.length; i++) {
			registerChoice.add(registers[i]);
		}
		registerChoice.addItemListener(this);
		registerChoice.setBackground(TopPanel.USER_EDIT_COLOUR);

		add(new Label("Base Note:", Label.LEFT), BorderLayout.CENTER);

		final Panel p = new Panel(new GridLayout(1, 0));
		p.add(registerChoice);
		p.add(noteNameChoice);
		add(p, BorderLayout.EAST);

		setNoteName(noteName);
	}

	public final String getNoteName() {
		final String partName = noteNameChoice.getSelectedItem();
		final String register = registerChoice.getSelectedItem();
		return findWhistleNote(register, partName);
	}

	@Override
	public void itemStateChanged(final ItemEvent arg0) {
		if (null != listener) {
			listener.changed(this);
		}
	}

	public final void setNoteName(final String fullName) {
		String str = findPartName(fullName);
		noteNameChoice.select(str);
		str = findWhistleRegister(fullName);
		registerChoice.select(str);
	}
}
