package twjcalc.gui.specification.musical;

import java.awt.BorderLayout;
import java.awt.Choice;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import twjcalc.gui.ChangeListener;
import twjcalc.gui.TopPanel;
import twjcalc.model.calculate.CalculatorRegister;
import twjcalc.model.calculate.Specification;
import twjcalc.model.music.BaseNote;
import twjcalc.model.music.ScalePattern;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * MusicalDefinitionPanel.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Displays the musical definition of the whistle. Allows the user to select the base note and the
 * scale. Displays the notes of the scale and the frequency their frequencies.
 */
public class MusicalDefinitionPanel2 extends Panel implements ItemListener, ChangeListener {
	private static final long serialVersionUID = 1L;

	private final WhistleScaleChoice _scales;
	private final ChangeListener listener;
	private Specification specification;
	private final WhistlePitchChooser whistlePitchChooser;
	private final Choice _calculatorChoice;

	public MusicalDefinitionPanel2(final ChangeListener listener) {
		super(new BorderLayout());
		this.listener = listener;
		final Label l = new Label("Whistle Key", Label.CENTER);
		l.setFont(TopPanel.LABELFONT);
		add(l, BorderLayout.NORTH);
		whistlePitchChooser = new WhistlePitchChooser("D5", this);

		_scales = new WhistleScaleChoice(this);
		_scales.setBackground(TopPanel.USER_EDIT_COLOUR);

		_calculatorChoice = new Choice();
		_calculatorChoice.setBackground(TopPanel.USER_EDIT_COLOUR);
		final ArrayList names = CalculatorRegister.getNames();
		for (int i = 0; i < names.size(); i++) {
			final String name = names.get(i).toString();
			_calculatorChoice.add(name);
		}
		_calculatorChoice.select(0);
		final String calculatorName = _calculatorChoice.getSelectedItem();
		CalculatorRegister.setCurrentCalculatorName(calculatorName);

		CalculatorRegister.addListener(this);

		_calculatorChoice.addItemListener(this);

		final Panel gPanel = new Panel(new GridLayout(0, 1));
		gPanel.add(whistlePitchChooser);

		Panel p = new Panel(new BorderLayout());
		final Label lll = new Label("Scale:", Label.LEFT);
		p.add(lll, BorderLayout.WEST);
		p.add(_scales, BorderLayout.CENTER);
		gPanel.add(p);

		p = new Panel(new BorderLayout());
		p.add(new Label("Calculator", Label.LEFT), BorderLayout.WEST);
		p.add(_calculatorChoice, BorderLayout.CENTER);
		gPanel.add(p);

		add(gPanel, BorderLayout.CENTER);

	}

	@Override
	public void changed(final Object sender) {
		if (null != specification) {
			if (CalculatorRegister.class == sender) {
				_calculatorChoice.removeAll();
				final ArrayList names = CalculatorRegister.getNames();
				for (int i = 0; i < names.size(); i++) {
					final String name = names.get(i).toString();
					_calculatorChoice.add(name);
				}
				final String calculatorName = CalculatorRegister.getCurrentCalculator().getName();
				_calculatorChoice.select(calculatorName);
				// CalculatorRegister.setCurrentCalculatorName(calculatorName);
			}
			final String noteName = whistlePitchChooser.getNoteName();
			final BaseNote baseNote = new BaseNote(noteName);
			specification.setBaseNote(baseNote);

			final ScalePattern scalePattern = _scales.getSelected();
			specification.setScalePattern(scalePattern);
			if (null != listener) {
				listener.changed(this);
			}

		}

	}

	public ScalePattern getCurrentScalePattern() {
		return _scales.getSelected();
	}

	@Override
	public void itemStateChanged(final ItemEvent itemEvent) {
		if (null != specification) {
			final String calculatorName = _calculatorChoice.getSelectedItem();
			CalculatorRegister.setCurrentCalculatorName(calculatorName);
			if (null != listener) {
				listener.changed(this);
			}
		}
	}

	public final void setSpecification(final Specification specification) {
		this.specification = specification;
		final BaseNote note = specification.getBaseNote();
		final String noteName = note.getName();
		whistlePitchChooser.setNoteName(noteName);
		specification.calculateScale();
		final ScalePattern scalePattern = specification.getScalePattern();
		_scales.select(scalePattern);
	}

}
