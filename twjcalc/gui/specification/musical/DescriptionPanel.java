/*
 * Copyright 2010 Phill van Leersum
 * 
 * DescriptionPanel.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 *
 *
 */
package twjcalc.gui.specification.musical;

import java.awt.BorderLayout;
import java.awt.Label;
import java.awt.Panel;

import twjcalc.gui.MultiLineLable;
import twjcalc.gui.TopPanel;
import twjcalc.model.calculate.Specification;

/*
 * Copyright 2011 Phill van Leersum
 * 
 * DescriptionPanel.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 *
 */
public class DescriptionPanel extends Panel {
	private static final long serialVersionUID = 1L;
	private final Label name;
	private final MultiLineLable description;

	/**
	 * 
	 */
	public DescriptionPanel() {
		super(new BorderLayout());
		name = new Label("Whistle Name", Label.CENTER);
		name.setFont(TopPanel.LABELFONT);
		description = new MultiLineLable("Whistle description");
		// description.setEditable(false);
		description.setBackground(TopPanel.USER_EDIT_COLOUR);
		add(name, BorderLayout.NORTH);
		add(description, BorderLayout.CENTER);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.Container#getMinimumSize()
	 */
	// @Override
	// public Dimension getMinimumSize() {
	// final Dimension d = super.getMinimumSize();
	// d.height = sister.getMinimumSize().height;
	// return d;
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.Container#getPreferredSize()
	 */
	// @Override
	// public Dimension getPreferredSize() {
	// final Dimension d = super.getPreferredSize();
	// d.height = sister.getPreferredSize().height;
	// return d;
	// }

	/**
	 * @param specification
	 */
	public void setSpecification(final Specification specification) {
		name.setText(specification.getWhistleName());
		description.setText(specification.getWhistleComment());
	}
}
