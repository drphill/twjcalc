package twjcalc.gui.specification.musical;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;

import javax.swing.BoxLayout;

import twjcalc.gui.ChangeListener;
import twjcalc.gui.LabelledDoubleField;
import twjcalc.gui.NumberRender;
import twjcalc.gui.TopPanel;
import twjcalc.model.calculate.Specification;
import twjcalc.model.whistle.Embouchure;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * PhysicalDescriptionPanel.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
public class TubeDescriptionPanel extends Panel implements ChangeListener {

	private static final String LENGTH_BORE = "Sounding Length / Bore: ";
	private static final String PIPE_LENGTH = "Sounding Length: ";
	private static final long serialVersionUID = 1L;
	private final ChangeListener _listener;
	private final LabelledDoubleField _pipeID;
	private final LabelledDoubleField _pipeWall;
	private Specification _specification;
	private final Label _pipeLength;
	private final Label lengthToBoreRatio;

	public TubeDescriptionPanel(final ChangeListener listener) {
		super(new BorderLayout());
		_listener = listener;
		final Label l = new Label("Tube Size", Label.CENTER);
		l.setFont(TopPanel.LABELFONT);
		add(l, BorderLayout.NORTH);

		_pipeID = new LabelledDoubleField("ID:", 25.4, this);
		_pipeID.setEditBackground(TopPanel.USER_EDIT_COLOUR);
		_pipeWall = new LabelledDoubleField("Thickness:", 2, this);
		_pipeWall.setEditBackground(TopPanel.USER_EDIT_COLOUR);
		// _pipeLength = new Label("Pipe Length: ");

		final Panel gPanel = new Panel(new GridLayout(0, 1));

		final Panel p = new Panel();
		p.setLayout(new BoxLayout(p, BoxLayout.LINE_AXIS));
		p.add(_pipeID);
		p.add(_pipeWall);
		gPanel.add(p);
		_pipeLength = new Label(PIPE_LENGTH, Label.CENTER);
		lengthToBoreRatio = new Label(LENGTH_BORE, Label.CENTER);

		gPanel.add(_pipeLength);
		gPanel.add(lengthToBoreRatio);
		add(gPanel, BorderLayout.CENTER);

	}

	@Override
	public void changed(final Object sender) {
		_specification.setPipe(_pipeID.getValue(), _pipeWall.getValue());
		if (null != _listener) {
			_listener.changed(this);
		}
	}

	public final void setSpecification(final Specification specification) {
		_specification = specification;
		_pipeID.setValue(_specification.getPipeID());
		_pipeWall.setValue(_specification.getPipeWallThickness());
	}

	/**
	 * Called when a change to the _specification needs rendering
	 */
	public void update() {
		final Embouchure emb = _specification.getWhistle().embouchure;
		double f = (emb.whistle.hole[0].position - emb.correction);// - (emb.length / 2);
		String str = PIPE_LENGTH + NumberRender.stringWithDecimalPlaces(f, 3);
		_pipeLength.setText(str);

		f = f / _specification.getPipeID();
		str = LENGTH_BORE + NumberRender.stringWithDecimalPlaces(f, 3);
		lengthToBoreRatio.setText(str);
	}

}
