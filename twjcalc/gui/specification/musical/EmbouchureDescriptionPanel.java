package twjcalc.gui.specification.musical;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;

import twjcalc.gui.ChangeListener;
import twjcalc.gui.LabelledDoubleField;
import twjcalc.gui.TopPanel;
import twjcalc.model.calculate.Specification;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * PhysicalDescriptionPanel.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
public class EmbouchureDescriptionPanel extends Panel implements ChangeListener {

	private static final long serialVersionUID = 1L;
	private final ChangeListener _listener;
	private Specification specification;
	private final LabelledDoubleField embLength;
	private final LabelledDoubleField embWidth;
	private final LabelledDoubleField embHeight;

	public EmbouchureDescriptionPanel(final ChangeListener listener) {
		super(new BorderLayout());
		_listener = listener;
		final Label l = new Label("Embouchure", Label.CENTER);
		l.setFont(TopPanel.LABELFONT);
		add(l, BorderLayout.NORTH);

		embWidth = new LabelledDoubleField("Width", 10, this);
		embWidth.setEditBackground(TopPanel.USER_EDIT_COLOUR);
		embLength = new LabelledDoubleField("Length", 10, this);
		embLength.setEditBackground(TopPanel.USER_EDIT_COLOUR);
		embHeight = new LabelledDoubleField("Height", 2, this);
		embHeight.setEditBackground(TopPanel.USER_EDIT_COLOUR);

		final Panel p = new Panel(new BorderLayout());
		final Panel pp = new Panel(new GridLayout(0, 1));
		pp.add(embWidth);
		pp.add(embLength);
		pp.add(embHeight);
		p.add(pp, BorderLayout.NORTH);
		add(p, BorderLayout.CENTER);

	}

	@Override
	public void changed(final Object sender) {
		specification.setEmbouchure(embLength.getValue(), embWidth.getValue(), embHeight.getValue());
		if (null != _listener) {
			_listener.changed(this);
		}
	}

	public final void setSpecification(final Specification specification) {
		this.specification = specification;
		embWidth.setValue(specification.getEmbouchureWidth());
		embLength.setValue(specification.getEmbouchureLength());
		embHeight.setValue(specification.getEmbouchureHeight());
	}

	/**
	 * Called when a change to the _specification needs rendering
	 */
	public void update() {
		// float f = _specification.getWhistleLength();
		// String str = "Pipe Length: " + FloatRender.twoDP(f);
		// _pipeLength.setText(str);
	}

}
