package twjcalc.gui.specification.grid;

import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.Rectangle;

import twjcalc.gui.ChangeListener;
import twjcalc.model.DrillSet;
import twjcalc.model.calculate.Specification;
import twjcalc.model.whistle.Hole;
import twjcalc.model.whistle.Whistle;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * GridPanel.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
public class GridPanel extends Panel implements ChangeListener {

	public static final Font LABELFONT = new Font("San Serif", Font.BOLD, 16);
	private static final long serialVersionUID = 1L;
	private int _columns = -1;
	private final ChangeListener _listener;
	private Specification _specification;
	private final DrillSet drillSet = new DrillSet(this);
	private EndHoleDisplay endHoleDisplay;
	private HoleEditor[] holeEditor;
	private boolean showCutoff = true;

	public GridPanel(final Whistle whistle, final ChangeListener listener) {
		super();
		_listener = listener;
		redoLayout(whistle);
	}

	private final void changeGrid() {
		for (int i = 0; i < holeEditor.length; i++) {
			holeEditor[i].changeGrid(showCutoff);
		}
		endHoleDisplay.changeGrid(showCutoff);
	}

	private void redoLayout(final Whistle whistle) {
		final int columns = whistle.hole.length - 1;
		if (_columns != columns) {
			holeEditor = new HoleEditor[columns];
			removeAll();
			_columns = columns;
			setLayout(new GridLayout(1, columns + 1, 20, 20));
			if (_columns > 0) {
				endHoleDisplay = new EndHoleDisplay(whistle);
				add(endHoleDisplay);
			}
			for (int i = 0; i < columns; i++) {
				// No hole Editor for whistle Hole Zero
				final Hole hole = whistle.hole[(i + 1)];
				holeEditor[i] = new HoleEditor(i + 1, hole, drillSet, this);
				final Panel p = new Panel();
				p.add(holeEditor[i]);
				add(p);
			}
			if (null != getParent()) {
				changeGrid();
				getParent().validate();
			}
		} else if (null != holeEditor) {
			for (int i = 0; i < holeEditor.length; i++) {
				// No hole Editor for whistle Hole Zero
				holeEditor[i].readHole(whistle.hole[(i + 1)]);
			}
			endHoleDisplay.reread(whistle);
		}
	}

	@Override
	public void changed(final Object sender) {
		if (sender == drillSet) {
			for (int i = 0; i < holeEditor.length; i++) {
				holeEditor[i].use(drillSet);
			}
		}

		if (null != _listener) {
			_listener.changed(this);
		}
		for (int i = 0; i < holeEditor.length; i++) {
			endHoleDisplay.reread(_specification.getWhistle());
			holeEditor[i].readHole();
		}
	}

	public final int getColumns() {
		return _columns;
	}

	public DrillSet getDrillSet() {
		return drillSet;
	}

	/**
	 * @return array of the midX points of the hole data columns INCLUDING THE END HOLE
	 */
	public int[] getLableX() {
		final int baseX = getLocationOnScreen().x;
		final int num = holeEditor.length;
		final int[] pos = new int[num + 1];
		// endHole
		int x = endHoleDisplay.getLocationOnScreen().x;
		Rectangle r = endHoleDisplay.getBounds();
		pos[0] = x - baseX + (r.width / 2);

		for (int i = 0; i < num; i++) {
			final HoleEditor ed = holeEditor[i];
			if (null != ed) {
				x = ed.getLocationOnScreen().x;
				r = ed.getBounds();
				pos[i + 1] = x - baseX + (r.width / 2);
			}
		}
		return pos;
	}

	public final void setSpecification(final Specification specification) {
		_specification = specification;
		update();
	}

	public final void showCutoff(final boolean show) {
		showCutoff = show;
		changeGrid();
	}

	/**
	 * Something has changed that means the grid must update Problem is, this may be a GUI event (we
	 * need to change spec) or a spec change (need to change GUI).
	 */
	public final void update() {
		if (null != _specification) {
			final Whistle whistle = _specification.getWhistle();
			if (null != whistle) {
				redoLayout(whistle);
				endHoleDisplay.reread(whistle);
			}
			for (int i = 0; i < holeEditor.length; i++) {
				holeEditor[i].readHole();
			}
		}
	}

}
