package twjcalc.gui.specification.grid;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.util.ArrayList;

import twjcalc.gui.ChangeListener;
import twjcalc.gui.NumberRender;
import twjcalc.gui.spinner.Spinner;
import twjcalc.gui.spinner.SpinnerListener;
import twjcalc.model.DrillSet;
import twjcalc.model.music.ET12;
import twjcalc.model.whistle.Hole;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * HoleEditor.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 *
 */
public class HoleEditor extends Panel implements SpinnerListener {
	private static final ArrayList<String> _holeSizes;
	private static final ArrayList<String> _offSets;
	private static final long serialVersionUID = 1L;

	static {
		_holeSizes = new ArrayList<String>();
		for (float f = 3; f < 30.4; f += 0.5) {
			_holeSizes.add(Float.toString(f));
		}
		_offSets = new ArrayList<String>();
		for (int f = 0; f < 360; f += 10) {
			_offSets.add(Integer.toString(f));
		}
	}

	private final Label cutoffRatio;
	private final Label distanceFromBottom;
	private Hole hole;
	// private final Spinner holeOffset;

	private final Spinner holeSize;
	private final ChangeListener listener;
	private final Label noteFrequency;
	private final Label noteName;
	private final Panel innerPanel;

	public HoleEditor(final int p, final Hole hole, final DrillSet drillSet, final ChangeListener listener) {
		super(new BorderLayout());
		this.hole = hole;
		this.listener = listener;

		final Label name = new Label("Hole " + p, Label.CENTER);
		name.setFont(GridPanel.LABELFONT);
		add(name, BorderLayout.NORTH);

		String str = ET12.noteName(hole.frequency);
		noteName = new Label(str, Label.CENTER);

		str = NumberRender.stringWithDecimalPlaces(hole.frequency, 2) + " Hz";
		noteFrequency = new Label(str, Label.CENTER);

		holeSize = new Spinner(drillSet.getSizes());
		holeSize.addListener(this);
		str = Double.toString(hole.size);
		holeSize.select(str);

		// holeOffset = new Spinner(_offSets);
		// holeOffset.addListener(this);
		// holeOffset.setWrap(true);
		// str = Integer.toString(hole.offset);
		// holeOffset.select(str);
		final double fromEnd = hole.whistle.hole[0].position - hole.position;
		str = NumberRender.stringWithDecimalPlaces(fromEnd, 3);
		distanceFromBottom = new Label(str, Label.CENTER);
		cutoffRatio = new Label(str, Label.CENTER);

		innerPanel = new Panel(new GridLayout(0, 1, 0, 0));
		innerPanel.add(noteName);
		innerPanel.add(noteFrequency);
		innerPanel.add(holeSize);
		innerPanel.add(distanceFromBottom);

		addComponents(true);

		add(innerPanel, BorderLayout.SOUTH);
		if (null != getParent()) {
			getParent().validate();
		}

	}

	private void addComponents(final boolean showCutoff) {
		if (showCutoff) {
			innerPanel.add(cutoffRatio);
		}
	}

	public final void changeGrid(final boolean showCutoff) {
		innerPanel.remove(cutoffRatio);
		addComponents(showCutoff);
	}

	public final Hole getHole() {
		return hole;
	}

	public void readHole() {
		readHole(hole);
	}

	public void readHole(final Hole hole) {
		this.hole = hole;
		String str = ET12.noteName(hole.frequency);
		noteName.setText(str);
		str = NumberRender.stringWithDecimalPlaces(hole.frequency, 2) + " Hz";
		noteFrequency.setText(str);
		str = Double.toString(hole.size);
		holeSize.select(str);
		final double fromEnd = hole.whistle.hole[0].position - hole.position;
		if (Double.isNaN(fromEnd) || (fromEnd <= 0)) {
			distanceFromBottom.setForeground(Color.RED);
			distanceFromBottom.setText("Error");
		} else {
			distanceFromBottom.setForeground(Color.BLACK);
			str = NumberRender.stringWithDecimalPlaces(fromEnd, 3);
			distanceFromBottom.setText(str);
		}

		final double cutoff = hole.relativeCutoff;
		if (Double.isNaN(cutoff)) {
			cutoffRatio.setForeground(Color.RED);
			cutoffRatio.setText("Error");
		} else if (cutoff < 2.0) {
			cutoffRatio.setForeground(Color.RED);
			str = NumberRender.stringWithDecimalPlaces(cutoff, 3);
			cutoffRatio.setText(str);
		} else {
			cutoffRatio.setForeground(Color.BLACK);
			str = NumberRender.stringWithDecimalPlaces(cutoff, 3);
			cutoffRatio.setText(str);
		}

	}

	public final void setHole(final Hole hole) {
		this.hole = hole;
	}

	@Override
	public void spinValueChanged(final Spinner source, final int newValue) {
		final String str = holeSize.getCurrentValue();
		hole.size = Double.parseDouble(str);
		if (null != listener) {
			listener.changed(this);
		}
	}

	public void use(final DrillSet drillSet) {
		holeSize.use(drillSet.getSizes());
	}

}
