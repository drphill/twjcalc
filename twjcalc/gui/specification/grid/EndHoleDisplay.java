package twjcalc.gui.specification.grid;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;

import twjcalc.gui.NumberRender;
import twjcalc.model.music.ET12;
import twjcalc.model.whistle.Hole;
import twjcalc.model.whistle.Whistle;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * LablesForHoleEditor.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 *
 */
public class EndHoleDisplay extends Panel {
	private static final long serialVersionUID = 1L;
	private final Label holeSize;
	private final Label noteFrequency;
	private final Label noteName;
	private final Label endHole;
	private final Label cutoff;
	private final Panel innerPanel;

	public EndHoleDisplay(final Whistle whistle) {
		super(new BorderLayout());
		final Hole hole = whistle.hole[0];

		final Label name = new Label("End Hole", Label.CENTER);
		name.setFont(GridPanel.LABELFONT);
		add(name, BorderLayout.NORTH);

		String str = ET12.noteName(hole.frequency);
		noteName = new Label(str, Label.CENTER);

		str = NumberRender.stringWithDecimalPlaces(hole.frequency, 2) + " Hz";
		noteFrequency = new Label(str, Label.CENTER);

		str = NumberRender.stringWithDecimalPlaces(whistle.bore, 2);
		holeSize = new Label("Hole Size:", Label.RIGHT);
		final Font font = new Font("Sans Serif", Font.BOLD, 12);
		holeSize.setFont(font);
		endHole = new Label("Position:", Label.RIGHT);
		endHole.setFont(font);
		cutoff = new Label("Cutoff Ratio:", Label.RIGHT);
		cutoff.setFont(font);

		innerPanel = new Panel(new GridLayout(0, 1));
		innerPanel.add(noteName);
		innerPanel.add(noteFrequency);
		innerPanel.add(holeSize);
		innerPanel.add(endHole);
		add(innerPanel, BorderLayout.SOUTH);
		addComponents(true);

	}

	private final void addComponents(final boolean showCutoff) {
		if (showCutoff) {
			innerPanel.add(cutoff);
		}
	}

	public final void changeGrid(final boolean showCutoff) {
		innerPanel.remove(cutoff);
		addComponents(showCutoff);
	}

	public final void reread(final Whistle whistle) {
		final Hole hole = whistle.hole[0];
		String str = ET12.noteName(hole.frequency);
		noteName.setText(str);

		str = NumberRender.stringWithDecimalPlaces(hole.frequency, 2) + " Hz";
		noteFrequency.setText(str);
	}

}
