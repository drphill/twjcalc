package twjcalc.gui.specification;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Checkbox;
import java.awt.Choice;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import twjcalc.gui.TopPanel;
import twjcalc.gui.whistle.WhistlePicturePanel;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * GraphControlPanel.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 */

/**
 * GraphControlPanel
 * <p>
 * Jan 20, 2011
 */
public class GraphControlPanel extends Panel implements ItemListener, ActionListener {

	private static final String DISPLAY_CUTOFF = "Cutoff Graph.";
	private static final String DISPLAY_TUNER_LO = "Tuner - low octave.";
	private static final String DISPLAY_TUNER_HI = "Tuner - high octave.";
	private static final String DISPLAY_TUNER_BOTH = "Tuner - both octaves.";
	private static final String DISPLAY_TUNER_FULL = "Tuner - full range.";

	private static final String TUNER_100 = "100ms samples";
	private static final String TUNER_250 = "250ms samples";
	private static final String TUNER_500 = "500ms samples";
	private static final String TUNER_1000 = "1000ms samples";

	private static final String TUNER_START = "Start";
	private static final String TUNER_STOP = "Stop";

	private static final long serialVersionUID = 1L;
	private final WhistlePicturePanel whistlePicture;
	private final Choice displayChoice, durationChoice;
	private final Button tunerStartStopButton;
	private final Checkbox accumulate;

	/**
	 * GraphControlPanel Constructor
	 */
	public GraphControlPanel(final WhistlePicturePanel whistlePicture) {
		super(new BorderLayout());
		this.whistlePicture = whistlePicture;

		final Label l = new Label("Graph Control", Label.CENTER);
		l.setFont(TopPanel.LABELFONT);
		add(l, BorderLayout.NORTH);

		final Panel controls = new Panel(new GridLayout(0, 1));

		displayChoice = new Choice();
		displayChoice.add(DISPLAY_CUTOFF);
		displayChoice.add(DISPLAY_TUNER_LO);
		displayChoice.add(DISPLAY_TUNER_HI);
		displayChoice.add(DISPLAY_TUNER_BOTH);
		displayChoice.add(DISPLAY_TUNER_FULL);
		displayChoice.select(0);
		displayChoice.addItemListener(this);
		controls.add(displayChoice);

		durationChoice = new Choice();
		durationChoice.add(TUNER_100);
		durationChoice.add(TUNER_250);
		durationChoice.add(TUNER_500);
		durationChoice.add(TUNER_1000);
		durationChoice.select(0);
		durationChoice.setEnabled(false);
		durationChoice.addItemListener(this);
		controls.add(durationChoice);

		accumulate = new Checkbox("Accumulate", false);
		accumulate.addItemListener(this);
		accumulate.setEnabled(false);
		controls.add(accumulate);

		tunerStartStopButton = new Button(TUNER_START);
		tunerStartStopButton.setEnabled(false);
		tunerStartStopButton.addActionListener(this);
		controls.add(tunerStartStopButton);

		add(controls, BorderLayout.CENTER);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(final ActionEvent event) {
		if (tunerStartStopButton == event.getSource()) {
			final String str = event.getActionCommand();
			if (TUNER_START.equals(str)) {
				tunerStartStopButton.setLabel(TUNER_STOP);
				durationChoice.setEnabled(false);
				displayChoice.setEnabled(false);
				accumulate.setEnabled(false);
				whistlePicture.startTuner();
			} else if (TUNER_STOP.equals(str)) {
				durationChoice.setEnabled(true);
				displayChoice.setEnabled(true);
				accumulate.setEnabled(true);
				tunerStartStopButton.setLabel(TUNER_START);
				whistlePicture.stopTuner();
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
	 */
	@Override
	public void itemStateChanged(final ItemEvent event) {
		if ((event.getSource() == displayChoice)) {
			if (displayChoice.getSelectedItem().equals(DISPLAY_CUTOFF)) {
				whistlePicture.stopTuner();
				tunerStartStopButton.setEnabled(false);
				accumulate.setEnabled(false);
				durationChoice.setEnabled(false);
				whistlePicture.showCutoff();
			} else if (displayChoice.getSelectedItem().equals(DISPLAY_TUNER_LO)) {
				tunerStartStopButton.setEnabled(true);
				durationChoice.setEnabled(true);
				accumulate.setEnabled(true);
				whistlePicture.setLowOctave();
			} else if (displayChoice.getSelectedItem().equals(DISPLAY_TUNER_HI)) {
				tunerStartStopButton.setEnabled(true);
				durationChoice.setEnabled(true);
				accumulate.setEnabled(true);
				whistlePicture.setHighOctave();
			} else if (displayChoice.getSelectedItem().equals(DISPLAY_TUNER_BOTH)) {
				tunerStartStopButton.setEnabled(true);
				durationChoice.setEnabled(true);
				accumulate.setEnabled(true);
				whistlePicture.setBothOctaves();
			} else if (displayChoice.getSelectedItem().equals(DISPLAY_TUNER_FULL)) {
				tunerStartStopButton.setEnabled(true);
				durationChoice.setEnabled(true);
				accumulate.setEnabled(true);
				whistlePicture.setFullRange();
			}
		} else if (event.getSource() == durationChoice) {
			if (durationChoice.getSelectedItem().equals(TUNER_100)) {
				whistlePicture.setTunerDuration(100);
			} else if (durationChoice.getSelectedItem().equals(TUNER_250)) {
				whistlePicture.setTunerDuration(250);
			} else if (durationChoice.getSelectedItem().equals(TUNER_500)) {
				whistlePicture.setTunerDuration(500);
			} else if (durationChoice.getSelectedItem().equals(TUNER_1000)) {
				whistlePicture.setTunerDuration(1000);
			}
		} else if (event.getSource() == accumulate) {
			whistlePicture.setAccumulate(accumulate.getState());
		}
	}

}
