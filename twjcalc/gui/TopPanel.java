package twjcalc.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Panel;

import twjcalc.gui.specification.GraphControlPanel;
import twjcalc.gui.specification.SpecifyPanel;
import twjcalc.gui.whistle.WhistlePicturePanel;
import twjcalc.model.DrillSet;
import twjcalc.model.calculate.Specification;
import twjcalc.model.music.ScalePattern;
import twjcalc.model.whistle.Whistle;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * TopPanel.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Panel contains all the GUI elements (apart from the menuBar) that form TWJCalc. This panel can be
 * used in either an application (in a Frame) or in an applet.
 */
public class TopPanel extends Panel implements ChangeListener {
	// TODO: create panel (in specify panel) to control Graph/Tuner/None display
	private static final long serialVersionUID = 1L;
	public static final Font LABELFONT = new Font("San Serif", Font.BOLD, 16);
	public static final Color USER_EDIT_COLOUR = new Color(220, 220, 220);
	private final WhistlePicturePanel _picturePanel;
	private Specification _specification;
	public final SpecifyPanel specificationPanel;
	private final Panel gcp;

	public TopPanel(final Specification specification) {
		super(new BorderLayout());
		_specification = specification;
		specificationPanel = new SpecifyPanel(_specification, this);
		add(specificationPanel, BorderLayout.NORTH);
		_picturePanel = new WhistlePicturePanel(this);

		gcp = new BorderPanel(new GraphControlPanel(_picturePanel));
		specificationPanel.addGraphControl(gcp);

		final Panel p = new BorderPanel(_picturePanel);
		p.setBackground(USER_EDIT_COLOUR);
		add(p, BorderLayout.CENTER);
		setFont(new Font("Sans Serif", Font.PLAIN, 12));
		setSpecification(_specification);
	}

	@Override
	public void changed(final Object sender) {
		if (null != _picturePanel) {
			final Whistle whistle = _specification.getWhistle();
			_picturePanel.setWhistle(whistle);
			_picturePanel.repaint();
		}
	}

	public ScalePattern getCurrentScalePattern() {
		return specificationPanel.getCurrentScalePattern();
	}

	public DrillSet getDrillSet() {
		return specificationPanel.getDrillSet();
	}

	public final int[] getLabelX() {
		return specificationPanel.getLableX();
	}

	public final Specification getSpecification() {
		return _specification;
	}

	public void recalculate() {
		specificationPanel.changed(null);
	}

	public void reInitialise() {
		specificationPanel.setSpecification(_specification);
	}

	public final void setSpecification(final Specification specification) {
		_specification = specification;
		specificationPanel.setSpecification(specification);
	}

	public void showCutoffRatio(final boolean state) {
		specificationPanel.showCutoffRatio(state);
		getParent().validate();
	}

	public void showGraph(final boolean show) {
		if (show) {
			specificationPanel.addGraphControl(gcp);
		} else {
			specificationPanel.remove(gcp);
		}
		getParent().validate();
		_picturePanel.setShowGraph(show);
	}

	public final void specificationHasChanged() {
		specificationPanel.changed(null);
		_picturePanel.repaint();
	}
}
