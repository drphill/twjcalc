package twjcalc.gui.spinner;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * SpinControl.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Provides a visual representation to select from one of a given collection of Strings.
 * <p>
 * notifies a listener when changes made
 * </p>
 */
public class Spinner extends Panel implements ActionListener {
	private static final String DECREMENT = "-";

	private static final String INCREMENT = "+";

	private static final long serialVersionUID = 1L;

	public static final Color USER_EDIT_COLOUR = new Color(210, 210, 210);
	private int _currentIndex;
	private final ArrayList _items;
	private final Label value;

	private boolean wrap = false;

	SpinnerListener listener;

	public Spinner(final ArrayList items) {
		this(items, 0);
	}

	/**
	 * SpinControl constructor comment.
	 */
	public Spinner(final ArrayList items, final int initialIndex) {
		super(new BorderLayout());
		_items = new ArrayList(items);
		_currentIndex = initialIndex;

		Button b = new Button(DECREMENT);
		b.addActionListener(this);
		add(b, BorderLayout.WEST);
		// b.setBackground(USER_EDIT_COLOUR);
		value = new Label(/* Integer.toString(maximum) + " " */);
		// value.setBackground(USER_EDIT_COLOUR);
		value.setAlignment(Label.CENTER);
		value.setText(Integer.toString(_currentIndex));
		add(value, BorderLayout.CENTER);
		b = new Button(INCREMENT);
		b.addActionListener(this);
		// b.setBackground(USER_EDIT_COLOUR);
		add(b, BorderLayout.EAST);
	}

	private void _setCurrentValue() {
		if (wrap) {
			while (_currentIndex >= _items.size()) {
				_currentIndex -= _items.size();
			}
			while (_currentIndex < 0) {
				_currentIndex += _items.size();
			}
		} else {
			if (_currentIndex >= _items.size()) {
				_currentIndex = _items.size() - 1;
			}
			if (_currentIndex < 0) {
				_currentIndex = 0;
			}
		}
		value.setText(_items.get(_currentIndex).toString());
	}

	@Override
	public void actionPerformed(final ActionEvent e) {
		final String cmd = e.getActionCommand();
		if (INCREMENT.equals(cmd)) {
			_currentIndex++;
			_setCurrentValue();
			if (null != listener) {
				listener.spinValueChanged(this, _currentIndex);
			}
		} else if (DECREMENT.equals(cmd)) {
			_currentIndex--;
			_setCurrentValue();
			if (null != listener) {
				listener.spinValueChanged(this, _currentIndex);
			}
		}
	}

	public final void addListener(final SpinnerListener listener) {
		this.listener = listener;
	}

	public final int getCurrentIndex() {
		return _currentIndex;
	}

	public final String getCurrentValue() {
		return _items.get(_currentIndex).toString();
	}

	public final void select(final String str) {
		int index = -1;
		for (int i = 0; (-1 == index) && (i < _items.size()); i++) {
			if (_items.get(i).toString().equals(str)) {
				index = i;
				setCurrentIndex(index);
			}
		}
	}

	public final void setCurrentIndex(final int i) {
		_currentIndex = i;
		_setCurrentValue();
	}

	public final void setWrap(final boolean wrap) {
		this.wrap = wrap;
	}

	public void use(final ArrayList arrayList) {
		final String current = getCurrentValue();
		_items.clear();
		_items.addAll(arrayList);
		select(current);
	}

}
