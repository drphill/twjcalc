package twjcalc.gui.tuner;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * CaptureThread.java is part of Phill van Leersum's EarTrainer program.
 *
 * EarTrainer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * EarTrainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with EarTrainer.  If not, see http://www.gnu.org/licenses/.
 */

/**
 * 
 * CaptureThread - a package private Thread that listens to the microphone, and collects data from
 * it. When enough has been collected the data is passed to the NoteDetector for processing.
 * <p>
 * Jan 16, 2011
 */
final class CaptureThread extends Thread {
	// I tried 16 bits, but it gave a noisier start to the DFT. I had hoped that I could
	// lower the sample time if I increased the range of values, but hat did not work.
	private static final int SampleSizeInBits = 8; // 8 bits are enough for us
	private static final int Channels = 1; // 1 = Mono, 2 = Stereo
	private static final boolean Signed = true; // true,false
	private static final boolean BigEndian = true;
	// false big-endian means that the first byte is the lower 8 bits, the second the upper 8
	// bits. Java is BigEndian But if we use bytes that does not matter

	private volatile boolean capture = true;
	private TargetDataLine targetDataLine;
	private final NoteDetector noteDetector;
	private final int sampleSize;

	/**
	 * CaptureThread Constructor
	 * 
	 * @param noteDetector
	 * @param sampleSize
	 *            number of dataPoints to sample at any one time
	 * @param sampleRate
	 *            choose from 8000,11025,16000,22050,44100
	 */
	CaptureThread(final NoteDetector noteDetector, final int sampleSize, final float sampleRate) {
		this.noteDetector = noteDetector;
		this.sampleSize = sampleSize;
		final AudioFormat audioFormat = new AudioFormat(sampleRate, SampleSizeInBits, Channels, Signed, BigEndian);
		final DataLine.Info dataLineInfo = new DataLine.Info(TargetDataLine.class, audioFormat);
		try {
			targetDataLine = (TargetDataLine) AudioSystem.getLine(dataLineInfo);
			targetDataLine.open(audioFormat);
			targetDataLine.start();
		} catch (final LineUnavailableException e) {
			e.printStackTrace();
		}
	}

	synchronized final void stopCapture() {
		capture = false;
	}

	@Override
	public void run() {
		try {
			while (capture) {
				// Read data from the internal
				// buffer of the data line.
				//
				final byte tempBuffer[] = new byte[sampleSize];
				final int cnt = targetDataLine.read(tempBuffer, 0, tempBuffer.length);
				if (cnt > 0) {
					noteDetector.process(tempBuffer);
					Thread.yield();
				}

			}
		} catch (final Exception e) {
			System.out.println(e);
		} finally {
			targetDataLine.close();
		}
	}// end run
}
