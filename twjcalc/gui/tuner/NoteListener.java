package twjcalc.gui.tuner;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * NoteListener.java is part of Phill van Leersum's EarTrainer program.
 *
 * EarTrainer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * EarTrainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with EarTrainer.  If not, see http://www.gnu.org/licenses/.
 */

/**
 * NoteListener defines the interface for an object that listens to a NoteDetector Jan 17, 2011
 */
public interface NoteListener {
	/**
	 * @param noteName
	 *            : (String) Name of the note detected. This is one of the note names defined in
	 *            ET12. The note detected is the best match for the detected frequency. The note
	 *            name may be null if no note was detected.
	 * @param frequency
	 *            : (double)the frequency that was detected, which is not necessarily the correct
	 *            frequency for the detected note. The frequency may be zero if no note was
	 *            detected.
	 * @param peak
	 *            : the integer position in the data set corresponding to the peak of the detected
	 *            note. This is include for graphical output. This value is invalid if no peak was
	 *            detected.
	 * @param data
	 *            (double[]) the FFT data set. This is include for graphical output.
	 */
	public void noteChanged(String noteName, double frequency, int peak, double[] data);
}
