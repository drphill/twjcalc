package twjcalc.gui.tuner;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * GraphPanel.java is part of Phill van Leersum's EarTrainer program.
 *
 * EarTrainer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * EarTrainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with EarTrainer.  If not, see http://www.gnu.org/licenses/.
 */

/**
 * GraphPanel Jan 16, 2011
 */
public class GraphPanel extends Canvas {
	private static final long serialVersionUID = 1L;
	private double[] data;
	private int peak = -1;

	/**
	 * GraphPanel Constructor
	 */
	public GraphPanel() {
	}

	public final void draw(final byte[] in) {
		data = new double[in.length];
		for (int i = 0; i < in.length; i++) {
			data[i] = in[i];
		}
		peak = -1;
		repaint();
	}

	public final void draw(final double[] in) {
		data = new double[in.length];
		for (int i = 0; i < in.length; i++) {
			data[i] = in[i];
		}
		repaint();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.Container#paint(java.awt.Graphics)
	 */
	@Override
	public void paint(final Graphics graphics) {
		if (null == data) {
			return;
		}
		double max = Double.MIN_VALUE;
		double min = Double.MAX_VALUE;
		for (int i = 1; i < data.length; i++) {
			if (data[i] > max) {
				max = data[i];
			}
			if (data[i] < min) {
				min = data[i];
			}
		}
		final double spread = max - min;

		// System.out.println("Graph Panel paint");
		final double yScale = (getHeight()) / (spread);
		final int yOff = getHeight() - 1;
		final double xScale = ((double) getWidth()) / ((double) data.length);

		int x1 = 0;
		int y1 = yOff - (int) ((data[0] - min) * yScale);

		for (int i = 1; i < data.length; i++) {
			// System.out.println("("+x1+","+y1+")");
			final int x2 = (int) (xScale * i);
			final int y2 = yOff - (int) ((data[i] - min) * yScale);
			if ((x1 != x2) || (y1 != y2)) {
				graphics.drawLine(x1, y1, x2, y2);
				x1 = x2;
				y1 = y2;
			}
		}
		if (-1 != peak) {
			x1 = (int) (xScale * peak);
			graphics.setColor(Color.RED);
			graphics.drawLine(x1, 0, x1, getHeight());
		}
	}

	/**
	 * @param i
	 */
	public void setPeak(final int i) {
		peak = i;
	}

}
