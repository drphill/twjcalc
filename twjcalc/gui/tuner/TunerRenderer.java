package twjcalc.gui.tuner;

import java.awt.Color;
import java.awt.Component;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;

import twjcalc.gui.NumberRender;
import twjcalc.model.whistle.Whistle;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * TunerRenderer.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 */

/**
 * TunerRenderer
 * <p>
 * Jan 20, 2011
 */
public class TunerRenderer implements NoteListener {

	public static final int RANGE_FULL = 0;
	public static final int RANGE_LOW_OCTAVE = 1;
	public static final int RANGE_HIGH_OCTAVE = 2;
	public static final int RANGE_TWO_OCTAVES = 3;

	private static final int ALIGN_BOTTOM = 10;
	private static final int ALIGN_FIRST = 10;
	private static final int ALIGN_SECOND = 50;
	private static final int SAMPLE_RATE = 8000;

	private int sampleDuration = 100;
	private Color paleColour = Color.LIGHT_GRAY;
	private final Component owner;
	private Whistle whistle;
	private final NoteDetector noteDetector;
	private Rectangle graphRect = new Rectangle();
	private double[] data;
	private int peak = -1;
	private int[] noteMarkers;
	private double currentPeakFrequency;

	private int range = RANGE_FULL;
	double xScale, lowFrequency, highFrequency;
	int xOff, minDisplayX, maxDisplayX;
	private boolean enabled = false;
	private boolean accumulate = false;

	/**
	 * TunerRenderer Constructor
	 * 
	 * @param owner
	 */
	public TunerRenderer(final Component owner) {
		super();
		this.owner = owner;
		noteDetector = new NoteDetector();
		noteDetector.addListener(this);
	}

	private void internalRender(final Graphics graphics) {
		graphics.clearRect(graphRect.x, graphRect.y, graphRect.width, graphRect.height);
		getNoteMarkers();
		if (null != noteMarkers) {
			graphics.setColor(paleColour);

			for (int i = 0; i < noteMarkers.length; i++) {
				graphics.drawLine(noteMarkers[i], ALIGN_SECOND, noteMarkers[i], graphRect.y + graphRect.height
					+ ALIGN_BOTTOM);
			}
		}
		graphics.setColor(Color.BLACK);
		graphics.drawLine(graphRect.x - 1, graphRect.y, graphRect.x - 1, graphRect.y + graphRect.height);
		graphics.drawLine(graphRect.x, graphRect.y + graphRect.height + 1, graphRect.x + graphRect.width, graphRect.y
			+ graphRect.height + 1);

		if (null == data) {
			return;
		}

		double max = Double.MIN_VALUE;
		double min = Double.MAX_VALUE;
		for (int i = 1; i < data.length; i++) {
			if (data[i] > max) {
				max = data[i];
			}
			if (data[i] < min) {
				min = data[i];
			}
		}
		final double spread = max - min;

		// System.out.println("Graph Panel paint");
		final double yScale = graphRect.height / (spread);
		final int yOff = graphRect.y + graphRect.height;
		int x1 = xPosFromOffset(minDisplayX);
		int y1 = yOff - (int) ((data[0] - min) * yScale);

		if (y1 < graphRect.y) {
			y1 = graphRect.y;
		}

		graphics.setColor(Color.BLACK);
		for (int i = minDisplayX + 1; i < maxDisplayX; i++) {
			// System.out.println("("+x1+","+y1+")");
			// final int x2 = (int) (xScale * i) + xOff;
			final int x2 = xPosFromOffset(i);

			int y2 = yOff - (int) ((data[i] - min) * yScale);
			if (y2 < graphRect.y) {
				y2 = graphRect.y;
			}
			if ((x1 != x2) || (y1 != y2)) {
				graphics.drawLine(x1, y1, x2, y2);
				x1 = x2;
				y1 = y2;
			}
		}
		if (minDisplayX < peak) {
			x1 = xPosFromOffset(peak);
			graphics.setColor(Color.RED);
			graphics.drawLine(x1, graphRect.y, x1, graphRect.y + graphRect.height);
			final String str = NumberRender.stringWithDecimalPlaces(currentPeakFrequency, 2);
			final FontMetrics fm = graphics.getFontMetrics();
			x1 -= fm.stringWidth(str);
			graphics.drawString(str, x1, graphRect.y + graphRect.height - 5);
		}
	}

	private final int offsetFromFrequency(final double frequency) {
		return (int) ((frequency * sampleDuration) / 1000);

	}

	private final int xPosFromOffset(final int offset) {
		return (int) (((offset - minDisplayX) * xScale) + xOff);
	}

	public final int[] getNoteMarkers() {
		if ((null == noteMarkers) && (null != whistle)) {
			switch (range) {
				case RANGE_FULL:
					lowFrequency = 0;
					highFrequency = SAMPLE_RATE / 2;
					break;
				case RANGE_LOW_OCTAVE:
					lowFrequency = whistle.hole[0].frequency * 0.9;
					highFrequency = whistle.hole[whistle.hole.length - 1].frequency * 1.1;
					break;
				case RANGE_HIGH_OCTAVE:
					lowFrequency = whistle.hole[0].frequency * 0.9 * 2;
					highFrequency = whistle.hole[whistle.hole.length - 1].frequency * 1.1 * 2;
					break;
				case RANGE_TWO_OCTAVES:
					lowFrequency = whistle.hole[0].frequency * 0.9;
					highFrequency = whistle.hole[whistle.hole.length - 1].frequency * 1.1 * 2;
					break;
			}
			minDisplayX = offsetFromFrequency(lowFrequency);
			maxDisplayX = offsetFromFrequency(highFrequency);

			final int diff = maxDisplayX - minDisplayX;
			// xScale = (graphRect.width * 2.0) / sampleSize;
			xScale = ((double) graphRect.width / diff);
			xOff = graphRect.x;

			noteMarkers = new int[whistle.hole.length];
			for (int i = 0; i < noteMarkers.length; i++) {
				double frequency = whistle.hole[i].frequency;
				if (RANGE_HIGH_OCTAVE == range) {
					frequency *= 2;
				}
				final int xx = offsetFromFrequency(frequency);
				final int x = xPosFromOffset(xx);
				noteMarkers[i] = x;
			}
		}
		return noteMarkers;
	}

	public void labelYAxis(final Graphics graphics) {
		graphics.setColor(Color.BLACK);
		final FontMetrics fm = graphics.getFontMetrics();
		String str = NumberRender.stringWithDecimalPlaces(lowFrequency, 2);
		int x = graphRect.x - (fm.stringWidth(str) / 2);
		int y = graphRect.y + graphRect.height + fm.getHeight();
		graphics.drawString(str, x, y);
		str = NumberRender.stringWithDecimalPlaces(highFrequency, 2);
		x = graphRect.x + graphRect.width - (fm.stringWidth(str) / 2);
		y = graphRect.y + graphRect.height + fm.getHeight();
		graphics.drawString(str, x, y);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see twjcalc.gui.tuner.NoteListener#noteChanged(java.lang.String, double, int, double[])
	 */
	@Override
	public void noteChanged(final String noteName, final double frequency, final int peak, final double[] data) {
		if (enabled) {
			final Graphics graphics = owner.getGraphics();
			if (accumulate && (null != this.data)) {
				for (int i = 0; (i < data.length) && (i < this.data.length); i++) {
					this.data[i] += data[i];
				}
			} else {
				this.data = data;
			}
			this.peak = peak;
			currentPeakFrequency = frequency;
			internalRender(graphics);
		}
	}

	public final int[] render(final Graphics graphics, final Rectangle graphSize, final int[] topAligners) {

		graphRect = new Rectangle(graphSize);
		graphRect.x = topAligners[0] / 2;
		graphRect.width -= topAligners[0];
		graphRect.y += ALIGN_SECOND;
		graphRect.height -= (ALIGN_SECOND + ALIGN_BOTTOM);
		getNoteMarkers();
		graphics.setColor(paleColour);
		if (null != noteMarkers) {
			for (int i = 0; i < noteMarkers.length; i++) {
				graphics.drawLine(topAligners[i], 0, topAligners[i], ALIGN_FIRST);
				graphics.drawLine(topAligners[i], ALIGN_FIRST, noteMarkers[i], ALIGN_SECOND);
			}
		}
		labelYAxis(graphics);

		internalRender(graphics);

		return noteMarkers;
	}

	/**
	 * @param state
	 */
	public void setAccumulate(final boolean state) {
		accumulate = state;
		data = null;
	}

	public final void setBothOctaves() {
		range = RANGE_TWO_OCTAVES;
		noteMarkers = null;
	}

	/**
	 * @param i
	 */
	public void setDuration(final int i) {
		data = null;
		noteMarkers = null;
		sampleDuration = i;
		owner.repaint();
	}

	public final void setFullRange() {
		range = RANGE_FULL;
		noteMarkers = null;
	}

	public final void setHighOctave() {
		range = RANGE_HIGH_OCTAVE;
		noteMarkers = null;
	}

	public final void setLowOctave() {
		range = RANGE_LOW_OCTAVE;
		noteMarkers = null;
	}

	public final void setPaleColour(final Color paleColour) {
		this.paleColour = paleColour;
	}

	/**
	 * @param whistle
	 *            the whistle to set
	 */
	public void setWhistle(final Whistle whistle) {
		this.whistle = whistle;
		noteMarkers = null;
	}

	public void start() {
		if (!enabled) {
			noteDetector.startCapture(sampleDuration, SAMPLE_RATE);
		}
		enabled = true;
	}

	public void stop() {
		if (enabled) {
			noteDetector.stopCapture();
		}
		enabled = false;
	}

}
