/*
 * Copyright 2010 Phill van Leersum
 * 
 * BorderPanel.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 *
 *
 */
package twjcalc.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.Panel;

/*
 * Copyright 2011 Phill van Leersum
 * 
 * BorderPanel.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 *
 */
public class BorderPanel extends Panel {
	class Layout implements LayoutManager {
		/*
		 * (non-Javadoc)
		 * 
		 * @see java.awt.LayoutManager#addLayoutComponent(java.lang.String, java.awt.Component)
		 */
		@Override
		public void addLayoutComponent(final String name, final Component comp) {
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.awt.LayoutManager#layoutContainer(java.awt.Container)
		 */
		@Override
		public void layoutContainer(final Container parent) {
			final Dimension d = parent.getSize();
			d.width -= 2 * BORDER;
			d.height -= 2 * BORDER;
			panel.setBounds(BORDER, BORDER, d.width, d.height);

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.awt.LayoutManager#minimumLayoutSize(java.awt.Container)
		 */
		@Override
		public Dimension minimumLayoutSize(final Container parent) {
			final Dimension dim = panel.getMinimumSize();
			dim.width += 2 * BORDER;
			dim.height += 2 * BORDER;
			return dim;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.awt.LayoutManager#preferredLayoutSize(java.awt.Container)
		 */
		@Override
		public Dimension preferredLayoutSize(final Container parent) {
			final Dimension dim = panel.getPreferredSize();
			dim.width += 2 * BORDER;
			dim.height += 2 * BORDER;
			return dim;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.awt.LayoutManager#removeLayoutComponent(java.awt.Component)
		 */
		@Override
		public void removeLayoutComponent(final Component comp) {
		}

	}
	private static final long serialVersionUID = 1L;
	private static final int BORDER = 5;
	public static final int THIN_UP = 1;
	public static final int THICK_UP = 2;
	public static final int THIN_DOWN = 3;
	public static final int THICK_DOWN = 4;
	public static final int LEVEL_DARK = 5;

	public static final int LEVEL_LIGHT = 6;;

	private final Component panel;
	private final int style;

	private static final Color LIGHT = new Color(250, 250, 250);

	private static final Color DARK = new Color(150, 150, 150);

	public BorderPanel(final Component panel) {
		this(panel, THIN_UP);
	}

	/**
	 * 
	 */
	public BorderPanel(final Component panel, final int style) {
		super();
		// setBackground(TopPanel.USER_EDIT_COLOUR);
		setLayout(this.new Layout());
		this.panel = panel;
		add(panel);
		this.style = style;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.Container#paint(java.awt.Graphics)
	 */
	@Override
	public void paint(final Graphics graphics) {
		super.paint(graphics);
		final Dimension d = getSize();
		final int left = 3;
		final int right = d.width - 4;
		final int top = 3;
		final int bottom = d.height - 4;

		switch (style) {
			case THIN_UP:
			case THICK_UP:
			case LEVEL_LIGHT:
				graphics.setColor(LIGHT);
				break;
			case THIN_DOWN:
			case THICK_DOWN:
			case LEVEL_DARK:
				graphics.setColor(DARK);
				break;
		}
		graphics.drawLine(left, top, right, top);
		graphics.drawLine(left, top, left, bottom);

		switch (style) {
			case THICK_UP:
			case THICK_DOWN:
				graphics.drawLine(left + 1, top + 1, right - 1, top + 1);
				graphics.drawLine(left + 1, top + 1, left + 1, bottom - 1);
				break;
		}

		switch (style) {
			case THIN_UP:
			case THICK_UP:
			case LEVEL_DARK:
				graphics.setColor(DARK);
				break;
			case THIN_DOWN:
			case THICK_DOWN:
			case LEVEL_LIGHT:
				graphics.setColor(LIGHT);
				break;
		}
		graphics.drawLine(left, bottom, right, bottom);
		graphics.drawLine(right, top, right, bottom);

		switch (style) {
			case THICK_UP:
			case THICK_DOWN:
				graphics.drawLine(left + 1, bottom - 1, right - 1, bottom - 1);
				graphics.drawLine(right - 1, top + 1, right - 1, bottom - 1);
				break;
		}

	}
}
