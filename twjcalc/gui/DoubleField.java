package twjcalc.gui;

import java.awt.TextField;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * DoubleField.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Presents an edit field that allows a float to be edited.
 */
public class DoubleField extends TextField implements TextListener {

	private static final long serialVersionUID = 1L;
	private final ChangeListener _listener;
	private String _value = "0.00"; // save last valid value

	public DoubleField(final double value, final ChangeListener listener) {
		super("0.000");
		_listener = listener;
		addTextListener(this);
		setValue(value);
	}

	private final boolean isValid(final String str) {
		boolean valid = true;
		try {
			Float.parseFloat(str);
		} catch (final NumberFormatException e) {
			valid = false;
		}
		return valid;
	}

	public final double getValue() {
		final String str = getText();
		final double f = Double.parseDouble(str);
		return f;
	}

	public final void setValue(final double value) {
		final String str = NumberRender.stringWithDecimalPlaces(value, 3);
		setText(str);
	}

	@Override
	public void textValueChanged(final TextEvent arg0) {
		final String str = getText();
		if (isValid(str)) {
			_value = str;
			if (null != _listener) {
				_listener.changed(this);
			}
		} else {
			setText(_value);
		}
	}

}
