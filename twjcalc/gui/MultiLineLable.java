/*
 * Copyright 2010 Phill van Leersum
 * 
 * NonEditTextField.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 *
 *
 */
package twjcalc.gui;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.Scrollbar;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.util.ArrayList;
import java.util.StringTokenizer;

/*
 * Copyright 2011 Phill van Leersum
 * 
 * MultiLineLable.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 *
 */
public class MultiLineLable extends Panel implements AdjustmentListener {

	private static final long serialVersionUID = 1L;
	private final Scrollbar scrollbar;
	private final Canvas canvas;
	private String text;

	/**
	 * 
	 */
	public MultiLineLable() {
		super(new BorderLayout());
		scrollbar = new Scrollbar(Scrollbar.VERTICAL);
		scrollbar.addAdjustmentListener(this);
		canvas = new Canvas() {
			private static final long serialVersionUID = 1L;

			@Override
			public void paint(final Graphics g) {
				super.paint(g);
				doPaint(g, getSize());
			};
		};
		// canvas.setBackground(TopPanel.USER_EDIT_COLOUR);
		canvas.setFont(new Font("Dialog", Font.PLAIN, 14));
		add(new BorderPanel(canvas, BorderPanel.THIN_DOWN), BorderLayout.CENTER);
		add(scrollbar, BorderLayout.EAST);
	}

	public MultiLineLable(final String text) {
		this();
		setText(text);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.AdjustmentListener#adjustmentValueChanged(java.awt.event.AdjustmentEvent)
	 */
	@Override
	public void adjustmentValueChanged(final AdjustmentEvent e) {
		canvas.repaint();
	}

	public void doPaint(final Graphics graphics, final Dimension size) {

		int width = getWidth() - 10 - scrollbar.getSize().width;
		if (width < 10) {
			width = 200;
		}
		if (width > 10) {
			final FontMetrics fm = graphics.getFontMetrics();
			// first break into lines on line-break characters
			final ArrayList lines = new ArrayList();
			final StringTokenizer lineTokeniser = new StringTokenizer(text, "\n");
			while (lineTokeniser.hasMoreTokens()) {
				// now break each line down into words that we can wrap
				final String line = lineTokeniser.nextToken();
				final StringTokenizer wordTokeniser = new StringTokenizer(line);
				String lineBeingBuilt = "";
				while (wordTokeniser.hasMoreTokens()) {
					final String word = wordTokeniser.nextToken();
					String tempLine = lineBeingBuilt;
					if (tempLine.length() > 0) {
						tempLine += " ";
					}
					tempLine += word;
					if (fm.stringWidth(tempLine) > width) {
						// tempLine is too long, so save the buildingLine
						lines.add(lineBeingBuilt);
						lineBeingBuilt = word;
					} else {
						// tempLine is shorter than maximum size - keep building it
						lineBeingBuilt = tempLine;
					}
				}
				// no more tokens, so we need to save what we have of the buildingLine
				if (lineBeingBuilt.length() > 0) {
					lines.add(lineBeingBuilt);
					lineBeingBuilt = "";
				}
			}
			// now have a list of lines
			final int offset = scrollbar.getValue();
			final int nLines = lines.size();
			int y = fm.getHeight();
			final int visibleLines = getHeight() / fm.getHeight();
			for (int l = offset; l < nLines; l++) {
				graphics.drawString(lines.get(l).toString(), 5, y);
				y += fm.getHeight();
			}
			scrollbar.setValues(offset, visibleLines, 0, nLines);
		}

	}

	public final String getText() {
		return text;
	}

	public final void setText(final String text) {
		this.text = text;
		scrollbar.setValues(0, 1, 0, 1);
		canvas.repaint();
	}

}
