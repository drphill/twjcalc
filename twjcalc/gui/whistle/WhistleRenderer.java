package twjcalc.gui.whistle;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;

import twjcalc.gui.NumberRender;
import twjcalc.model.whistle.Hole;
import twjcalc.model.whistle.Whistle;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * WhistleRenderer.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
public class WhistleRenderer {
	private static final int TOP_ALIGN = 10;

	private static final double ARROWFLARE = 0.6;
	private static final int ARROWSIZE = 7;
	private static final int ENGINEERING = 90;

	private FontMetrics _fontMetrics;
	private int drawWhistleWidth;
	private final int[] externalAlign;
	private Color paleColour = new Color(240, 240, 240); // Color.LIGHT_GRAY;
	private final Whistle whistle;
	private int whistleBottom;

	private double scaleFactor;

	private int xOffset;

	private int yOffset;

	int[] holePositions;

	private int[] aligners;

	public WhistleRenderer(final Whistle whistle, final int[] align, final boolean transparent, final boolean lineDraw) {
		super();
		this.whistle = whistle;
		externalAlign = align;
	}

	private final void _distanceLine(final Graphics graphics, int x1, int x2, final int y, final String label) {
		x1 += 3;
		x2 -= 3;
		graphics.setColor(paleColour);
		graphics.drawLine(x1, y, x2, y);
		_drawArrow(graphics, x1, y, x2, y);
		_drawArrow(graphics, x2, y, x1, y);
		graphics.setColor(Color.BLACK);
		final int strWid = _fontMetrics.stringWidth(label);
		final int strX = (x2 + x1 - strWid) / 2;
		final int strY = y + _fontMetrics.getHeight();
		graphics.drawString(label, strX, strY);

	}

	private final void _drawArrow(final Graphics graphics, final int srcX, final int srcY, final int dstX,
			final int dstY) {
		final int[] _x = new int[4];
		final int[] _y = new int[4];

		for (int i = 0; i < 4; i++) {
			_x[i] = dstX;
			_y[i] = dstY;
		}

		int deltaX = srcX - dstX;
		int deltaY = srcY - dstY;
		// calculate length of line between points
		final double lineLength = java.lang.Math.sqrt((deltaX * deltaX) + (deltaY * deltaY));
		// what proportion of this do we want?
		double ratio = ARROWSIZE / lineLength; // small arrow length is
		// 20?
		_x[2] += (int) (ratio * deltaX);
		_y[2] += (int) (ratio * deltaY);

		/*
		 * this is the 'back point' of arrow on line. We wish to construct an imaginary line through
		 * this orthogonal to the original line and offset along it in both directions to determine
		 * the outer points of the arrow
		 */
		_x[1] = _x[3] = dstX + (int) (ratio * deltaX * 2);
		_y[1] = _y[3] = dstY + (int) (ratio * deltaY * 2);

		/*
		 * with an orthogonal line the x and y deltas are exchanged but the length will remain the
		 * same, so the ratios will be the same but we wish to reduce arrow angle
		 */
		ratio *= ARROWFLARE;
		final int temp = deltaX;
		deltaX = deltaY;
		deltaY = temp;
		_x[1] += (int) (ratio * deltaX);
		_y[1] -= (int) (ratio * deltaY);
		_x[3] -= (int) (ratio * deltaX);
		_y[3] += (int) (ratio * deltaY);

		graphics.fillPolygon(_x, _y, 4);
	}

	private final int getValidHoleCount() {
		int validCount = 0;
		boolean valid = true;
		for (int i = 0; valid && (i < whistle.hole.length); i++) {
			final Hole hole = whistle.hole[i];
			final double position = hole.position;
			if (Double.isNaN(position)) {
				valid = false;
			} else if (position <= 0) {
				valid = false;
			} else {
				validCount += 1;
			}
		}
		return validCount;
	}

	private final int getX(final double xx) {
		return (int) (xx * scaleFactor);
	}

	private final int getXPos(final double xx) {
		return xOffset + getX(xx);
	}

	private final int getY(final double yy) {
		return (int) (yy * scaleFactor);
	}

	private final int getYPos(final double yy) {
		return yOffset - getY(yy);
	}

	private final int renderWhistle(final Graphics graphics, final int x, final Rectangle size) {
		// size.width is width of whistle picture,
		// size.height is bottom of window,
		final double bladePosition = whistle.hole[0].position - whistle.embouchure.correction;
		final double whistleLength = bladePosition + whistle.embouchure.length + whistle.bore;
		final double whistleWidth = whistle.bore + (2 * whistle.wallThickness);
		xOffset = x;
		yOffset = size.height - ENGINEERING - 20;
		scaleFactor = size.width / whistleLength;

		final int top = getYPos(whistleWidth);
		final int height = getY(whistleWidth);
		final int left = x;
		final int width = size.width;

		graphics.setColor(Color.BLACK);
		graphics.drawRect(left, top, width, height);

		final int midLine = top + (height / 2);
		holePositions = new int[whistle.hole.length];
		for (int i = 1; i < holePositions.length; i++) {
			final double endHolePos = whistle.hole[0].position;
			holePositions[i] = getXPos(endHolePos - whistle.hole[i].position);
			final int holeSize = getX(whistle.hole[i].size);
			final int xx = holePositions[i] - (holeSize / 2);
			final int yy = midLine - (holeSize / 2);
			if (whistle.hole[i].isThumbHole) {
				graphics.setColor(Color.LIGHT_GRAY);
			} else {
				graphics.setColor(Color.BLACK);
			}
			graphics.drawOval(xx, yy, holeSize, holeSize);
		}

		final int embX = getXPos(bladePosition);
		final int embW = getX(whistle.embouchure.length);
		final int embH = getY(whistle.embouchure.width);
		final int embY = midLine - (embH / 2);
		graphics.setColor(Color.BLACK);
		graphics.drawRect(embX, embY, embW, embH);

		drawWhistleWidth = height;
		final int whistleTop = size.height - ENGINEERING - height - 20;

		return whistleTop;
	}

	public void drawAligners(final Graphics graphics, final int diagTop, final int[] drawPositions) {
		// draw aligners
		if (null != externalAlign) {
			graphics.setColor(paleColour);
			for (int i = 0; i < aligners.length; i++) {
				graphics.drawLine(aligners[i], diagTop, aligners[i], diagTop + TOP_ALIGN);
			}
		}
	}

	public final int[] getAligners() {
		return aligners;
	}

	public int getWhistleBottom() {
		return whistleBottom;
	}

	/**
	 * Render the whistle specified at x, y and width wide, including all the dimension lines
	 * 
	 * @param graphics
	 * @param whistleLeft
	 * @param whistleTop
	 * @param width
	 * @return The highest y coordinate printed at.
	 */
	public final int render(final Graphics graphics, final int whistleLeft, final Rectangle size) {

		// final int whistleTop = renderWhistle(graphics, whistleLeft, size, rotation, zoom);
		final int whistleTop = renderWhistle(graphics, whistleLeft, size);

		final int diagTop = whistleTop - TOP_ALIGN;
		whistleBottom = whistleTop + drawWhistleWidth;
		int windowTop = whistleTop + ((drawWhistleWidth) / 2);

		final int holeCount = getValidHoleCount() - 1;
		if (holeCount < 1) {
			System.out.println("whoops");
			return diagTop;
		}
		final int[] drawPositions = new int[holeCount];
		final int[] distances = new int[holeCount];

		// note that first hole position is at index 1.
		final double endHolePosition = whistle.hole[0].position;
		for (int i = 1; i <= holeCount; i++) {
			final Hole hole = whistle.hole[i];
			final double fromEnd = endHolePosition - hole.position;
			// final double drawPosition = wimage.x(fromEnd);
			final double drawPosition = getXPos(fromEnd);
			drawPositions[i - 1] = (int) drawPosition;
			// final double fromEndPrev = endHolePosition - whistle.hole[i - 1].position;
			// distances[i - 1] = (int) (fromEnd - fromEndPrev);
			distances[i - 1] = (int) (whistle.hole[i - 1].position - hole.position);
		}

		// engineering lines and distances
		_fontMetrics = graphics.getFontMetrics();
		windowTop = whistleTop + drawWhistleWidth + 10;

		final int engineeringTop = whistleTop + drawWhistleWidth + 10;

		int lastPos = whistleLeft;

		graphics.setColor(Color.LIGHT_GRAY);

		// draw vertical marker line
		graphics.drawLine(whistleLeft, windowTop, whistleLeft, engineeringTop + 90);

		for (int i = 0; i < drawPositions.length; i++) {
			graphics.setColor(paleColour);
			final int windowLeft = drawPositions[i];
			graphics.drawLine(windowLeft, engineeringTop, windowLeft, engineeringTop + 30);
			final String str = NumberRender.asInt(distances[i]);
			_distanceLine(graphics, lastPos, windowLeft, engineeringTop + 15, str);
			lastPos = windowLeft;
		}

		// float[] position = _calculation.getHolePositions();
		// B1 - B3 grouping
		graphics.setColor(paleColour);
		if (drawPositions.length > 2) {
			final int x1 = drawPositions[0];
			graphics.drawLine(x1, engineeringTop + 30, x1, engineeringTop + 60);
			final int x2 = drawPositions[2];
			graphics.drawLine(x2, engineeringTop + 30, x2, engineeringTop + 60);
			final double delta = (whistle.hole[1].position - whistle.hole[3].position);
			final String str = NumberRender.asInt(delta);
			_distanceLine(graphics, x1, x2, engineeringTop + 45, str);
		}

		if (drawPositions.length > 5) {
			final int topHole = whistle.hole.length - 1;
			graphics.setColor(paleColour);
			// T1 - T3 grouping
			final int x1 = drawPositions[3];
			graphics.drawLine(x1, engineeringTop + 30, x1, engineeringTop + 60);
			final int x2 = drawPositions[drawPositions.length - 1];
			graphics.drawLine(x2, engineeringTop + 30, x2, engineeringTop + 60);
			final double delta = (whistle.hole[4].position - whistle.hole[topHole].position);
			final String str = NumberRender.asInt(delta);
			_distanceLine(graphics, x1, x2, engineeringTop + 45, str);
		}

		graphics.setColor(paleColour);
		final double midWindow = endHolePosition - whistle.embouchure.correction; // -
																					// embHalfLength;
		// int windowMeasure = wimage.x(midWindow);
		final int windowMeasure = getXPos(midWindow);
		String str = NumberRender.asInt((float) midWindow);

		// windowMeasure += whistleLeft;
		graphics.drawLine(windowMeasure, engineeringTop /* + 60 */, windowMeasure, engineeringTop + 90);
		_distanceLine(graphics, whistleLeft, windowMeasure, engineeringTop + 75, str);

		final int xx = drawPositions[drawPositions.length - 1] + 20;
		str = "(All measurements approximate)";
		graphics.setColor(Color.LIGHT_GRAY);
		graphics.drawString(str, xx, engineeringTop + 30);

		aligners = new int[drawPositions.length + 1];
		aligners[0] = whistleLeft;
		for (int i = 1; i < aligners.length; i++) {
			aligners[i] = drawPositions[i - 1];
		}
		drawAligners(graphics, diagTop, drawPositions);

		return diagTop;
	}

	public final void setPaleColour(final Color paleColour) {
		this.paleColour = paleColour;
	}

}
