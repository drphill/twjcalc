package twjcalc.gui.whistle;

import java.awt.Color;
import java.awt.Component;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;

import twjcalc.gui.NumberRender;
import twjcalc.model.whistle.Hole;
import twjcalc.model.whistle.Whistle;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * GraphRenderer.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
public class GraphRenderer implements GraphController.Listener {
	private static final int TICK = 5;
	private static final double DELTA = 0.1;

	private Color paleColour = Color.LIGHT_GRAY;
	private final Component owner;
	private Whistle whistle;
	private final GraphController maxController, minController;
	private double graphMinimum = 0, graphMaximum = 0, cutoffMinimum = 0, cutoffMaximum = 0;

	private static final int BOTTOM_ALIGN = 10;

	public GraphRenderer(final Component owner) {
		super();
		this.owner = owner;
		maxController = new GraphController(this);
		minController = new GraphController(this);
		owner.addMouseListener(maxController);
		owner.addMouseListener(minController);
	}

	/**
	 * Decrease the maximum of the graph range if appropriate
	 */
	@Override
	public void decreaseGraph(final GraphController sender) {
		if (maxController == sender) {
			graphMaximum -= DELTA;
			if (graphMaximum < cutoffMaximum) {
				graphMaximum += DELTA;
			}
		} else if (minController == sender) {
			graphMinimum -= DELTA;
			if (graphMinimum < 0) {
				graphMinimum = 0;
			}
		}
		owner.repaint();
	}

	/**
	 * Increase the maximum of the graph range if appropriate
	 */
	@Override
	public void increaseGraph(final GraphController sender) {
		if (maxController == sender) {
			graphMaximum += DELTA;
			if (graphMaximum > 10) {
				graphMaximum = 10;
			}
		} else if (minController == sender) {
			graphMinimum += DELTA;
			if (graphMinimum > cutoffMinimum) {
				graphMinimum -= DELTA;
			}
		}
		owner.repaint();
	}

	public final int[] render(final Graphics graphics, final Rectangle graphSize, final int[] topAligners) {
		graphics.setColor(paleColour);
		for (int i = 1; i < topAligners.length; i++) {
			graphics.drawLine(topAligners[i], graphSize.height, topAligners[i], 0);
		}

		final int graphTop = 10;
		graphics.setColor(Color.BLACK);
		final FontMetrics fm = graphics.getFontMetrics();
		final int textHeight = fm.getAscent();
		final int yScalePixels = graphSize.height - textHeight - BOTTOM_ALIGN;
		final int yBottom = graphTop + yScalePixels;

		double actualMaxCutoff = -1;
		double actualMinCutoff = 100;

		// first relative cut off is always zero - ignore
		for (int i = 1; i < whistle.hole.length; i++) {
			final Hole hole = whistle.hole[i];
			if (hole.relativeCutoff > actualMaxCutoff) {
				actualMaxCutoff = hole.relativeCutoff;
			}
			if (hole.relativeCutoff < actualMaxCutoff) {
				actualMinCutoff = hole.relativeCutoff;
			}
		}

		final double graphMin = Math.min(graphMinimum, actualMinCutoff);
		final double graphMax = Math.max(graphMaximum, actualMaxCutoff);

		final int[] cutoffPixelsY = new int[whistle.hole.length - 1];
		final double graphRange = graphMax - graphMin;
		for (int i = 0; i < cutoffPixelsY.length; i++) {
			final Hole hole = whistle.hole[i + 1];
			// [i+1] because cutoffRatios[0] is not used
			double f = hole.relativeCutoff - graphMin;
			f = (f / graphRange) * yScalePixels;
			cutoffPixelsY[i] = (int) (yBottom - f);
		}

		// final int[] cutoffPixelsX = topAligners;
		final int leftOfGraph = topAligners[1] - 50;
		final int rightOfGraph = topAligners[topAligners.length - 1] + 20;

		double step = 0.1;
		if (graphRange > 3) {
			step = 0.2;
		}
		graphics.setColor(new Color(240, 240, 240));
		for (double d = 0; d < graphMax; d += step) {
			if (d > graphMin) {
				final int y2 = yBottom - (int) ((d - graphMin) * yScalePixels / graphRange);
				graphics.drawLine(leftOfGraph, y2, rightOfGraph, y2);
			}
		}

		graphics.setColor(Color.BLACK);
		final int textLeft = leftOfGraph - fm.stringWidth("0.00") - (2 * TICK);

		String str = NumberRender.stringWithDecimalPlaces(graphMax, 2);
		graphics.drawString(str, textLeft, graphTop + (textHeight / 2));
		graphics.drawLine(leftOfGraph - TICK, graphTop, leftOfGraph, graphTop);

		str = NumberRender.stringWithDecimalPlaces(graphMin, 2);
		graphics.drawString(str, textLeft, yBottom + (textHeight / 2));

		graphics.drawLine(leftOfGraph - 5, yBottom, rightOfGraph + TICK, yBottom);
		graphics.drawLine(leftOfGraph, yBottom + TICK, leftOfGraph, graphTop - TICK);
		graphics.drawLine(rightOfGraph, yBottom + TICK, rightOfGraph, graphTop - TICK);

		graphics.setColor(Color.GRAY);
		for (double d = 1; d < graphMax; d += 1) {
			if (d > graphMin) {
				final int y2 = yBottom - (int) ((d - graphMin) * yScalePixels / graphRange);
				str = NumberRender.stringWithDecimalPlaces(d, 2);
				graphics.drawString(str, textLeft, y2 + (textHeight / 2));
				graphics.drawLine(leftOfGraph - TICK, y2, rightOfGraph, y2);
			}
		}

		if ((graphMax > 2) && (graphMin < 2)) {
			graphics.setColor(Color.BLACK);
			final int y2 = yBottom - (int) ((2 - graphMin) * yScalePixels / graphRange);
			graphics.drawString("2.00", textLeft, y2 + (textHeight / 2));
			graphics.setColor(Color.GREEN);
			graphics.drawLine(leftOfGraph - TICK, y2, rightOfGraph, y2);
		}

		graphics.setColor(Color.BLACK);
		str = "Cut Off Ratio";
		graphics.drawString(str, 5, graphTop + (textHeight * 2));
		graphics.setColor(paleColour);
		str = "(Cut Off Frequency ";
		graphics.drawString(str, 5, graphTop + (textHeight * 3));
		str = "divided by";
		graphics.drawString(str, 25, graphTop + (textHeight * 4));
		str = " Hole Frequency)";
		graphics.drawString(str, 5, graphTop + (textHeight * 5));

		graphics.setColor(Color.RED);
		for (int i = 1; i < cutoffPixelsY.length; i++) {
			graphics.drawLine(topAligners[i], cutoffPixelsY[i - 1], topAligners[i + 1], cutoffPixelsY[i]);
		}

		maxController.draw(graphics, rightOfGraph + TICK, graphTop);
		minController.draw(graphics, rightOfGraph + TICK, yBottom);
		cutoffMaximum = actualMaxCutoff;
		cutoffMinimum = actualMinCutoff;

		return topAligners;
	}

	public final void setPaleColour(final Color paleColour) {
		this.paleColour = paleColour;
	}

	/**
	 * @param whistle
	 *            the whistle to set
	 */
	public void setWhistle(final Whistle whistle) {
		if (this.whistle != whistle) {
			graphMaximum = 0;
			graphMinimum = 0;
		} else {
		}
		this.whistle = whistle;
	}

}
