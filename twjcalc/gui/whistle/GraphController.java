/*
 * Copyright 2010 Phill van Leersum
 * 
 * FloatingButton.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 *
 *
 */
package twjcalc.gui.whistle;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/*
 * Copyright 2011 Phill van Leersum
 * 
 * GraphController.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 *
 */
public class GraphController extends MouseAdapter {

	public interface Listener {
		public void decreaseGraph(GraphController sender);

		public void increaseGraph(GraphController sender);
	}
	private final Rectangle upRect = new Rectangle();
	private final Rectangle downRect = new Rectangle();
	private final Listener listener;
	private final String upText = "+";

	private final String downText = "-";

	/**
	 * 
	 */
	public GraphController(final Listener listener) {
		this.listener = listener;
	}

	public final void draw(final Graphics graphics, final int drawX, final int drawY) {
		final FontMetrics fm = graphics.getFontMetrics();
		final int textHeight = fm.getHeight();
		upRect.x = drawX;
		upRect.width = fm.stringWidth(upText) + 2;
		upRect.height = textHeight;
		upRect.y = drawY - (textHeight / 2); // - upRect.height;

		downRect.x = upRect.x + upRect.width;
		downRect.y = upRect.y;
		downRect.width = fm.stringWidth(downText);
		downRect.height = upRect.height;
		if (downRect.width > upRect.width) {
			upRect.width = downRect.width;
		} else {
			downRect.width = upRect.width;
		}

		graphics.setColor(new Color(240, 240, 240));
		graphics.fillRect(upRect.x, upRect.y, upRect.width, upRect.height);
		graphics.fillRect(downRect.x, downRect.y, downRect.width, downRect.height);

		graphics.setColor(Color.BLACK);
		graphics.drawRect(upRect.x, upRect.y, upRect.width, upRect.height);
		graphics.drawRect(downRect.x, downRect.y, downRect.width, downRect.height);
		int tx = (upRect.width - fm.stringWidth(upText)) / 2;
		final int h = fm.getAscent();
		graphics.drawString(upText, upRect.x + tx, upRect.y + h);
		tx = (downRect.width - fm.stringWidth(downText)) / 2;
		graphics.drawString(downText, downRect.x + tx, downRect.y + h);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseClicked(final MouseEvent event) {
		final Point pt = event.getPoint();
		if (upRect.contains(pt)) {
			listener.increaseGraph(this);
		} else if (downRect.contains(pt)) {
			listener.decreaseGraph(this);
		}
	}

}
