package twjcalc.gui.whistle;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Panel;
import java.awt.Rectangle;

import twjcalc.gui.TopPanel;
import twjcalc.gui.tuner.TunerRenderer;
import twjcalc.model.whistle.Whistle;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * WhistlePicturePanel.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Panel to allow the drawing of one or more specified whistles.
 */
public class WhistlePicturePanel extends Panel {
	// TODO: allow switching between GRAPH/TUNER/NONE
	private static final long serialVersionUID = 1L;
	private final TopPanel _topPanel;
	private Whistle whistle;
	private WhistleRenderer renderer;
	private final boolean lineDraw = false;
	private final boolean transparent = false;
	private boolean showGraph = true;
	private final TunerRenderer tuner;

	final GraphRenderer graph;

	private boolean showCutoffInsteadOfTuner = true;

	public WhistlePicturePanel(final TopPanel topPanel) {
		super();
		setBackground(Color.WHITE);
		_topPanel = topPanel;
		tuner = new TunerRenderer(this);
		graph = new GraphRenderer(this);
	}

	public final boolean isShowGraph() {
		return showGraph;
	}

	@Override
	public void paint(final Graphics graphics) {
		final Color paleColour = new Color(225, 225, 225); // Color.LIGHT_GRAY;
		if (null != whistle) {
			final int[] newLabelX = _topPanel.getLabelX();
			final int[] lableX = new int[newLabelX.length - 1];
			for (int i = 0; i < lableX.length; i++) {
				lableX[i] = newLabelX[i + 1];
			}
			if (null != lableX) {
				// draw an image
				final Image image = createImage(getWidth(), getHeight());
				final Graphics imageGraphics = image.getGraphics();

				renderer = new WhistleRenderer(whistle, lableX, transparent, lineDraw);
				renderer.setPaleColour(paleColour);

				final Rectangle dim = getBounds();
				dim.x = 10;
				dim.y = 0;
				dim.width -= 20;
				dim.height = renderer.render(imageGraphics, 10, dim);

				final int[] bottomAligners = renderer.getAligners();
				int[] topAligners = newLabelX;
				final int alignerBottom = dim.height;
				dim.height -= 30;
				if (showGraph) {
					if (showCutoffInsteadOfTuner) {
						graph.setPaleColour(paleColour);
						topAligners = graph.render(imageGraphics, dim, newLabelX);
						imageGraphics.setColor(paleColour);
						for (int i = 0; i < topAligners.length; i++) {
							imageGraphics.drawLine(topAligners[i], dim.height, bottomAligners[i], alignerBottom);
						}
					} else {
						tuner.setPaleColour(paleColour);
						topAligners = tuner.render(imageGraphics, dim, newLabelX);
						imageGraphics.setColor(paleColour);
						if (null != topAligners) {
							for (int i = 0; i < topAligners.length; i++) {
								imageGraphics.drawLine(topAligners[i], dim.height, bottomAligners[i], alignerBottom);
							}
						}
					}
				} else {
					graphics.setColor(paleColour);
					for (int i = 0; i < newLabelX.length; i++) {
						imageGraphics.drawLine(newLabelX[i], 0, newLabelX[i], dim.height);
					}
					imageGraphics.setColor(paleColour);
					for (int i = 0; i < topAligners.length; i++) {
						imageGraphics.drawLine(topAligners[i], dim.height, bottomAligners[i], alignerBottom);
					}
				}
				// draw image to screen
				graphics.drawImage(image, 0, 0, null);
			}
		}
	}

	/**
	 * @param state
	 */
	public void setAccumulate(final boolean state) {
		tuner.setAccumulate(state);
	}

	public void setBothOctaves() {
		tuner.setBothOctaves();
		showTuner();
	}

	/**
	 * 
	 */
	public void setFullRange() {
		tuner.setFullRange();
	}

	public void setHighOctave() {
		tuner.setHighOctave();
		showTuner();
	}

	public void setLowOctave() {
		tuner.setLowOctave();
		showTuner();
	}

	public final void setShowGraph(final boolean showGraph) {
		this.showGraph = showGraph;
		if (!showGraph) {
			tuner.stop();
		}
		repaint();
	}

	/**
	 * @param i
	 */
	public void setTunerDuration(final int i) {
		tuner.setDuration(i);
	}

	public final void setWhistle(final Whistle whistle) {
		this.whistle = whistle;
		graph.setWhistle(whistle);
		tuner.setWhistle(whistle);
	}

	/**
	 * 
	 */
	public void showCutoff() {
		showCutoffInsteadOfTuner = true;
		setShowGraph(true);
		tuner.stop();
		repaint();
	}

	/**
	 * 
	 */
	public void showTuner() {
		if (showCutoffInsteadOfTuner) {
			showCutoffInsteadOfTuner = false;
			setShowGraph(true);
		}
		repaint();
	}

	public void startTuner() {
		tuner.start();
	}

	public void stopTuner() {
		tuner.stop();
	}

	@Override
	public void update(final Graphics arg0) {
		// override the method to suppress the screen clear before the paint.
		// Our paint routine paints the entire screen
		paint(arg0);
	}

}
