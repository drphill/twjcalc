package twjcalc.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Label;
import java.awt.Panel;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * LabelledDoubleField.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Presents an edit field that allows a float to be edited.
 */
public class LabelledDoubleField extends Panel implements ChangeListener {

	private static final long serialVersionUID = 1L;
	private final ChangeListener _listener;
	private final DoubleField doubleField;

	public LabelledDoubleField(final String name, final double value) {
		this(name, value, null);
	}

	public LabelledDoubleField(final String name, final double value, final ChangeListener listener) {
		super(new BorderLayout());
		doubleField = new DoubleField(value, this);
		_listener = listener;
		final Label _label = new Label(name, Label.RIGHT);
		add(_label, BorderLayout.CENTER);
		add(doubleField, BorderLayout.EAST);

	}

	@Override
	public void changed(final Object sender) {
		if (doubleField == sender) {
			if (null != _listener) {
				_listener.changed(this);
			}
		}
	}

	public final double getValue() {
		return doubleField.getValue();
	}

	public void setEditBackground(final Color colour) {
		doubleField.setBackground(colour);
	}

	public final void setValue(final double value) {
		doubleField.setValue(value);
	}
}
