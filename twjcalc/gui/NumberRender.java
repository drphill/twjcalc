package twjcalc.gui;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * NumberRender.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
public class NumberRender {

	public static final String asInt(double f) {
		f = f + 0.5F;
		return Integer.toString((int) f);
	}

	public static void main(final String[] args) {
		double d = 123.456789;
		for (int i = 1; i < 6; i++) {
			System.out.println("" + i + " ==>" + stringWithDecimalPlaces(d, i));
		}
		d = 1234.25;
		for (int i = 1; i < 6; i++) {
			System.out.println("" + i + " ==>" + stringWithDecimalPlaces(d, i));
		}
	}

	public static final String stringWithDecimalPlaces(double f, final int dp) {
		for (int i = 0; i <= dp; i++) {
			f *= 10;
		}
		f = f + 5;
		f = f / 10;
		f = Math.floor(f);
		for (int i = 0; i < dp; i++) {
			f /= 10;
		}
		// some integers don't map to exact doubles, and we get numbers like
		// 123.457000000002
		// when we expect 123.457 - so we need to do string manipulation
		String str = Double.toString(f);
		final int dpPos = str.indexOf('.');
		if (-1 != dpPos) {
			int l = str.length();
			final int desiredLength = dpPos + dp + 1;
			if (l > desiredLength) {
				str = str.substring(0, desiredLength);
			} else {
				while (desiredLength > l) {
					str = str + "0";
					l++;
				}
			}
		}
		return str;
	}

	public NumberRender() {
		super();
	}

}
