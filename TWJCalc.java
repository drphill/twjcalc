import java.awt.Frame;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.logging.MemoryHandler;
import java.util.logging.SimpleFormatter;

import twjcalc.application.TWFrame;
import twjcalc.application.logging.LoggingOutputStream;
import twjcalc.application.logging.StdOutErrLevel;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * TWJCalc.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Top level class, entry point for java application
 * 
 */
public class TWJCalc {
	private static final Logger setupLogger() {
		final Logger twcalcLogger = Logger.getLogger("twjcalc");
		try {

			// this code taken from:
			// http://blogs.sun.com/nickstephen/entry/java_redirecting_system_out_and
			// initialize logging to go to rolling log file
			final LogManager logManager = LogManager.getLogManager();
			logManager.reset();
			Handler fileHandler;
			//
			fileHandler = new FileHandler("TWJCalcLog.txt");
			fileHandler.setFormatter(new SimpleFormatter());
			final MemoryHandler memoryHandler = new MemoryHandler(fileHandler, 100, Level.SEVERE);
			twcalcLogger.addHandler(memoryHandler);
			twcalcLogger.setLevel(Level.FINER);
			// preserve old stdout/stderr streams in case they might be useful
			// PrintStream stdout = System.out;
			// PrintStream stderr = System.err;
			// now rebind stdout/stderr to logger
			Logger logger = Logger.getLogger("stdout");
			LoggingOutputStream los = new LoggingOutputStream(logger, StdOutErrLevel.STDOUT);
			System.setOut(new PrintStream(los, true));

			logger = Logger.getLogger("stderr");
			los = new LoggingOutputStream(logger, StdOutErrLevel.STDERR);
			System.setErr(new PrintStream(los, true));

		} catch (final SecurityException e) {
			e.printStackTrace();
		} catch (final IOException e) {
			e.printStackTrace();
		}
		return twcalcLogger;
	}

	public static void main(final String[] args) {
		Logger logger = null;
		if (true) {
			logger = setupLogger();
		}
		try {
			final Frame frame = new TWFrame();
			frame.setVisible(true);
		} catch (final Throwable t) {
			if (null != logger) {
				logger.severe(t.getMessage());
			} else {
				t.printStackTrace();
			}
		}
	}

}
