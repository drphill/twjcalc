import java.applet.Applet;
import java.applet.AppletContext;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;

import twjcalc.JumpTo;
import twjcalc.gui.TopPanel;
import twjcalc.model.calculate.Specification;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * TWJCalcApplet.java is part of Phill van Leersum's TWJCalc program.
 *
 * TWJCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TWJCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TWJCalc.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
public class TWJCalcApplet extends Applet implements ActionListener {

	private static final long serialVersionUID = 1L;

	public TWJCalcApplet() throws HeadlessException {
		super();
		final Panel panel = new Panel(new BorderLayout());
		final TopPanel topPanel = new TopPanel(new Specification());
		panel.add(topPanel, BorderLayout.CENTER);

		final Panel p = new Panel(new BorderLayout());
		p.setBackground(TopPanel.USER_EDIT_COLOUR);
		final Label l = new Label(JumpTo.VERSION + " by Phill van Leersum.", Label.LEFT);
		p.add(l, BorderLayout.WEST);

		final Button b = new Button("User Guide");
		b.addActionListener(this);
		p.add(b, BorderLayout.EAST);
		panel.add(p, BorderLayout.SOUTH);

		add(panel);
		panel.setPreferredSize(new Dimension(1000, 700));
	}

	@Override
	public void actionPerformed(final ActionEvent event) {
		try {
			final AppletContext a = getAppletContext();
			final URL url = new URL(JumpTo.BASE_URL + JumpTo.HELP_URL);
			a.showDocument(url, "_blank");
			// a.showDocument(url,"_self");
			// _self to open page in same window
		} catch (final MalformedURLException e) {
			System.out.println(e.getMessage());
		}

	}

}
